export const BaseErrors = {
  ERROR_FILE_SIZE_LIMIT: 'Размер файла слишком большой',
  ERROR_FILE_TYPE: 'Не подходящий тип файла',
  COMMON_ERROR_UPLOAD_FILE: 'Ошибка загрузки файл. Попробуйте заново',
  PERMISSION_ERROR: 'Ошибка доступа',
  FILE_UPLOAD_ERROR: 'Ошибка загрузки файла',
  INCORRECT_RELATION_VALUES: 'Указанны не верные данные',
  INCORRECT_RELATION_VALUES_FULL: 'Для поля :field указанны не верные данные. Поправьте или обратитесь в тех. поддержку',
}
