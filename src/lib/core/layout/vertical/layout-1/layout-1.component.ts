import {
  ChangeDetectionStrategy, ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
  ViewEncapsulation
} from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { FuseConfigService } from '@shared/fuse/services/config.service';
import { AuthenticationService } from '@shared/core/services/auth/authentication.service';
import { UserModel } from '@shared/models/models';
import { FuseConfig } from '@shared/fuse/types';

@Component({
  selector: 'vertical-layout-1',
  templateUrl: './layout-1.component.html',
  styleUrls: ['./layout-1.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class VerticalLayout1Component implements OnInit, OnDestroy {
  fuseConfig: FuseConfig;
  navigation: any;

  // Private
  private _unsubscribeAll: Subject<any>;
  isGuest = true;

  /**
   * Constructor
   */
  constructor(
    private _fuseConfigService: FuseConfigService,
    private authService: AuthenticationService,
    private cd: ChangeDetectorRef
  ) {
    // Set the defaults
    // this.navigation = navigation;

    // Set the private defaults
    this._unsubscribeAll = new Subject();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle hooks
  // -----------------------------------------------------------------------------------------------------

  /**
   * On init
   */
  ngOnInit(): void {
    this.authService.currentUser
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((user: UserModel) => {
        this.isGuest = !!(!user || !user.id);
        this.cd.markForCheck();
      });

    // Subscribe to config changes
    this._fuseConfigService.config
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((config) => {
        this.fuseConfig = config;
        this.fuseConfig.layout.toolbar.hidden = false;
        this.fuseConfig.layout.toolbar.position = 'below-fixed';
      });
  }

  /**
   * On destroy
   */
  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }
}
