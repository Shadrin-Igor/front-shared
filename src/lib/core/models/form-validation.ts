export interface FormValidation {
  type: string,
  value: string,
  isValid?: boolean | null,
  loaded?: boolean;
  itemId?: number;
}
