import { RequestResultInfo } from '@shared/models/endpoints/http/request-result-info';

export interface HttpClientResult<T> {
  data: T;
  info: RequestResultInfo;
}
