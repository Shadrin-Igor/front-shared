import {FormValue} from './form-value';

export interface ImageFormValue extends FormValue {
  title: string;
  categoryId: number;
  tagIds: (number | string)[];
  description: string;
  metaTitle: string;
  metaDescription: string;
  metaKeyWord: string;
}
