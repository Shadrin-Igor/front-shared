export interface MetaDataModel {
  title?: string;
  description?: string;
  keyWords?: string;
}
