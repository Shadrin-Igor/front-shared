import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CountFormatPipe} from './count-format.pipe';

@NgModule({
  declarations: [CountFormatPipe],
  imports: [
    CommonModule
  ],
  exports: [CountFormatPipe]
})
export class CountFormatModule {
}
