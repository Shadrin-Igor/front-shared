import { createFeatureSelector, createSelector } from '@ngrx/store';
import { STATIC_DATA_KEY, StaticDataState } from './static-data.reducer';
import { UserModel } from '@shared/models/models/user/user-model';

// Lookup the 'Users' feature state managed by NgRx
const getStaticDataState = createFeatureSelector<StaticDataState>(STATIC_DATA_KEY);

const getLoaded = createSelector(
  getStaticDataState,
  (state: StaticDataState) => state.loaded
);
const getData = createSelector(
  getStaticDataState,
  (state: StaticDataState) => {
    return state.token ? state : null;
  }
);


export const staticDataQuery = {
  getLoaded,
  getData
};
