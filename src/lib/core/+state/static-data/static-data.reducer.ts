import { ActionReducer } from '@ngrx/store';
import {
  StaticDataAction,
  StaticDataActionTypes
} from '@shared/state/static-data/static-data.actions';
import { StaticData } from '../../../../../../core/src/lib/+state/static-data/static-data-types';

export const STATIC_DATA_KEY = 'staticData';

/**
 * Interface for the 'StaticData' data used in
 *  - StaticDataState, and
 *  - usersReducer
 *
 *  Note: replace if already defined in another module
 */

export interface StaticDataState {
  [key: string]: StaticData
}

export interface StaticDataStateData {
  staticData: StaticDataState
}

export interface StaticDataPartialState {
  readonly [STATIC_DATA_KEY]: StaticDataStateData;
}

export const initialDataStaticState = {};

export function staticDataReducer(
  state: StaticDataState = initialDataStaticState,
  action: StaticDataAction
): StaticDataState {
  switch (action.type) {
    case StaticDataActionTypes.LoadDataSuccess: {
      const { type, items } = action.payload;

      state = {
        ...state,
        [type]: {
          items,
          loaded: true
        }
      };
      break;
    }
    /*    case StaticDataActionTypes.SetLoaded: {
          console.log('RefreshToken');
          state = {
            ...state,
            loaded: false
          };
          break;
        }*/
  }
  return state;
}

export function initUserReducer(
  reducer: ActionReducer<any>
): ActionReducer<any> {
  return function (state, action) {
    return reducer(state, action);
  };
}
