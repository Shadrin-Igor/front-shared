import { Behavior } from '../../behavior';
import { HttpClientResult } from '@shared/core/index';
import { SetLastRequestResult } from '../../last-request-result';
import { BaseResponse, ResponseStatus } from '@shared/models/index';
import { LoginSendCreate201 } from '@shared/models/endpoints/auth';
import {
  LoadData,
  LoadDataFail,
  LoadDataSuccess
} from '@shared/state/static-data/static-data.actions';
import { StaticDataTypes } from '../../../../../../../core/src/lib/+state/static-data/static-data-types';

export class LoadDataBehavior extends Behavior {
  constructor(
    protected action: LoadData,
    protected result: HttpClientResult<BaseResponse>
  ) {
    super(action);
  }

  resolve(): Array<LoadDataSuccess | LoadDataFail | SetLastRequestResult> {
    const status = this.result.data.status;
    const correlationParams = this.action.correlationParams;
    let returnedActions: Array<LoadDataSuccess | LoadDataFail | SetLastRequestResult>;
    const setLastRequestResponseAction = this.generateSetLastRequestResult(
      this.action.type,
      this.result.info,
      this.action.correlationParams
    );
    switch (status) {
      case ResponseStatus.STATUS_200: {
        const response: LoginSendCreate201 = this.result.data;
        const { user } = response.body;
        // const userData: UserModel = this.modelAdapter.convertItem<UserModel, UserModel>(UserModel, user);
        returnedActions = [
          new LoadDataSuccess({ type: StaticDataTypes.Countries, items: [] }, correlationParams)
        ];
        break;
      }
      case ResponseStatus.STATUS_400:
      case ResponseStatus.STATUS_404:
      case ResponseStatus.STATUS_422:
      case ResponseStatus.STATUS_401:
        const errorResponse = this.result.data;
        const error = errorResponse.body.errors[0].title;
        returnedActions = [
          new LoadDataFail({type: StaticDataTypes.Countries}, { error }, correlationParams)
        ];
        break;
      default: {
        returnedActions = [
          new LoadDataFail({type: StaticDataTypes.Countries}, { error: 'Unhandled error' }, correlationParams)
        ];
      }

    }
    return [setLastRequestResponseAction, ...returnedActions];
  }
}
