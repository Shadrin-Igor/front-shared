import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { flatMap, map, switchMap, tap } from 'rxjs/operators';
import { Store } from '@ngrx/store';

import { HttpClientResult, StaticDataService } from '@shared/core/index';
import { BaseEffects } from '@shared/state/base.effects';
import { LoginSendCreateResponse } from '@shared/models/endpoints/auth';
import { CoreState } from '../../../../../../core/src/lib/+state/core.reducer';
import {
  LoadData,
  LoadDataFail,
  LoadDataSuccess,
  StaticDataActionTypes
} from '@shared/state/static-data/static-data.actions';
import { LoadDataBehavior } from '@shared/state/static-data/behavios/load-data-behavior';
import { StaticDataTypes } from '../../../../../../core/src/lib/+state/static-data/static-data-types';

@Injectable()
export class StaticDataEffects extends BaseEffects {
  @Effect()
  loginSend$ = this.effect(StaticDataActionTypes.LoadData, {
    operatorFunctions: (action: LoadData) => [
      switchMap(() => {
        return this.staticDataService.staticDataLoad({ type: action.payload.type });
      }),
      flatMap((r: HttpClientResult<LoginSendCreateResponse>) => new LoadDataBehavior(action, r).resolve())
    ],
    onError: (action: LoadData, error) => {
      return new LoadDataFail({ type: StaticDataTypes.Countries }, error, action.correlationParams);
    }
  });

  @Effect({ dispatch: false })
  loginSendSuccess$ = this.effect(StaticDataActionTypes.LoadDataSuccess, {
    operatorFunctions: (action: LoadDataSuccess) => [
      tap(() => {
        console.log('LoadDataSuccess', action);
      })
    ]
  });

  constructor(
    protected actions$: Actions,
    protected store: Store<CoreState>,
    protected staticDataService: StaticDataService
  ) {
    super(actions$, store);
  }
}
