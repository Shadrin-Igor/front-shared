import { Injectable } from '@angular/core';

import { select, Store } from '@ngrx/store';

import { StaticDataPartialState } from './static-data.reducer';
import { staticDataQuery } from './static-data.selectors';
import { guid } from '@shared/core/utils';
import { StaticDataTypes } from '../../../../../../core/src/lib/+state/static-data/static-data-types';
import { LoadData } from '@shared/state/static-data/static-data.actions';

@Injectable()
export class StaticDataFacade {
  loaded$ = this.store.pipe(select(staticDataQuery.getLoaded));
  getData$ = this.store.pipe(select(staticDataQuery.getData));

  constructor(private store: Store<StaticDataPartialState>) {
  }

  loadData(type: StaticDataTypes) {
    const params = { correlationId: guid() };
    this.store.dispatch(new LoadData({type}, params));
  }

  getLoading(type: StaticDataTypes) {
    const corelParams = { correlationId: guid() };
    /*    const params: LoginSendPayment = {
          email, password, remember
        };

        this.store.dispatch(new LoginSend(params, corelParams));*/
  }
}
