import { AggregatableAction, CorrelationParams, FailActionForAggregation } from '../aggregate';
import {
  StaticData,
  StaticDataItem,
  StaticDataTypes
} from '../../../../../../core/src/lib/+state/static-data/static-data-types';

export enum StaticDataActionTypes {
  LoadData = '[StaticData] Load Data',
  LoadDataSuccess = '[StaticData] Load Data Success',
  LoadDataFail = '[StaticData] Load Data Error',

  ClearData = '[StaticData] Clear Data',
  ClearDataSuccess = '[StaticData] Clear Data Success',
  ClearDataError = '[StaticData] Clear Data Error',

  SetLoaded = '[StaticData] Set Loaded'
}

export interface EmptyPayload {
};

export interface StaticDataPayload {
  type: StaticDataTypes,
}

export class LoadData implements AggregatableAction {
  readonly type = StaticDataActionTypes.LoadData;

  constructor(public payload: StaticDataPayload, public correlationParams: CorrelationParams) {
  }
}

export interface LoadDataSuccessSuccessPayload {
  type: StaticDataTypes,
  items: StaticDataItem[],
}

export class LoadDataSuccess implements AggregatableAction {
  readonly type = StaticDataActionTypes.LoadDataSuccess;

  constructor(public payload: LoadDataSuccessSuccessPayload, public correlationParams: CorrelationParams) {
  }
}

export class LoadDataFail implements FailActionForAggregation {
  readonly type = StaticDataActionTypes.LoadDataFail;

  constructor(public payload: StaticDataPayload, public error: any, public correlationParams: CorrelationParams) {
  }
}

export class SetLoaded implements AggregatableAction {
  readonly type = StaticDataActionTypes.SetLoaded;

  constructor(public payload: {}, public correlationParams: CorrelationParams) {
  }
}

export type StaticDataAction =
  | LoadData
  | LoadDataSuccess
  | LoadDataFail
  | SetLoaded;
