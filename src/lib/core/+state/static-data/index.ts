export * from './static-data.facade';
export * from './static-data.reducer';
export * from './static-data.selectors';
