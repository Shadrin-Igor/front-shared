import { UsersEffects } from './users/users.effects';
import { UsersFacade } from './users';
import { RouterEffects } from './router';
import { NotificationsEffects } from '@shared/state/notifications/notifications.effects';
import { HashtagsEffects } from '@shared/state/hashtags/hashtags.effects';
import { UiUnitsEffects } from '@shared/state/uiUnits/uiUnits.effects';
import { FilesEffects, FilesFacade } from '@shared/state/files';
import { MainEffects } from '@shared/state/main/main.effects';
import { UiUnitsFacade } from '@shared/state/uiUnits';
import { NotificationsFacade } from '@shared/state/notifications';
import { RouterFacade } from '@shared/state/router/router.facade';
import { FormsFacade } from '@shared/state/forms';
import { HashtagsFacade } from '@shared/state/hashtags';
import { MainFacade } from '@shared/state/main';

import { StaticDataEffects } from '@shared/state/static-data/static-data.effects';
import { StaticDataFacade } from '@shared/state/static-data';
import { FormValidationEffects } from '@shared/state/form-validation/form-validation.effects';
import { FormValidationFacade } from '@shared/state/form-validation';
import { FormErrorsEffects } from '@shared/state/form-errors/form-errors.effects';
import { FormErrorsFacade } from '@shared/state/form-errors';
import { FormsEffects } from '@shared/state/forms/forms.effects';

export const sharedEffects = [
  UsersEffects,
  RouterEffects,
  NotificationsEffects,
  FilesEffects,
  UiUnitsEffects,
  HashtagsEffects,
  MainEffects,
  StaticDataEffects,
  FormsEffects,
  FormValidationEffects,
  FormErrorsEffects,
];

export const sharedFacades = [
  UsersFacade,
  UiUnitsFacade,
  NotificationsFacade,
  RouterFacade,
  FilesFacade,
  FormsFacade,
  HashtagsFacade,
  MainFacade,
  StaticDataFacade,
  FormsFacade,
  FormValidationFacade,
  FormErrorsFacade
];
