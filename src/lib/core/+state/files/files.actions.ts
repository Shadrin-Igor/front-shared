import { AggregatableAction, CorrelationParams, FailActionForAggregation } from '../aggregate';
import { FileModel } from '@shared/models/models/file';
import { FieldsModel } from '@shared/models/index';

export enum FilesActionTypes {
  FilesLoad = '[Files] File Load',
  FilesLoadSuccess = '[Files] File Load Success',
  FilesLoadFail = '[Files] File Load Error',

  FileUpdate = '[Files] File Update',
  FileUpdateSuccess = '[Files] File Update Success',
  FileUpdateFail = '[Files] File Update Error',

  FileDelete = '[Files] File Delete',
  FileDeleteSuccess = '[Files] File Delete Success',
  FileDeleteFail = '[Files] File Delete Error',

  FilesSort = '[Files] File Sort',
  FilesSortSuccess = '[Files] File Sort Success',
  FilesSortFail = '[Files] File Sort Error'
}

export interface FilesLoadPayload {
  section: string;
  itemId: number;
  fields?: FieldsModel;
}

export class FilesLoad implements AggregatableAction {
  readonly type = FilesActionTypes.FilesLoad;

  constructor(public payload: FilesLoadPayload, public correlationParams: CorrelationParams) {
  }
}

export interface FilesLoadSuccessPayload {
  files: FileModel[];
}

export interface EmptyPayload {
}

export class FilesLoadSuccess implements AggregatableAction {
  readonly type = FilesActionTypes.FilesLoadSuccess;

  constructor(public payload: FilesLoadSuccessPayload, public correlationParams: CorrelationParams) {
  }
}

export class FilesLoadFail implements FailActionForAggregation {
  readonly type = FilesActionTypes.FilesLoadFail;

  constructor(public payload: EmptyPayload, public error: any, public correlationParams: CorrelationParams) {
  }
}

export interface FileUpdatePayload {
  id: number;
  sort?: number;
  title?: string;
  main?: boolean;
  section: string;
  itemId: number;
}

export class FileUpdate implements AggregatableAction {
  readonly type = FilesActionTypes.FileUpdate;

  constructor(public payload: FileUpdatePayload, public correlationParams: CorrelationParams) {
  }
}

export interface FileUpdateSuccessPayload {
  files: FileModel[];
}

export interface EmptyPayload {
}

export class FileUpdateSuccess implements AggregatableAction {
  readonly type = FilesActionTypes.FileUpdateSuccess;

  constructor(public payload: FileUpdateSuccessPayload, public correlationParams: CorrelationParams) {
  }
}

export class FileUpdateFail implements FailActionForAggregation {
  readonly type = FilesActionTypes.FileUpdateFail;

  constructor(public payload: EmptyPayload, public error: any, public correlationParams: CorrelationParams) {
  }
}

export interface FileDeletePayload {
  id: number;
}

export class FileDelete implements AggregatableAction {
  readonly type = FilesActionTypes.FileDelete;

  constructor(public payload: FileDeletePayload, public correlationParams: CorrelationParams) {
  }
}

export interface FileDeleteSuccessPayload {
  deleteId: number;
}

export interface EmptyPayload {
}

export class FileDeleteSuccess implements AggregatableAction {
  readonly type = FilesActionTypes.FileDeleteSuccess;

  constructor(public payload: FileDeleteSuccessPayload, public correlationParams: CorrelationParams) {
  }
}

export class FileDeleteFail implements FailActionForAggregation {
  readonly type = FilesActionTypes.FileDeleteFail;

  constructor(public payload: EmptyPayload, public error: any, public correlationParams: CorrelationParams) {
  }
}

export interface FilesSortPayload {
  files: { id: number, order: number }[];
}

export class FilesSort implements AggregatableAction {
  readonly type = FilesActionTypes.FilesSort;

  constructor(public payload: FilesSortPayload, public correlationParams: CorrelationParams) {
  }
}

export interface FilesSortSuccessPayload {
  files: FileModel[];
}

export interface EmptyPayload {
}

export class FilesSortSuccess implements AggregatableAction {
  readonly type = FilesActionTypes.FilesSortSuccess;

  constructor(public payload: FilesSortSuccessPayload, public correlationParams: CorrelationParams) {
  }
}

export class FilesSortFail implements FailActionForAggregation {
  readonly type = FilesActionTypes.FilesSortFail;

  constructor(public payload: EmptyPayload, public error: any, public correlationParams: CorrelationParams) {
  }
}

export type FilesAction =
  | FilesLoad
  | FilesLoadSuccess
  | FilesLoadFail
  | FileUpdate
  | FileUpdateSuccess
  | FileUpdateFail
  | FileDelete
  | FileDeleteSuccess
  | FileDeleteFail
  | FilesSort
  | FilesSortSuccess
  | FilesSortFail;
