import { Behavior } from '../../behavior';
import { SetLastRequestResult } from '../../last-request-result';
import {
  FilesSort,
  FilesSortFail,
  FilesSortSuccess
} from '../../files/files.actions';
import { HttpClientResult } from '@shared/core/index';
import { FileModel } from '@shared/models/models/file';
import { BaseResponse, ResponseStatus } from '@shared/models/index';
import { FilesUpdate200 } from '@shared/models/endpoints/files';

export class FilesSortBehavior extends Behavior {
  constructor(
    protected action: FilesSort,
    protected result: HttpClientResult<BaseResponse>
  ) {
    super(action);
  }

  resolve(): Array<FilesSortSuccess | FilesSortFail | SetLastRequestResult> {
    const status = this.result.data.status;
    const correlationParams = this.action.correlationParams;
    let returnedActions: Array<FilesSortSuccess | FilesSortFail | SetLastRequestResult>;
    const setLastRequestResponseAction = this.generateSetLastRequestResult(
      this.action.type,
      this.result.info,
      this.action.correlationParams
    );
    switch (status) {
      case ResponseStatus.STATUS_200: {
        const response: FilesUpdate200 = this.result.data;
        const { data } = response.body;
        const fileData: FileModel[] = this.modelAdapter.convertItems<FileModel, FileModel>(FileModel, data);
        returnedActions = [
          new FilesSortSuccess({ files: fileData }, correlationParams)
        ];
        break;
      }
      case ResponseStatus.STATUS_400:
      case ResponseStatus.STATUS_404:
      case ResponseStatus.STATUS_422:
      case ResponseStatus.STATUS_401:
        const errorResponse = this.result.data;
        const error = errorResponse.body.errors[0].title;
        returnedActions = [
          new FilesSortFail({}, { error }, correlationParams)
        ];
        break;
      default: {
        returnedActions = [
          new FilesSortFail({}, { error: 'Unhandled error' }, correlationParams)
        ];
      }

    }
    return [setLastRequestResponseAction, ...returnedActions];
  }
}
