import { Behavior } from '../../behavior';
import { SetLastRequestResult } from '../../last-request-result';
import {
  FilesLoad,
  FilesLoadFail,
  FilesLoadSuccess
} from '../../files/files.actions';
import { FileModel } from '@shared/models/models/file';
import { HttpClientResult } from '@shared/core/index';
import { BaseResponse, ResponseStatus } from '@shared/models/index';
import { FilesLoad200 } from '@shared/models/endpoints/files';

export class FilesLoadBehavior extends Behavior {
  constructor(
    protected action: FilesLoad,
    protected result: HttpClientResult<BaseResponse>
  ) {
    super(action);
  }

  resolve(): Array<FilesLoadSuccess | FilesLoadFail | SetLastRequestResult> {
    const status = this.result.data.status;
    const correlationParams = this.action.correlationParams;
    let returnedActions: Array<FilesLoadSuccess | FilesLoadFail | SetLastRequestResult>;
    const setLastRequestResponseAction = this.generateSetLastRequestResult(
      this.action.type,
      this.result.info,
      this.action.correlationParams
    );
    switch (status) {
      case ResponseStatus.STATUS_200: {
        const response: FilesLoad200 = this.result.data;
        const { data } = response.body;
        const fileData: FileModel[] = this.modelAdapter.convertItems<FileModel, FileModel>(FileModel, data);
        returnedActions = [
          new FilesLoadSuccess({ files: fileData }, correlationParams)
        ];
        break;
      }
      case ResponseStatus.STATUS_400:
      case ResponseStatus.STATUS_404:
      case ResponseStatus.STATUS_422:
      case ResponseStatus.STATUS_401:
        const errorResponse = this.result.data;
        const error = errorResponse.body.errors ? errorResponse.body.errors[0].title : 'Unknown error';
        returnedActions = [
          new FilesLoadFail({}, { error }, correlationParams)
        ];
        break;
      default: {
        returnedActions = [
          new FilesLoadFail({}, { error: 'Unhandled error' }, correlationParams)
        ];
      }

    }
    return [setLastRequestResponseAction, ...returnedActions];
  }
}
