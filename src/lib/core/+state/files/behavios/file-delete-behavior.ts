import { Behavior } from '@shared/state/behavior';
import { SetLastRequestResult } from '@shared/state/last-request-result';
import {
  FileDelete,
  FileDeleteFail,
  FileDeleteSuccess
} from '@shared/state/files/files.actions';
import { HttpClientResult } from '@shared/core/index';
import { BaseResponse, ResponseStatus } from '@shared/models/index';
import { FilesDelete200 } from '@shared/models/endpoints/files';

export class FileDeleteBehavior extends Behavior {
  constructor(
    protected action: FileDelete,
    protected result: HttpClientResult<BaseResponse>
  ) {
    super(action);
  }

  resolve(): Array<FileDeleteSuccess | FileDeleteFail | SetLastRequestResult> {
    const status = this.result.data.status;
    const correlationParams = this.action.correlationParams;
    let returnedActions: Array<FileDeleteSuccess | FileDeleteFail | SetLastRequestResult>;
    const setLastRequestResponseAction = this.generateSetLastRequestResult(
      this.action.type,
      this.result.info,
      this.action.correlationParams
    );
    switch (status) {
      case ResponseStatus.STATUS_200: {
        const response: FilesDelete200 = this.result.data;
        const { data } = response.body;
        // const imageData: ImageModel = this.modelAdapter.convertItem<ImageModel, ImageModelData>(ImageModel, data);
        returnedActions = [
          new FileDeleteSuccess({ deleteId: data.delete_id }, correlationParams)
        ];
        break;
      }
      case ResponseStatus.STATUS_400:
      case ResponseStatus.STATUS_404:
      case ResponseStatus.STATUS_422:
      case ResponseStatus.STATUS_401:
        const errorResponse = this.result.data;
        const error = errorResponse.body.errors[0].title;
        returnedActions = [
          new FileDeleteFail({}, { error }, correlationParams)
        ];
        break;
      default: {
        returnedActions = [
          new FileDeleteFail({}, { error: 'Unhandled error' }, correlationParams)
        ];
      }

    }
    return [setLastRequestResponseAction, ...returnedActions];
  }
}
