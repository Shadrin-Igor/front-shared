import { Behavior } from '../../behavior';
import { SetLastRequestResult } from '@shared/state/last-request-result';
import {
  FileUpdate,
  FileUpdateFail,
  FileUpdateSuccess
} from '@shared/state/files/files.actions';
import { FileModel } from '@shared/models/models/file';
import { HttpClientResult } from '@shared/core/index';
import { BaseResponse, ResponseStatus } from '@shared/models/index';
import { FilesUpdate200 } from '@shared/models/endpoints/files';

export class FileUpdateBehavior extends Behavior {
  constructor(
    protected action: FileUpdate,
    protected result: HttpClientResult<BaseResponse>
  ) {
    super(action);
  }

  resolve(): Array<FileUpdateSuccess | FileUpdateFail | SetLastRequestResult> {
    const status = this.result.data.status;
    const correlationParams = this.action.correlationParams;
    let returnedActions: Array<FileUpdateSuccess | FileUpdateFail | SetLastRequestResult>;
    const setLastRequestResponseAction = this.generateSetLastRequestResult(
      this.action.type,
      this.result.info,
      this.action.correlationParams
    );
    switch (status) {
      case ResponseStatus.STATUS_200: {
        const response: FilesUpdate200 = this.result.data;
        const { data } = response.body;
        const fileData: FileModel[] = this.modelAdapter.convertItems<FileModel, FileModel>(FileModel, data);
        console.log('fileData', fileData);
        returnedActions = [
          new FileUpdateSuccess({ files: fileData }, correlationParams)
        ];
        break;
      }
      case ResponseStatus.STATUS_400:
      case ResponseStatus.STATUS_404:
      case ResponseStatus.STATUS_422:
      case ResponseStatus.STATUS_401:
        const errorResponse = this.result.data;
        const error = errorResponse.body.errors[0].title;
        returnedActions = [
          new FileUpdateFail({}, { error }, correlationParams)
        ];
        break;
      default: {
        returnedActions = [
          new FileUpdateFail({}, { error: 'Unhandled error' }, correlationParams)
        ];
      }

    }
    return [setLastRequestResponseAction, ...returnedActions];
  }
}
