import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { flatMap, switchMap } from 'rxjs/operators';
import { Store } from '@ngrx/store';

import { BaseEffects } from '@shared/state/base.effects';
import {
  FilesDeleteRequestConfig,
  FilesDeleteResponse,
  FilesLoadRequestConfig,
  FilesLoadResponse,
  FilesSortRequestConfig,
  FilesUpdateRequestConfig,
  FilesUpdateResponse
} from '@shared/models/endpoints/files';
import { HttpClientResult } from '@shared/core/index';
import { FilesService } from '@shared/core/services/files/files.service';
import { AuthenticationService } from '@shared/core/services/auth/authentication.service';
import { CoreState } from '../../../../../../core/src/lib/+state/core.reducer';
import {
  FileDelete,
  FileDeleteFail,
  FilesActionTypes,
  FilesLoad,
  FilesLoadFail,
  FilesSort,
  FilesSortFail,
  FileUpdate,
  FileUpdateFail
} from '@shared/state/files/files.actions';
import { FileDeleteBehavior } from '@shared/state/files/behavios/file-delete-behavior';
import { FileUpdateBehavior } from '@shared/state/files/behavios/file-update-behavior';
import { FilesSortBehavior } from '@shared/state/files/behavios/files-sort-behavior';
import { FilesLoadBehavior } from '@shared/state/files/behavios/files-load-behavior';
import { UiUnitsFacade } from '@shared/state/uiUnits';
import { NotificationsFacade } from '@shared/state/notifications';

@Injectable()
export class FilesEffects extends BaseEffects {

  @Effect()
  fileDelete$ = this.effectMerge(FilesActionTypes.FileDelete, {
    operatorFunctions: (action: FileDelete) => [
      // switchMap(() => this.applySelector(getUserAuthData)),
      switchMap(() => {
        const headers = this.authenticationService.loadAuthDataFromLocalStore();
        const id = action.payload.id;
        const config: FilesDeleteRequestConfig = {
          headers: { token: headers.token, uid: headers.id },
          id
        };
        return this.filesService.fileDelete(config);
      }),
      flatMap((r: HttpClientResult<FilesDeleteResponse>) => new FileDeleteBehavior(action, r).resolve())
    ],
    onError: (action: FileDelete, error) => {
      return new FileDeleteFail({}, error, action.correlationParams);
    }
  });

  @Effect()
  fileUpdate2$ = this.effect(FilesActionTypes.FileUpdate, {
    operatorFunctions: (action: FileUpdate) => [
      // switchMap(() => this.applySelector(getUserAuthData)),
      switchMap(() => {
        const headers = this.authenticationService.loadAuthDataFromLocalStore();
        const { id, main, sort, title, section, itemId } = action.payload;
        const config: FilesUpdateRequestConfig = {
          id,
          headers: { token: headers.token, uid: headers.id },
          section,
          itemId
        };
        if (main) config['main'] = main;
        if (sort) config['sort'] = sort;
        if (title) config['title'] = title;

        return this.filesService.fileUpdate(config);
      }),
      flatMap((r: HttpClientResult<FilesUpdateResponse>) => new FileUpdateBehavior(action, r).resolve())
    ],
    onError: (action: FileUpdate, error) => {
      return new FileUpdateFail({}, error, action.correlationParams);
    }
  });

  @Effect()
  filesSort$ = this.effect(FilesActionTypes.FilesSort, {
    operatorFunctions: (action: FilesSort) => [
      switchMap(() => {
        const headers = this.authenticationService.loadAuthDataFromLocalStore();
        const { files } = action.payload;
        const config: FilesSortRequestConfig = {
          headers: { token: headers.token, uid: headers.id },
          files
        };

        return this.filesService.filesSort(config);
      }),
      flatMap((r: HttpClientResult<FilesUpdateResponse>) => new FilesSortBehavior(action, r).resolve())
    ],
    onError: (action: FilesSort, error) => {
      return new FilesSortFail({}, error, action.correlationParams);
    }
  });

  @Effect()
  filesLoad$ = this.effect(FilesActionTypes.FilesLoad, {
    operatorFunctions: (action: FilesLoad) => [
      // switchMap(() => this.applySelector(getUserAuthData)),
      switchMap(() => {
        const headers = this.authenticationService.loadAuthDataFromLocalStore();
        const { section, itemId, fields } = action.payload;
        const config: FilesLoadRequestConfig = {
          headers: { token: headers.token, uid: headers.id },
          section,
          itemId,
          fields
        };
        return this.filesService.filesLoad(config);
      }),
      flatMap((r: HttpClientResult<FilesLoadResponse>) => new FilesLoadBehavior(action, r).resolve())
    ],
    onError: (action: FilesLoad, error) => {
      return new FilesLoadFail({}, error, action.correlationParams);
    }
  });

  constructor(
    protected actions$: Actions,
    protected store: Store<CoreState>,
    protected filesService: FilesService,
    protected uiUnitsFacade: UiUnitsFacade,
    protected notificationsFacade: NotificationsFacade,
    protected authenticationService: AuthenticationService
  ) {
    super(actions$, store);
  }
}
