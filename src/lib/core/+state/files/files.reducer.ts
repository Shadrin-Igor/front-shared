import { FilesAction, FilesActionTypes } from './files.actions';
import { ReducerService } from '@shared/core/services/reducer/reducer.service';
import { FileModel } from '@shared/models/models';

export const FILES_FEATURE_KEY = 'files';

/**
 * Interface for the 'Files' data used in
 *  - FilesState, and
 *  - usersReducer
 *
 *  Note: replace if already defined in another module
 */

export interface FilesState {
  list: FileModel[];
  ids: string[];
  selected: number;
  loading: boolean; // has the Files list been loading
  error?: any; // last none error (if any)
}

export interface FilesStateData {
  files: FilesState;
}

export interface FilesPartialState {
  readonly [FILES_FEATURE_KEY]: FilesStateData;
}

export const initialFileState = {
  list: [],
  ids: [],
  selected: null,
  loading: false
};

export function filesReducer(
  state: FilesState = initialFileState,
  action: FilesAction
): FilesState {
  switch (action.type) {
    case FilesActionTypes.FilesLoadSuccess: {
      const files: FileModel[] = action.payload.files;
      state = {
        ...ReducerService.upsertMeny(state, files),
        loading: false
      };
      break;
    }
    case FilesActionTypes.FileDeleteSuccess: {
      state = {
        ...ReducerService.deleteOne(state, action.payload.deleteId),
        loading: false
      };
      break;
    }
    case FilesActionTypes.FilesLoad:
    case FilesActionTypes.FileUpdate:
    case FilesActionTypes.FileDelete:
    case FilesActionTypes.FilesSort: {
      state = {
        ...state,
        loading: true
      };
      break;
    }
    case FilesActionTypes.FileUpdateSuccess: {
      const files: FileModel[] = action.payload.files;
      state = {
        ...ReducerService.upsertMeny(state, files),
        loading: false
      };
      break;
    }
    case FilesActionTypes.FilesSortSuccess: {
      const files: FileModel[] = action.payload.files;
      state = {
        ...ReducerService.upsertMeny(state, files),
        loading: true
      };
      break;
    }
  }
  return state;
}
