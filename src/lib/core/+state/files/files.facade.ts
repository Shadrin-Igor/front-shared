import { Injectable } from '@angular/core';
import { filter } from 'rxjs/operators';
import { select, Store } from '@ngrx/store';

import { guid } from '@shared/core/utils';
import { filesQuery } from '@shared/state/files/files.selectors';
import { FilesPartialState } from '@shared/state/files/files.reducer';
import {
  FileDelete, FilesLoad,
  FilesSort,
  FileUpdate,
  FileUpdatePayload
} from '@shared/state/files/files.actions';

@Injectable()
export class FilesFacade {
  loading$ = this.store.pipe(select(filesQuery.getLoading));
  getFiles$ = this.store.pipe(select(filesQuery.getFiles));
  selected$ = this.store.pipe(select(filesQuery.geSelected));

  constructor(private store: Store<FilesPartialState>) {
  }

  waitLoading() {
    return this.store.pipe(
      select(filesQuery.getLoading),
      filter((item) => !!item)
    );
  }

  filesDelete(itemId: number) {
    const corelParams = { correlationId: guid() };
    this.store.dispatch(new FileDelete({ id: itemId }, corelParams));
  }

  filesUpdate(data: { title?: string, sort?: number, main?: boolean, id: number, section: string, itemId: number }) {
    const params: FileUpdatePayload = { ...data };
    this.store.dispatch(new FileUpdate(params, { correlationId: guid() }));
  }

  getFiles(data: { itemId?: number, section?: string }) {
    return this.store.pipe(select(filesQuery.getFilteredFiles(data.itemId, data.section)));
  }

  /*    getFilesOrLoad(data: { itemId: number, section: string }) {
          return this.store.pipe(
              select(filesQuery.getLoading),
              tap((files: FileModel[]) => {
                  if (!files || !files.loaded) {
                      this.loadFiles(data);
                  }
                  console.log('files', files);
              })
          );
      }*/

  filesSort(files: { id: number, order: number }[]) {
    const corelParams = { correlationId: guid() };
    this.store.dispatch(new FilesSort({ files }, corelParams));
  }

  loadFiles(data: { itemId: number, section: string }) {
    const corelParams = { correlationId: guid() };
    const params = {
      ...data,
      fields: {
        fields: [],
        versions: {}
      }
    };
    return this.store.dispatch(new FilesLoad(params, corelParams));
  }
}
