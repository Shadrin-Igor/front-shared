import { Action } from '@ngrx/store';

import { AggregatableAction, CorrelationParams } from './aggregate';
import {
  SetLastRequestResult,
  SetLastRequestResultPayload
} from '@shared/state/last-request-result';
import { ModelAdapter } from '../../../../../models/src/lib/model-adapter/model-adapter';
import { RequestResultInfo } from '@shared/models/endpoints';


export abstract class Behavior {
  protected modelAdapter: ModelAdapter = ModelAdapter.createInstance();

  constructor(protected action: AggregatableAction) {
  }

  abstract resolve(): Action[];

  protected generateSetLastRequestResult(
    actionType: string,
    requestResultInfo: RequestResultInfo,
    correlationParams: CorrelationParams
  ) {
    const lastRequestResult = {
      actionType,
      ...requestResultInfo
    };

    const setLastRequestResultPayload: SetLastRequestResultPayload = {
      lastRequestResult
    };
    return new SetLastRequestResult(
      setLastRequestResultPayload,
      correlationParams
    );
  }
}
