import { AggregatableAction, CorrelationParams, FailActionForAggregation } from '../aggregate';
import { FormValidation } from '@shared/core/models/form-validation';

export enum FormValidationActionTypes {
  CheckValidation = '[FormValidation] Check Validation',
  CheckValidationSuccess = '[FormValidation] Check Validation Success',
  CheckValidationFail = '[FormValidation] Check Validation Error',
}

export interface FormValidationPayload {
  type: string,
  value: string,
  itemId?: number,
}

export class CheckValidation implements AggregatableAction {
  readonly type = FormValidationActionTypes.CheckValidation;

  constructor(public payload: FormValidationPayload, public correlationParams: CorrelationParams) {
  }
}

export class CheckValidationSuccess implements AggregatableAction {
  readonly type = FormValidationActionTypes.CheckValidationSuccess;

  constructor(public payload: FormValidation, public correlationParams: CorrelationParams) {
  }
}

export class CheckValidationFail implements FailActionForAggregation {
  readonly type = FormValidationActionTypes.CheckValidationFail;

  constructor(public payload: FormValidationPayload, public error: any, public correlationParams: CorrelationParams) {
  }
}

export type FormValidationAction =
  | CheckValidation
  | CheckValidationSuccess
  | CheckValidationFail;
