import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { formValidationQuery } from './form-validation.selectors';
import { guid } from '@shared/core/utils';
import { CheckValidation } from '@shared/state/form-validation/form-validation.actions';
import { FormValidationStateData } from '@shared/state/form-validation/form-validation.reducer';
import { Observable } from 'rxjs';
import { FormValidation } from '@shared/core/models/form-validation';

@Injectable()
export class FormValidationFacade {
  constructor(private store: Store<FormValidationStateData>) {
  }

  sendValidation(type: string, value: string, itemId?: number): void {
    const params = { correlationId: guid() };
    this.store.dispatch(new CheckValidation({ type, value, itemId }, params));
  }

  getValidation(type: string, value: string): Observable<FormValidation> {
    return this.store.pipe(select(formValidationQuery.getFormValidation(type, value)));
  }
}
