import { ActionReducer } from '@ngrx/store';
import {
  FormValidationAction,
  FormValidationActionTypes
} from '@shared/state/form-validation/form-validation.actions';
import { FormValidation } from '@shared/core/models/form-validation';
import { ReducerService } from '@shared/core/services/reducer/reducer.service';

export const FORM_VALIDATION_KEY = 'formValidation';

export interface FormValidationState {
  list: FormValidation[];
  ids: string[];
}

export interface FormValidationStateData {
  formValidation: FormValidationState
}

export interface FormValidationPartialState {
  readonly [FORM_VALIDATION_KEY]: FormValidationState;
}

export const initialFormValidationState = {
  list: [],
  ids: []
};

export function formValidationReducer(
  state: FormValidationState = initialFormValidationState,
  action: FormValidationAction
): FormValidationState {
  switch (action.type) {
    case FormValidationActionTypes.CheckValidation: {
      const { type, value, itemId } = action.payload;
      const id = `${type}${value}`;
      state = {
        ...ReducerService.upsertOne(state, {
          id,
          type,
          value,
          itemId,
          loaded: false
        })
      };
      break;
    }
    case FormValidationActionTypes.CheckValidationSuccess: {
      const { type, value, isValid, itemId } = action.payload;
      const id = `${type}${value}`;
      state = {
        ...ReducerService.upsertOne(state, {
          id,
          type,
          value,
          isValid,
          itemId,
          loaded: true
        })
      };
      break;
    }
  }
  return state;
}

export function initFormValidationReducer(
  reducer: ActionReducer<any>
): ActionReducer<any> {
  return function (state, action) {
    return reducer(state, action);
  };
}
