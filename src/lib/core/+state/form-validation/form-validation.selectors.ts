import { createFeatureSelector, createSelector } from '@ngrx/store';
import { FormValidation } from '@shared/core/models/form-validation';
import {
  FORM_VALIDATION_KEY,
  FormValidationState
} from '@shared/state/form-validation/form-validation.reducer';

const getFormValidationState = createFeatureSelector<FormValidationState>(FORM_VALIDATION_KEY);

export const getFormValidation = (type: string, value: string) =>
  createSelector(
    getFormValidationState,
    (state: FormValidationState) => {
      return state.list.find(item => item.type === type && item.value === value);
    }
  );

export const formValidationQuery = {
  getFormValidation
};
