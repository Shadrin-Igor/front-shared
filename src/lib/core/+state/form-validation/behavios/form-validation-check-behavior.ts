import { Behavior } from '../../behavior';
import { HttpClientResult } from '@shared/core/index';
import { SetLastRequestResult } from '../../last-request-result';
import { BaseResponse, ResponseStatus } from '@shared/models/index';
import {
  CheckValidation,
  CheckValidationFail,
  CheckValidationSuccess
} from '@shared/state/form-validation/form-validation.actions';

export class FormValidationCheckBehavior extends Behavior {
  constructor(
    protected action: CheckValidation,
    protected result: HttpClientResult<BaseResponse>
  ) {
    super(action);
  }

  resolve(): Array<CheckValidationSuccess | CheckValidationFail | SetLastRequestResult> {
    const status = this.result.data.status;
    const correlationParams = this.action.correlationParams;
    let returnedActions: Array<CheckValidationSuccess | CheckValidationFail | SetLastRequestResult>;
    const setLastRequestResponseAction = this.generateSetLastRequestResult(
      this.action.type,
      this.result.info,
      this.action.correlationParams
    );
    const { type, value, itemId } = this.action.payload;

    switch (status) {
      case ResponseStatus.STATUS_200: {
        const response = this.result.data;
        const { isValid } = response.body.data;
        returnedActions = [
          new CheckValidationSuccess({ type, value, itemId, isValid }, correlationParams)
        ];
        break;
      }
      case ResponseStatus.STATUS_400:
      case ResponseStatus.STATUS_404:
      case ResponseStatus.STATUS_422:
      case ResponseStatus.STATUS_401:
        const errorResponse = this.result.data;
        const error = errorResponse.body.errors[0].title;
        returnedActions = [
          new CheckValidationFail({ type, value }, { error }, correlationParams)
        ];
        break;
      default: {
        returnedActions = [
          new CheckValidationFail({ type, value }, { error: 'Unhandled error' }, correlationParams)
        ];
      }
    }

    return [setLastRequestResponseAction, ...returnedActions];
  }
}
