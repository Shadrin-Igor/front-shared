import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { flatMap, map, mergeMap, switchMap } from 'rxjs/operators';
import { Store } from '@ngrx/store';

import { HttpClientResult } from '@shared/core/index';
import { BaseEffects } from '@shared/state/base.effects';
import { LoginSendCreateResponse } from '@shared/models/endpoints/auth';
import { CoreState } from '../../../../../../core/src/lib/+state/core.reducer';
import {
  CheckValidation,
  CheckValidationFail, CheckValidationSuccess,
  FormValidationActionTypes
} from '@shared/state/form-validation/form-validation.actions';
import { FormValidationCheckBehavior } from '@shared/state/form-validation/behavios/form-validation-check-behavior';
import { FormValidationService } from '@shared/core/services/form-validation/form-validation.service';
import { AuthenticationService } from '@shared/core/services/auth/authentication.service';
import { BaseResponse } from '@shared/models/endpoints';

@Injectable()
export class FormValidationEffects extends BaseEffects {
  @Effect()
  checkValidation$ = this.effect(FormValidationActionTypes.CheckValidation, {
    operatorFunctions: (action: CheckValidation) => [
      switchMap(() => {
        const { type, value, itemId } = action.payload;
        const headers = this.authenticationService.loadAuthDataFromLocalStore();
        return this.formValidationService.formValidationCheck({
          type,
          value,
          itemId
        });
      }),
      flatMap((r: HttpClientResult<BaseResponse>) => new FormValidationCheckBehavior(action, r).resolve())
    ],
    onError: (action: CheckValidation, error) => {
      return new CheckValidationFail({
        type: action.payload.type,
        value: action.payload.value
      }, error, action.correlationParams);
    }
  });

  constructor(
    protected actions$: Actions,
    protected store: Store<CoreState>,
    protected formValidationService: FormValidationService,
    protected authenticationService: AuthenticationService,
  ) {
    super(actions$, store);
  }
}
