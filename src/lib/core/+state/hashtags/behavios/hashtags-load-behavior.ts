import { Behavior } from '../../behavior';
import { HttpClientResult } from '@shared/core/index';
import { SetLastRequestResult } from '../../last-request-result';
import { BaseResponse, ResponseStatus } from '@shared/models/index';

import {
  HashtagsLoad,
  HashtagsLoadFail,
  HashtagsLoadSuccess
} from '../../hashtags/hashtags.actions';

import { HashtagModel } from '@shared/models/models/hashtag';
import { HashtagsLoad200 } from '@shared/models/endpoints/hashtags';

export class HashtagsLoadBehavior extends Behavior {
  constructor(
    protected action: HashtagsLoad,
    protected result: HttpClientResult<BaseResponse>
  ) {
    super(action);
  }

  resolve(): Array<HashtagsLoadSuccess | HashtagsLoadFail | SetLastRequestResult> {
    const status = this.result.data.status;
    const correlationParams = this.action.correlationParams;
    let returnedActions: Array<HashtagsLoadSuccess | HashtagsLoadFail | SetLastRequestResult>;
    const setLastRequestResponseAction = this.generateSetLastRequestResult(
      this.action.type,
      this.result.info,
      this.action.correlationParams
    );
    switch (status) {
      case ResponseStatus.STATUS_200: {
        const response: HashtagsLoad200 = this.result.data;
        const { data, meta } = response.body;
        const hashtagsData: HashtagModel[] = this.modelAdapter.convertItems<HashtagModel, HashtagModel>(HashtagModel, data);

        returnedActions = [
          new HashtagsLoadSuccess({ hashtags: hashtagsData, meta }, correlationParams)
        ];

        break;
      }
      case ResponseStatus.STATUS_400:
      case ResponseStatus.STATUS_404:
      case ResponseStatus.STATUS_422:
      case ResponseStatus.STATUS_401:
        const errorResponse = this.result.data;
        const error = errorResponse.body.errors ? errorResponse.body.errors[0].title : '';
        returnedActions = [
          new HashtagsLoadFail({}, { error }, correlationParams)
        ];
        break;
      default: {
        returnedActions = [
          new HashtagsLoadFail({}, { error: 'Unhandled error' }, correlationParams)
        ];
      }

    }
    return [setLastRequestResponseAction, ...returnedActions];
  }
}
