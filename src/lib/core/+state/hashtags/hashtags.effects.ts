import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { flatMap, switchMap } from 'rxjs/operators';
import { Store } from '@ngrx/store';

import { HttpClientResult } from '@shared/core/index';
import { BaseEffects } from '@shared/state/base.effects';
import {
  HashtagsActionTypes,
  HashtagsLoad,
  HashtagsLoadFail
} from '@shared/state/hashtags/hashtags.actions';
import { HashtagsLoadBehavior } from '@shared/state/hashtags/behavios/hashtags-load-behavior';
import { LoginSendCreateResponse } from '@shared/models/endpoints/auth';
import { HashtagsService } from '@shared/core/services/model-services/hashtags/hashtags.service';
import { AuthenticationService } from '@shared/core/services/auth/authentication.service';
import { CoreState } from '../../../../../../core/src/lib/+state/core.reducer';

@Injectable()
export class HashtagsEffects extends BaseEffects {
  @Effect()
  hashtagsLoad$ = this.effect(HashtagsActionTypes.HashtagsLoad, {
    operatorFunctions: (action: HashtagsLoad) => [
      switchMap(() => {
        const { where, fields, perPage } = action.payload;
        const headers = this.authenticationService.loadAuthDataFromLocalStore();
        return this.hashtagsService.hashtagsLoad({
          where,
          fields,
          perPage,
          headers: { token: headers.token, uid: headers.id }
        });
      }),
      flatMap((r: HttpClientResult<LoginSendCreateResponse>) => new HashtagsLoadBehavior(action, r).resolve())
    ],
    onError: (action: HashtagsLoad, error) => {
      return new HashtagsLoadFail({}, error, action.correlationParams);
    }
  });

  constructor(
    protected actions$: Actions,
    protected store: Store<CoreState>,
    protected hashtagsService: HashtagsService,
    protected authenticationService: AuthenticationService
  ) {
    super(actions$, store);
  }
}
