import {Injectable} from '@angular/core';
import {filter} from 'rxjs/operators';
import {select, Store} from '@ngrx/store';

import { guid } from '@shared/core/utils';
import {Observable} from 'rxjs';
import { hashtagsQuery } from '@shared/state/hashtags/hashtags.selectors';
import { HashtagsPartialState } from '@shared/state/hashtags/hashtags.reducer';
import { HashtagsLoad } from '@shared/state/hashtags/hashtags.actions';
import { HashtagModel } from '@shared/models/models/hashtag';

@Injectable()
export class HashtagsFacade {
  loaded$ = this.store.pipe(select(hashtagsQuery.getLoaded));
  getHashtags$ = this.store.pipe(select(hashtagsQuery.getHashtags));
  selected$ = this.store.pipe(select(hashtagsQuery.geSelected));

  constructor(private store: Store<HashtagsPartialState>) {
  }

  loadHashtags(where = {}, fields = {fields: ['name', 'slug']}, perPage?: number) {
    this.store.dispatch(new HashtagsLoad({where, fields, perPage}, {correlationId: guid()}));
  }

  getHashtags(slugOrId: string | number): Observable<HashtagModel> {
    return this.store.pipe(select(hashtagsQuery.getHashtag(slugOrId)));
  }

  waitLoading() {
    return this.store.pipe(
      select(hashtagsQuery.getLoaded),
      filter((item) => !!item)
    );
  }
}
