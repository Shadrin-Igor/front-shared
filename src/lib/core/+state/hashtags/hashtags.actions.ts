import {
  AggregatableAction,
  CorrelationParams,
  FailActionForAggregation
} from '@shared/state/aggregate';
import { HashtagModel } from '@shared/models/models/hashtag/hashtagModel';
import { MetaModel } from '@shared/models/index';

export enum HashtagsActionTypes {
  HashtagsLoad = '[Hashtags] Hashtags Load',
  HashtagsLoadSuccess = '[Hashtags] Hashtags Load Success',
  HashtagsLoadFail = '[Hashtags] Hashtags Load Error',

  HashtagsSetLoading = '[Hashtags] Hashtags Set Loading'
}

export interface HashtagsLoadPayload {
  where: { id?: number };
  fields?: any;
  perPage?: number;
}

export class HashtagsLoad implements AggregatableAction {
  readonly type = HashtagsActionTypes.HashtagsLoad;

  constructor(public payload: HashtagsLoadPayload, public correlationParams: CorrelationParams) {
  }
}

export interface HashtagsLoadSuccessPayload {
  hashtags: HashtagModel[];
  meta?: MetaModel;
}

export interface EmptyPayload {
}

export class HashtagsLoadSuccess implements AggregatableAction {
  readonly type = HashtagsActionTypes.HashtagsLoadSuccess;

  constructor(public payload: HashtagsLoadSuccessPayload, public correlationParams: CorrelationParams) {
  }
}

export class HashtagsLoadFail implements FailActionForAggregation {
  readonly type = HashtagsActionTypes.HashtagsLoadFail;

  constructor(public payload: EmptyPayload, public error: any, public correlationParams: CorrelationParams) {
  }
}

export class HashtagsSetLoading implements AggregatableAction {
  readonly type = HashtagsActionTypes.HashtagsSetLoading;

  constructor(public payload: EmptyPayload, public correlationParams: CorrelationParams) {
  }
}

export type HashtagsAction =
  | HashtagsLoad
  | HashtagsLoadSuccess
  | HashtagsLoadFail
  | HashtagsSetLoading;
