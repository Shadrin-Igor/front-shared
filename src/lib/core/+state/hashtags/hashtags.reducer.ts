import { ReducerService } from '@shared/core/services/reducer/reducer.service';
import { HashtagModel } from '@shared/models/models/hashtag';
import { CATEGORIES_FEATURE_KEY } from '@shared/state/feature.selectors';
import { HashtagsAction, HashtagsActionTypes } from '@shared/state/hashtags/hashtags.actions';

/**
 * Interface for the 'Hashtags' data used in
 *  - HashtagsState, and
 *  - usersReducer
 *
 *  Note: replace if already defined in another module
 */

export interface HashtagsState {
  list: HashtagModel[];
  ids: string[];
  selected: number;
  loaded: boolean; // has the Hashtags list been loaded
  error?: any; // last none error (if any)
}

export interface HashtagsStateData {
  hashtags: HashtagsState;
}

export interface HashtagsPartialState {
  readonly [CATEGORIES_FEATURE_KEY]: HashtagsStateData;
}

export const initialHashtagState = {
  list: [],
  ids: [],
  selected: null,
  loaded: false
};

export function hashtagsReducer(
  state: HashtagsState = initialHashtagState,
  action: HashtagsAction
): HashtagsState {
  switch (action.type) {
    case HashtagsActionTypes.HashtagsLoad: {
      state = {
        ...state,
        loaded: false
      };
      break;
    }

    case HashtagsActionTypes.HashtagsLoadSuccess: {
      state = {
        ...ReducerService.upsertMeny(state, action.payload.hashtags),
        loaded: true
      };
      break;
    }
  }
  return state;
}
