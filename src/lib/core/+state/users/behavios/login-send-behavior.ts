import { Behavior } from '../../behavior';
import { HttpClientResult } from '@shared/core/index';
import {
  LoginSend,
  LoginSendFail,
  LoginSendSuccess
} from '../../users/users.actions';
import { SetLastRequestResult } from '../../last-request-result';
import { BaseResponse, ResponseStatus } from '@shared/models/index';
import { UserModel } from '@shared/models/models/user/user-model';
import { LoginSendCreate201 } from '@shared/models/endpoints/auth';

;

export class LoginSendBehavior extends Behavior {
  constructor(
    protected action: LoginSend,
    protected result: HttpClientResult<BaseResponse>
  ) {
    super(action);
  }

  resolve(): Array<LoginSendSuccess | LoginSendFail | SetLastRequestResult> {
    const status = this.result.data.status;
    const correlationParams = this.action.correlationParams;
    let returnedActions: Array<LoginSendSuccess | LoginSendFail | SetLastRequestResult>;
    const setLastRequestResponseAction = this.generateSetLastRequestResult(
      this.action.type,
      this.result.info,
      this.action.correlationParams
    );
    switch (status) {
      case ResponseStatus.STATUS_200: {
        const response: LoginSendCreate201 = this.result.data;
        const { user } = response.body;
        const userData: UserModel = this.modelAdapter.convertItem<UserModel, UserModel>(UserModel, user);
        returnedActions = [
          new LoginSendSuccess({ user: userData }, correlationParams)
        ];
        break;
      }
      case ResponseStatus.STATUS_400:
      case ResponseStatus.STATUS_404:
      case ResponseStatus.STATUS_422:
      case ResponseStatus.STATUS_401:
        const errorResponse = this.result.data;
        const error = errorResponse.body.errors[0].title;
        returnedActions = [
          new LoginSendFail({}, { error }, correlationParams)
        ];
        break;
      default: {
        returnedActions = [
          new LoginSendFail({}, { error: 'Unhandled error' }, correlationParams)
        ];
      }

    }
    return [setLastRequestResponseAction, ...returnedActions];
  }
}
