import { Behavior } from '../../behavior';
import { HttpClientResult } from '@shared/core/index';
import {
  RefreshToken,
  RefreshTokenFail,
  RefreshTokenSuccess
} from '../../users/users.actions';
import { SetLastRequestResult } from '@shared/state/last-request-result';
import { BaseResponse, ResponseStatus } from '@shared/models/index';
import { UserModel } from '@shared/models/models/user/user-model';
import { RefreshToken200 } from '@shared/models/endpoints/auth/refresh-token';


export class RefreshTokenBehavior extends Behavior {
  constructor(
    protected action: RefreshToken,
    protected result: HttpClientResult<BaseResponse>
  ) {
    super(action);
  }

  resolve(): Array<RefreshTokenSuccess | RefreshTokenFail | SetLastRequestResult> {
    const status = this.result.data.status;
    const correlationParams = this.action.correlationParams;
    let returnedActions: Array<RefreshTokenSuccess | RefreshTokenFail | SetLastRequestResult>;
    const setLastRequestResponseAction = this.generateSetLastRequestResult(
      this.action.type,
      this.result.info,
      this.action.correlationParams
    );
    switch (status) {
      case ResponseStatus.STATUS_200: {
        const response: RefreshToken200 = this.result.data;
        const { user } = response.body;
        const userData: UserModel = this.modelAdapter.convertItem<UserModel, UserModel>(UserModel, user);
        returnedActions = [
          new RefreshTokenSuccess({
            token: userData.token,
            refreshToken: userData.refreshToken,
            tokenExp: userData.tokenExp
          }, correlationParams)
        ];
        break;
      }
      case ResponseStatus.STATUS_400:
      case ResponseStatus.STATUS_404:
      case ResponseStatus.STATUS_422:
      case ResponseStatus.STATUS_401:
        const errorResponse = this.result.data;
        const error = errorResponse.body.errors[0].title;
        returnedActions = [
          new RefreshTokenFail({}, { error }, correlationParams)
        ];
        break;
      default: {
        returnedActions = [
          new RefreshTokenFail({}, { error: 'Unhandled error' }, correlationParams)
        ];
      }

    }
    return [setLastRequestResponseAction, ...returnedActions];
  }
}
