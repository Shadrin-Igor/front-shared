import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { flatMap, map, switchMap, tap } from 'rxjs/operators';
import { Store } from '@ngrx/store';

import { HttpClientResult } from '@shared/core/index';
import { BaseEffects } from '@shared/state/base.effects';
import {
  LoginSend,
  LoginSendFail,
  LoginSendSuccess,
  RefreshToken,
  RefreshTokenFail, RefreshTokenSuccess,
  UsersActionTypes
} from './users.actions';
import { LoginSendBehavior } from '@shared/state/users/behavios/login-send-behavior';
import { RefreshTokenBehavior } from '@shared/state/users/behavios/refresh-token-behavior';
import {
  LoginSendCreateRequestConfig,
  LoginSendCreateResponse
} from '@shared/models/endpoints/auth';
import {
  RefreshTokenRequestConfig,
  RefreshTokenResponse
} from '@shared/models/endpoints/auth/refresh-token';
import { UsersService } from '@shared/core/services/users/users.service';
import { AuthenticationService } from '@shared/core/services/auth/authentication.service';
import { RouterService } from '@shared/core/services/router/router.service';
import { CoreState } from '../../../../../../core/src/lib/+state/core.reducer';

@Injectable()
export class UsersEffects extends BaseEffects {
  @Effect()
  loginSend$ = this.effect(UsersActionTypes.LoginSend, {
    operatorFunctions: (action: LoginSend) => [
      switchMap(() => {
        const { email, password, remember } = action.payload;
        const config: LoginSendCreateRequestConfig = { email, password };
        return this.usersService.loginSend(config);
      }),
      flatMap((r: HttpClientResult<LoginSendCreateResponse>) => new LoginSendBehavior(action, r).resolve())
    ],
    onError: (action: LoginSend, error) => {
      return new LoginSendFail({}, error, action.correlationParams);
    }
  });

  @Effect({ dispatch: false })
  loginSendSuccess$ = this.effect(UsersActionTypes.LoginSendSuccess, {
    operatorFunctions: (action: LoginSendSuccess) => [
      map(() => {
        this.authenticationService.saveAuthInfo(action.payload.user);
        const redirectUrl = this.authenticationService.getRedirectToUrl();
        this.routerService.goTo(redirectUrl ? [redirectUrl] : '/');
      })
    ]
  });

  @Effect()
  refreshToken$ = this.effect(UsersActionTypes.RefreshToken, {
    operatorFunctions: (action: RefreshToken) => [
      switchMap(() => {
        const { refreshToken } = action.payload;
        const config: RefreshTokenRequestConfig = { refreshToken };
        return this.usersService.refreshToken(config);
      }),
      flatMap((r: HttpClientResult<RefreshTokenResponse>) => new RefreshTokenBehavior(action, r).resolve())
    ],
    onError: (action: RefreshToken, error) => {
      return new RefreshTokenFail({}, error, action.correlationParams);
    }
  });

  @Effect({ dispatch: false })
  refreshTokenSuccess$ = this.effect(UsersActionTypes.RefreshTokenSuccess, {
    operatorFunctions: (action: RefreshTokenSuccess) => {
      return [
        tap(() => {
          const { token, refreshToken, tokenExp } = action.payload;
          this.authenticationService.saveRefreshToken(token, tokenExp, refreshToken);
        })
      ];
    }
  });

  constructor(
    protected actions$: Actions,
    protected store: Store<CoreState>,
    protected usersService: UsersService,
    protected authenticationService: AuthenticationService,
    protected routerService: RouterService
  ) {
    super(actions$, store);
  }
}
