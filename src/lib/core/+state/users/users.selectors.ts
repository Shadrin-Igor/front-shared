import { createFeatureSelector, createSelector } from '@ngrx/store';
import { USERS_FEATURE_KEY, UsersState } from './users.reducer';
import { UserModel } from '@shared/models/models/user/user-model';

// Lookup the 'Users' feature state managed by NgRx
const getUsersState = createFeatureSelector<UsersState>(USERS_FEATURE_KEY);

const getLoaded = createSelector(
  getUsersState,
  (state: UsersState) => state.loaded
);
const getError = createSelector(
  getUsersState,
  (state: UserModel) => {
    return state
  }
);

const getUser = createSelector(
  getUsersState,
  (state: UsersState) => {
    return state.token ? state : null;
  }
);

export const getUserAuthData = createSelector(
  getUsersState,
  (userSate: UserModel) => {
    return userSate && { token: userSate.token, uid: userSate.id };
  }
);

export const usersQuery = {
  getLoaded,
  getError,
  getUser,
};
