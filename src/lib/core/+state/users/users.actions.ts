import { AggregatableAction, CorrelationParams, FailActionForAggregation } from '../aggregate';
import { UserModel } from '@shared/models/models/user/user-model';

export enum UsersActionTypes {
  LoadUsers = '[Users] Load Users',
  UsersLoaded = '[Users] Users Loaded',
  UsersLoadError = '[Users] Users Load Error',

  LoginSend = '[Users] Auth Send',
  LoginSendSuccess = '[Users] Auth Send Success',
  LoginSendError = '[Users] Auth Send Error',

  RefreshToken = '[Users] Refresh Token',
  RefreshTokenSuccess = '[Users] Refresh Token Success',
  RefreshTokenError = '[Users] Refresh TokenError',

  SetLoaded = '[Users] Set Loaded'
}

export interface EmptyPayload {};

export class LoadUsers implements AggregatableAction {
  readonly type = UsersActionTypes.LoadUsers;
  constructor(public payload: EmptyPayload, public correlationParams: CorrelationParams) { }
}

export class UsersLoadFail implements FailActionForAggregation {
  readonly type = UsersActionTypes.UsersLoadError;
  constructor(public payload: EmptyPayload, public error: any, public correlationParams: CorrelationParams) { }
}

export class UsersLoaded implements AggregatableAction {
  readonly type = UsersActionTypes.UsersLoaded;
  constructor(public payload: any, public correlationParams: CorrelationParams) { }
}

export interface LoginSendPayment {
  email: string,
  password: string,
  remember?: boolean
}

export class LoginSend implements AggregatableAction {
  readonly type = UsersActionTypes.LoginSend;

  constructor(public payload: LoginSendPayment, public correlationParams: CorrelationParams) { }
}

export interface LoginSendSuccessPayment {
  user: UserModel
}

export class LoginSendSuccess implements AggregatableAction {
  readonly type = UsersActionTypes.LoginSendSuccess;
  constructor(public payload: LoginSendSuccessPayment, public correlationParams: CorrelationParams) { }
}

export class LoginSendFail implements FailActionForAggregation {
  readonly type = UsersActionTypes.LoginSendError;
  constructor(public payload: EmptyPayload, public error: any, public correlationParams: CorrelationParams) { }
}

export interface RefreshTokenPayment {
  refreshToken: string
}

export class RefreshToken implements AggregatableAction {
  readonly type = UsersActionTypes.RefreshToken;

  constructor(public payload: RefreshTokenPayment, public correlationParams: CorrelationParams) { }
}

export interface RefreshTokenSuccessPayment {
  token: string,
  refreshToken: string,
  tokenExp: number
}

export class RefreshTokenSuccess implements AggregatableAction {
  readonly type = UsersActionTypes.RefreshTokenSuccess;
  constructor(public payload: RefreshTokenSuccessPayment, public correlationParams: CorrelationParams) { }
}

export class RefreshTokenFail implements FailActionForAggregation {
  readonly type = UsersActionTypes.RefreshTokenError;
  constructor(public payload: EmptyPayload, public error: any, public correlationParams: CorrelationParams) { }
}

// export type UsersAction = LoadUsers | UsersLoaded | UsersLoadFail;

export class SetLoaded implements AggregatableAction {
  readonly type = UsersActionTypes.SetLoaded;

  constructor(public payload: {}, public correlationParams: CorrelationParams) { }
}

export type UsersAction =
  | LoadUsers
  | UsersLoaded
  | UsersLoadFail
  | LoginSend
  | LoginSendSuccess
  | LoginSendFail
  | RefreshToken
  | RefreshTokenSuccess
  | RefreshTokenFail
  | SetLoaded;
