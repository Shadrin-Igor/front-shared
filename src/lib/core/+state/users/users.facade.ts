import { Injectable } from '@angular/core';

import { select, Store } from '@ngrx/store';

import { UsersPartialState } from './users.reducer';
import { usersQuery } from './users.selectors';
import { LoadUsers, LoginSend, LoginSendPayment, RefreshToken, SetLoaded } from './users.actions';
import { guid } from '@shared/core/utils';

@Injectable()
export class UsersFacade {
  loaded$ = this.store.pipe(select(usersQuery.getLoaded));
  getUser$ = this.store.pipe(select(usersQuery.getUser));

  constructor(private store: Store<UsersPartialState>) {}

  loadAll() {
    const params = { correlationId: guid() };
    this.store.dispatch(new LoadUsers({}, params));
  }

  sendLogin({email, password, remember}) {
    const corelParams = { correlationId: guid() };
    const params: LoginSendPayment = {
      email, password, remember
    };

    this.store.dispatch(new LoginSend(params, corelParams));
  }

  logOut() {
    console.log('logOut');
  }

  refreshToken(refreshToken: string) {
    console.log('refreshToken');
    this.store.dispatch(new RefreshToken({refreshToken},  { correlationId: guid() }));
  }

  setLoaded() {
    this.store.dispatch(new SetLoaded({},  { correlationId: guid() }));
  }
}
