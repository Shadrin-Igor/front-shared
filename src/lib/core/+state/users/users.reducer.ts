import { UsersAction, UsersActionTypes } from './users.actions';
import { ActionReducer } from '@ngrx/store';

import { UserModel } from '@shared/models/models/user/user-model';

export const USERS_FEATURE_KEY = 'user';

/**
 * Interface for the 'Users' data used in
 *  - UsersState, and
 *  - usersReducer
 *
 *  Note: replace if already defined in another module
 */

export interface UsersState extends UserModel {
  loaded: boolean; // has the Users list been loaded
  error?: any; // last none error (if any)
}

export interface UsersStateData {
  user: UsersState
}

export interface UsersPartialState {
  readonly [USERS_FEATURE_KEY]: UsersStateData;
}

export const initialUserState = {
  loaded: false
};

export function usersReducer(
  state: UsersState = initialUserState,
  action: UsersAction
): UsersState {
  switch (action.type) {
    case UsersActionTypes.UsersLoaded: {
      state = {
        ...state,
        ...action.payload,
        loaded: true
      };
      break;
    }
    case UsersActionTypes.RefreshToken:
    case UsersActionTypes.SetLoaded: {
      console.log('RefreshToken');
      state = {
        ...state,
        loaded: false
      };
      break;
    }
    case UsersActionTypes.RefreshTokenSuccess: {
      console.log('RefreshTokenSuccess');
      const { token, refreshToken, tokenExp } = action.payload;
      state = {
        ...state,
        // data: { ...state.data, token, refreshToken, tokenExp },
        token,
        refreshToken,
        tokenExp,
        loaded: true
      };
      break;
    }
  }
  return state;
}

export function initUserReducer(
  reducer: ActionReducer<any>
): ActionReducer<any> {
  return function (state, action) {
    const newState = reducer(state, action);
    /*    if ([INIT.toString(), UPDATE.toString()].includes(action.type)) {
          const authData: UserModel = authenticationService.loadAuthDataFromLocalStore();
          const userState = { user: { ...authData } };
          return { ...newState, ...userState };
        }*/
    return newState;
  };
}
