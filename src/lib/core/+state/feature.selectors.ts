import { createFeatureSelector } from '@ngrx/store';
import { EntityState } from '@ngrx/entity';
import { FormGroupState } from 'ngrx-forms';
//import { FormValue } from '@shared/core/index';

// export const formsFeatureState = createFeatureSelector<EntityState<FormGroupState<FormValue>>>('forms');

export const CATEGORIES_FEATURE_KEY = 'categories';
export const HASHTAGS_FEATURE_KEY = 'hashtags';
export const MAIN_FEATURE_KEY = 'main';
