import {
  LastRequestResultActions,
  LastRequestResultActionTypes
} from './last-request-result.actions';

export interface LastRequestResultData {
  actionType: string;
  isValid: boolean;
  message: string;
}

export interface LastRequestResultState {
  readonly lastRequestResult: LastRequestResultData;
}

export const initialState: LastRequestResultData = {
  actionType: '',
  isValid: true,
  message: ''
};

export function lastRequestResultReducer(
  state = initialState,
  action: LastRequestResultActions
): LastRequestResultData {
  switch (action.type) {
    case LastRequestResultActionTypes.SetLastRequestResult:
      const lastRequestResult = action.payload.lastRequestResult;
      return { ...state, ...lastRequestResult };

    default:
      return state;
  }
}
