import { AggregatableAction, CorrelationParams } from '../aggregate';

export enum LastRequestResultActionTypes {
  SetLastRequestResult = '[LastRequestResult] Set Last Request Result'
}

export interface SetLastRequestResultPayload {
  lastRequestResult: {
    actionType: string;
    isValid: boolean;
    message: string;
  };
}

export class SetLastRequestResult implements AggregatableAction {
  readonly type = LastRequestResultActionTypes.SetLastRequestResult;
  constructor(
    public payload: SetLastRequestResultPayload,
    public corrlationParams?: CorrelationParams
  ) { }
}

export type LastRequestResultActions = SetLastRequestResult;
