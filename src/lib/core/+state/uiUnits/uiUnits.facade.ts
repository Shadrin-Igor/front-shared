import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';

import { guid } from '@shared/core/utils';
import { UiUnitsSet } from '@shared/state/uiUnits/uiUnits.actions';
import { getUIUnit } from '@shared/state/uiUnits/uiUnits.selectors';

@Injectable()
export class UiUnitsFacade {

  constructor(private store: Store<any>) {
  }

  setUIUnits(id: string, data: any) {
    this.store.dispatch(new UiUnitsSet({ id, data }, { correlationId: guid() }));
  }

  getUIUnits(id: string) {
    return this.store.pipe(select(getUIUnit(id)));
  }

}
