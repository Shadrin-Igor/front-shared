import { UiUnitsAction, UiUnitsActionTypes } from './uiUnits.actions';
import { ReducerService } from '@shared/core/services/reducer/reducer.service';
import { UiUnitItem } from '@shared/models/ui-units/uiUnitItem';

export const UIUNITS_FEATURE_KEY = 'uiunits';

/**
 * Interface for the 'UiUnits' data used in
 *  - UiUnitsState, and
 *  - usersReducer
 *
 *  Note: replace if already defined in another module
 */

export interface UiUnitsState {
  list: UiUnitItem[];
  ids: string[];
}

export interface UiUnitsStateData {
  uiunits: UiUnitsState;
}

export interface UiUnitsPartialState {
  readonly [UIUNITS_FEATURE_KEY]: UiUnitsStateData;
}

export const initialUiUnitsState = {
  list: [],
  ids: []
};

export function uiUnitsReducer(
  state: UiUnitsState = initialUiUnitsState,
  action: UiUnitsAction
): UiUnitsState {
  switch (action.type) {
    case UiUnitsActionTypes.UiUnitsSet: {
      state = {
        ...ReducerService.upsertOne(state, { data: action.payload.data, id: action.payload.id })
      };
      break;
    }
  }
  return state;
}
