import { Injectable } from '@angular/core';
import { Actions } from '@ngrx/effects';
import { Store } from '@ngrx/store';

import { BaseEffects } from '@shared/state/base.effects';
import { UiUnitsFacade } from '@shared/state/uiUnits';
import { CoreState } from '../../../../../../core/src/lib/+state/core.reducer';

@Injectable()
export class UiUnitsEffects extends BaseEffects {

  /*  @Effect({dispatch: false})
    ImagesLoadSuccess$ = this.effect(ImagesActionTypes.ImagesLoadSuccess, {
      operatorFunctions: (action: ImagesLoadSuccess) => [
        tap(() => {
          this.uiUnitsFacade.setUIUnits(UiUnitIds.FIRMS, action.payload.meta);
        })
      ],
      onError: (action: ImagesLoad, error) => {
        return new ImagesLoadFail({}, error, action.correlationParams);
      }
    });*/

  constructor(
    protected actions$: Actions,
    protected store: Store<CoreState>,
    protected uiUnitsFacade: UiUnitsFacade
  ) {
    super(actions$, store);
  }
}
