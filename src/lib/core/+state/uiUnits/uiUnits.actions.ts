import {AggregatableAction, CorrelationParams} from '../aggregate';

export enum UiUnitsActionTypes {
  UiUnitsSet = '[UiUnits] UiUnits Load'
}

export interface UiUnitsSetPayload {
  id: string;
  data: any;
}

export class UiUnitsSet implements AggregatableAction {
  readonly type = UiUnitsActionTypes.UiUnitsSet;

  constructor(public payload: UiUnitsSetPayload, public correlationParams: CorrelationParams) {
  }
}

export type UiUnitsAction =
  | UiUnitsSet;
