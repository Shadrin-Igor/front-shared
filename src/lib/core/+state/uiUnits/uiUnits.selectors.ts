import { createFeatureSelector, createSelector } from '@ngrx/store';
import { UIUNITS_FEATURE_KEY, UiUnitsState } from '@shared/state/uiUnits/uiUnits.reducer';
import { UiUnitItem } from '@shared/models/ui-units/uiUnitItem';

export const getAllUiUnits = createFeatureSelector<UiUnitsState>(UIUNITS_FEATURE_KEY);

export const getUIUnit = (id: string) =>
  createSelector(
    getAllUiUnits,
    (state: UiUnitsState) => {
      const item: UiUnitItem = state.list.find((item) => item.id === id);
      return item ? item.data : null;
    }
  );

export const getUiUnitDataById = (id: string) =>
  createSelector(
    getAllUiUnits,
    (state: UiUnitsState) => {
      const item: UiUnitItem = state.list.find((item) => item.id === id);
      return item ? item.data : null;
    }
  );

/*export const filterUiUnitIdsByType = (type: string) =>
  createSelector(
    getAllUiUnits,
    (state: UiUnitsState) => {
      return state.list
        .filter((item) => item.type === type)
        .map(item => item.id);
    }
  );

export const getUiUnitEntitiesByType = (type: string) =>
  createSelector(
    getAllUiUnits,
    (state: UiUnitsState) => {
      return state.list
        .filter((item) => item.type === type)
        .map(item => item.id);
    }
  );*/

export const uiUnitsQuery = {
  getUIUnit
};
