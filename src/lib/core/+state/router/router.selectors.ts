import { createFeatureSelector, createSelector } from '@ngrx/store';
import { ROUTER_FEATURE_KEY, RouterState } from '@shared/state/router/router.reducer';

const getRouterState = createFeatureSelector<RouterState>(ROUTER_FEATURE_KEY);

export const getRouterUrl = createSelector(
  getRouterState,
  (state: RouterState) => {
    return state && state.router.url;
  }
);

export const getRouterRoot = createSelector(
  getRouterState,
  (state: RouterState) => {
    return state && state.router.root;
  }
);

export const getRouterQueryParams = createSelector(
  getRouterState,
  (state: RouterState) => {
    return state && state.router.root.queryParams;
  }
);

export const getRouterLastRoute = createSelector(
  getRouterState,
  (state: RouterState) => {
    if (state) {
      let route = state.router.root;
      while (route.firstChild) {
        route = route.firstChild;
      }
      return route.url.length ? route.url[0].path : '';
    }
    return '';
  }
);

export const getRouterLastRouteParams = createSelector(
  getRouterState,
  (state: RouterState) => {
    if (state) {
      let route = state.router.root;
      while (route.firstChild) {
        route = route.firstChild;
      }
      return route.params;
    }
    return '';
  }
);
