import { Injectable } from '@angular/core';

import { Store } from '@ngrx/store';

import { Go } from '@shared/state/router/router.actions';
import { CoreState } from '../../../../../../core/src/lib/+state/core.reducer';

@Injectable()
export class RouterFacade {
  constructor(private store: Store<CoreState>) {
  }

  goTo(path, extras = null) {
    return this.store.dispatch(new Go({ path, extras }));
  }
}
