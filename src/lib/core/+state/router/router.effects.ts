import { Location } from '@angular/common';
import { Injectable } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { Actions, Effect } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { switchMap, tap } from 'rxjs/operators';

import { BaseEffects } from '@shared/state/base.effects';
import {
  Go,
  GoFail,
  GoPayload,
  RouterActionTypes,
  SetRouterQueryParams,
  SetRouterQueryParamsPayload
} from './router.actions';
import { getRouterUrl } from './router.selectors';
import { CoreState } from '../../../../../../core/src/lib/+state/core.reducer';

@Injectable()
export class RouterEffects extends BaseEffects {

  @Effect({ dispatch: false })
  go$ = this.effect(RouterActionTypes.Go, {
    operatorFunctions: (action: Go) => [
      tap(() => {
        const payload: GoPayload = action.payload;
        const path = this.preparePath(payload.path);
        let extras: NavigationExtras;
        if (payload.extras) {
          extras = payload.extras;
        }
        console.log('this.router.navigate', path, extras);
        this.router.navigate(path, extras)
          .then(data => {
            console.log('data', data);
          })
          .catch(error => {
            console.log('error', error);
          });
      })
    ],
    onError: (action: Go, error) => new GoFail({}, error, action.correlationParams)
  });

  @Effect({ dispatch: false })
  setRouterQueryParams$ = this.effect(RouterActionTypes.SetRouterQueryParams, {
    operatorFunctions: (action: SetRouterQueryParams) => [
      switchMap(() => this.applySelector(getRouterUrl)),
      tap((url: string) => {
        const payload: SetRouterQueryParamsPayload = action.payload;
        const extras: NavigationExtras = payload.extras;
        const path = [this.location.path().split('?')[0]];
        this.router.navigate(path, extras);
        // console.log(this.router.url, url, this.location.path());
      })
    ],
    onError: (action: Go, error) => new GoFail({}, error, action.correlationParams)
  });

  constructor(
    protected actions$: Actions,
    protected store: Store<CoreState>,
    private router: Router,
    private location: Location
  ) {
    super(actions$, store);
  }

  private preparePath(params: any[]): any[] {
    const path = [];
    let i = 0;
    while (params.length > i) {
      const p = params[i];
      if (typeof p === 'object') {
        for (const key in p) {
          if (p.hasOwnProperty(key)) {
            path.push(p[key]);
          }
        }
      } else {
        path.push(p);
      }
      i++;
    }
    return path;
  }
}
