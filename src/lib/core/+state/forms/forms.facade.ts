import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { formsQuery } from './forms.selectors';
import { guid } from '@shared/core/utils';
import { InitForm, UpdateForm } from '@shared/state/forms/forms.actions';
import { FormsStateData } from '@shared/state/forms/forms.reducer';
import { Observable } from 'rxjs';
import { Form } from '@shared/core/models';

@Injectable()
export class FormsFacade {
  constructor(private store: Store<FormsStateData>) {
  }

  initForm(form: Form): void {
    const params = { correlationId: guid() };
    this.store.dispatch(new InitForm(form, params));
  }

  updateForm(form: Form): void {
    const params = { correlationId: guid() };
    this.store.dispatch(new UpdateForm(form, params));
  }

  getForm(formId: string): Observable<Form> {
    return this.store.pipe(select(formsQuery.getForm(formId)));
  }
}
