import { ActionReducer } from '@ngrx/store';
import {
  FormsAction,
  FormsActionTypes
} from './forms.actions';
import { ReducerService } from '@shared/core/services/reducer/reducer.service';
import { Form } from '@shared/core/models';

export const FORMS_KEY = 'forms';

export interface FormsState {
  list: Form[];
  ids: string[];
}

export interface FormsStateData {
  forms: FormsState
}

export interface FormsPartialState {
  readonly [FORMS_KEY]: FormsState;
}

export const initialFormsState = {
  list: [],
  ids: []
};

export function formsReducer(
  state: FormsState = initialFormsState,
  action: FormsAction
): FormsState {
  switch (action.type) {
    case FormsActionTypes.InitForm:
    case FormsActionTypes.UpdateForm: {
      const { formId, loading } = action.payload;
      const id = formId;
      state = {
        ...ReducerService.upsertOne(state, {
          id,
          formId,
          loading
        })
      };
      break;
    }
    case FormsActionTypes.DeleteForm: {
      const { formId } = action.payload;
      const id = formId;
      state = {
        ...ReducerService.deleteOne(state, id)
      };
      break;
    }
  }
  return state;
}

export function initFormsReducer(
  reducer: ActionReducer<any>
): ActionReducer<any> {
  return function (state, action) {
    return reducer(state, action);
  };
}
