import { createFeatureSelector, createSelector } from '@ngrx/store';
import { FORMS_KEY, FormsState } from './forms.reducer';
import { Form } from '@shared/core/models';

const getFormsState = createFeatureSelector<FormsState>(FORMS_KEY);

export const getForm = (formId: string) =>
  createSelector(
    getFormsState,
    (state: FormsState): Form => {
      return state.list.find(item => item.formId === formId);
    }
  );

export const formsQuery = {
  getForm
};
