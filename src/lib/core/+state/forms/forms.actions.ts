import { AggregatableAction, CorrelationParams } from '../aggregate';
import { Form } from '@shared/core/models';

export enum FormsActionTypes {
  InitForm = '[Forms] Init form',
  DeleteForm = '[Forms] Delete Form',
  UpdateForm = '[Forms] Update Form'
}

export class InitForm implements AggregatableAction {
  readonly type = FormsActionTypes.InitForm;

  constructor(public payload: Form, public correlationParams: CorrelationParams) {
  }
}

export interface DeleteFormPayload {
  formId: string;
}

export class DeleteForm implements AggregatableAction {
  readonly type = FormsActionTypes.DeleteForm;

  constructor(public payload: DeleteFormPayload, public correlationParams: CorrelationParams) {
  }
}

export class UpdateForm implements AggregatableAction {
  readonly type = FormsActionTypes.UpdateForm;

  constructor(public payload: Form, public correlationParams: CorrelationParams) {
  }
}

export type FormsAction =
  | InitForm
  | DeleteForm
  | UpdateForm;
