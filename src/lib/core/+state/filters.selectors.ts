import { Dictionary } from '@ngrx/entity';
import { EntitySelectors } from '@ngrx/entity/src/models';
import { createSelector, MemoizedSelector } from '@ngrx/store';

export interface FilterSelectors<T, V> {
  filterEntityIds: (types: string[] | number[]) => MemoizedSelector<V, string[] | number[]>;
  filterEntities: (types: string[] | number[]) => MemoizedSelector<V, Dictionary<T>>;
  filterAll: (types: string[] | number[]) => MemoizedSelector<V, T[]>;
  filterOnlyEntityIds: (ids: string[] | number[]) => MemoizedSelector<V, string[] | number[]>;
}

export function getFilterSelectors<T, V>(selectors: EntitySelectors<T, V>, prop: string): FilterSelectors<T, any> {
    const {
        selectAll,
        selectEntities,
        selectIds
    } = selectors;

    const filterOnlyEntityIds = (filterIds: string[]) => createSelector(
        selectIds,
        selectEntities,
        (ids: string[], entities: Dictionary<T>) => {
          return ids.filter(id => filterIds.includes(id));
        });

  const filterEntityIds = (types: (string | number)[]) => createSelector(
        selectIds,
        selectEntities,
        (ids: string[], entities: Dictionary<T>) => {
          return ids.filter(id => types.includes(entities[id][prop]));
        });

  const filterEntities = (types: (string | number)[]) => createSelector(
        filterEntityIds(types),
        selectEntities,
        (ids: string[], entities) => {
          const filteredEntities: Dictionary<T> = {};
            for (const key in entities) {
                if (ids.includes(key)) {
                    filteredEntities[key] = entities[key];
                }
            }
            return filteredEntities;
        });

  const filterAll = (types: (string | number)[]) => createSelector(
        filterEntityIds(types),
        filterEntities(types),
        (ids: string[], entities) => {
            return ids.map(id => entities[id]);
        });



    return {
        filterEntityIds,
        filterEntities,
        filterAll,
        filterOnlyEntityIds
    };
}
