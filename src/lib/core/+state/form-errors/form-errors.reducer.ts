import { ActionReducer } from '@ngrx/store';
import {
  FormErrorsAction,
  FormErrorsActionTypes
} from '@shared/state/form-errors/form-errors.actions';
import { FormError } from '@shared/core/models/form-error';
import { ReducerService } from '@shared/core/services/reducer/reducer.service';

export const FORM_ERRORS_KEY = 'formErrors';

export interface FormErrorsState {
  list: FormError[];
  ids: string[];
}

export interface FormErrorsStateData {
  formErrors: FormErrorsState
}

export interface FormErrorsPartialState {
  readonly [FORM_ERRORS_KEY]: FormErrorsState;
}

export const initialFormErrorsState = {
  list: [],
  ids: []
};

export function formErrorsReducer(
  state: FormErrorsState = initialFormErrorsState,
  action: FormErrorsAction
): FormErrorsState {
  switch (action.type) {
    case FormErrorsActionTypes.AddError: {
      const { code, field, formId, message, params } = action.payload;
      const id = `${formId}${field}`;
      state = {
        ...ReducerService.upsertOne(state, {
          id,
          code,
          field,
          formId,
          message,
          params
        })
      };
      break;
    }
    case FormErrorsActionTypes.DeleteError: {
      const { formId, field } = action.payload;
      const id = `${formId}${field}`;
      state = {
        ...ReducerService.deleteOne(state, id)
      };
      break;
    }
    case FormErrorsActionTypes.ClearForm: {
      const { formId } = action.payload;
      state = {
        ...ReducerService.deleteItemsByField(state, 'formId', formId)
      };
      break;
    }
  }
  return state;
}

export function initFormErrorsReducer(
  reducer: ActionReducer<any>
): ActionReducer<any> {
  return function (state, action) {
    return reducer(state, action);
  };
}
