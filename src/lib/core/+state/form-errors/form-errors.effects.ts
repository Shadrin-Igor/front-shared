import { Injectable } from '@angular/core';
import { Actions } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { BaseEffects } from '@shared/state/base.effects';
import { CoreState } from '../../../../../../core/src/lib/+state/core.reducer';

@Injectable()
export class FormErrorsEffects extends BaseEffects {
  /*  @Effect()
    checkValidation$ = this.effect(FormErrorsActionTypes.CheckValidation, {
      operatorFunctions: (action: CheckValidation) => [
        switchMap(() => {
          const { type, value, itemId } = action.payload;
          const headers = this.authenticationService.loadAuthDataFromLocalStore();
          return this.formErrorsService.formErrorsCheck({
            type,
            value,
            itemId
          });
        }),
        flatMap((r: HttpClientResult<BaseResponse>) => new FormErrorsCheckBehavior(action, r).resolve())
      ],
      onError: (action: CheckValidation, error) => {
        return new CheckValidationFail({
          type: action.payload.type,
          value: action.payload.value
        }, error, action.correlationParams);
      }
    });*/

  constructor(
    protected actions$: Actions,
    protected store: Store<CoreState>
  ) {
    super(actions$, store);
  }
}
