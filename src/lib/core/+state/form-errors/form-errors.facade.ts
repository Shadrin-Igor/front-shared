import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { formErrorsQuery } from './form-errors.selectors';
import { guid } from '@shared/core/utils';
import { AddError, ClearForm } from '@shared/state/form-errors/form-errors.actions';
import { FormErrorsStateData } from '@shared/state/form-errors/form-errors.reducer';
import { Observable } from 'rxjs';
import { FormError } from '@shared/core/models/form-error';

@Injectable()
export class FormErrorsFacade {
  constructor(private store: Store<FormErrorsStateData>) {
  }

  saveError(error: FormError): void {
    const params = { correlationId: guid() };
    this.store.dispatch(new AddError(error, params));
  }

  getFormErrors(formId: string): Observable<FormError[]> {
    return this.store.pipe(select(formErrorsQuery.getFormErrors(formId)));
  }

  clearFormErrors(formId: string) {
    const params = { correlationId: guid() };
    this.store.dispatch(new ClearForm({ formId }, params));
  }
}
