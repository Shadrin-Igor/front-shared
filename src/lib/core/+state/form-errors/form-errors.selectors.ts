import { createFeatureSelector, createSelector } from '@ngrx/store';
import { FormError } from '@shared/core/models/form-error';
import {
  FORM_ERRORS_KEY,
  FormErrorsState
} from '@shared/state/form-errors/form-errors.reducer';

const getFormErrorsState = createFeatureSelector<FormErrorsState>(FORM_ERRORS_KEY);

export const getFormErrors = (formId: string) =>
  createSelector(
    getFormErrorsState,
    (state: FormErrorsState): FormError[] => {
      return state.list.filter(item => item.formId === formId);
    }
  );

export const getFormError = (formId: string, field: string) =>
  createSelector(
    getFormErrorsState,
    (state: FormErrorsState): FormError => {
      return state.list.find(item => item.formId === formId && field === field);
    }
  );

export const formErrorsQuery = {
  getFormErrors,
  getFormError
};
