import { AggregatableAction, CorrelationParams, FailActionForAggregation } from '../aggregate';
import { FormError } from '@shared/core/models/form-error';

export enum FormErrorsActionTypes {
  AddError = '[FormErrors] Add Error',
  DeleteError = '[FormErrors] Delete Error',
  ClearForm = '[FormErrors] Clear Form',
}

export class AddError implements AggregatableAction {
  readonly type = FormErrorsActionTypes.AddError;

  constructor(public payload: FormError, public correlationParams: CorrelationParams) {
  }
}

export interface DeleteErrorPayload {
  formId: string;
  field: string;
}

export class DeleteError implements AggregatableAction {
  readonly type = FormErrorsActionTypes.DeleteError;

  constructor(public payload: DeleteErrorPayload, public correlationParams: CorrelationParams) {
  }
}

export interface ClearFormPayload {
  formId: string;
}

export class ClearForm implements AggregatableAction {
  readonly type = FormErrorsActionTypes.ClearForm;

  constructor(public payload: ClearFormPayload, public correlationParams: CorrelationParams) {
  }
}

export type FormErrorsAction =
  | AddError
  | DeleteError
  | ClearForm;
