import { Injectable } from '@angular/core';
import {
  CreateForm,
  RemoveForm,
  SendAction,
  SendActionPayload,
  UpdateForm
} from '@shared/state/forms/forms.actions';
import { select, Store } from '@ngrx/store';
import { guid } from '@shared/core/utils';
import { getFormData } from '@shared/state/forms/forms.selectors';
import { CoreState } from '../../../../../../core/src/lib/+state/core.reducer';

@Injectable()
export class FormsFacade {
  constructor(private store: Store<CoreState>) {
  }

  createForm(formId: string, initialFormData: any) {
    this.store.dispatch(
      new CreateForm(
        {formId, initialFormData},
        {
          correlationId: guid()
        }
      )
    );
  }

  getForm(formId: string) {
    return this.store.pipe(select(getFormData(formId)));
  }

  updateForm(params) {
    this.store.dispatch(new UpdateForm(params));
  }

  sendAction(sendActionParams: SendActionPayload) {
    this.store.dispatch(new SendAction(sendActionParams));
  }

  removeForm(formId: string) {
    this.store.dispatch(
      new RemoveForm(
        {formId},
        {
          correlationId: guid()
        }
      )
    );
  }
}
