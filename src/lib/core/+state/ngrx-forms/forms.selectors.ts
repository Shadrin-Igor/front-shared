import { createSelector } from '@ngrx/store';
import { formsAdapter } from '@shared/state/forms/forms.reducer';
import { formsFeatureState } from '@shared/state/feature.selectors';

const entitySelectors = formsAdapter.getSelectors(formsFeatureState);

export const {
  selectEntities: getFormsEntities,
  selectIds: getFormIds,
  selectAll: getAllForms
} = entitySelectors;

export const getFormData = (formId: string) =>
  createSelector(
    getFormsEntities,
    (formsState: any) => {
      return formsState[formId];
    }
  );

/*export const getFormIds = createSelector(
  filterUiUnitIdsByType(formType),
    (ids: string[]) => ids
);

export const getFormAll = createSelector(
    getFormIds,
    getUiUnitEntitiesByType(formType),
    (ids: string[], entities) => ids.map(id => entities[id])
);*/

/*export const filterForm = (id: string) => createSelector(
    getFormEntities,
    entities => {
        const f = entities[id];
        return f;
    }
);*/


