import { Injectable } from '@angular/core';
import { Actions } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { BaseEffects } from '@shared/state/base.effects';
import { CoreState } from '../../../../../../core/src/lib/+state/core.reducer';

@Injectable()
export class FormsEffects extends BaseEffects {

  /*  @Effect({dispatch: false})
    CreateForm$ = this.effect(FormsActionTypes.CreateForm, {
      operatorFunctions: (action: CreateForm) => [
        tap(() => {
          const {id, initialFormData} = action.payload;
          this.uiUnitsFacade.setUIUnits(id, initialFormData);
        })
      ],
      onError: (action: CreateForm, error) => {
        return new CreateFormFail({}, error, action.correlationParams);
      }
    });*/

  constructor(
    protected actions$: Actions,
    protected store: Store<CoreState>
  ) {
    super(actions$, store);
  }
}
