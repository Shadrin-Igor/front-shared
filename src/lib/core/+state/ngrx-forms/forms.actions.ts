import {AggregatableAction, CorrelationParams, FailActionForAggregation,} from '../aggregate';

export enum FormsActionTypes {
  CreateForm = '[Forms] Create Form Unit',
  CreateFormSuccess = '[Forms] Create Form Success',
  CreateFormFail = '[Forms] Create Form Fail',

  UpdateForm = '[Forms] Update Form Unit',
  UpdateFormSuccess = '[Forms] Update Form Success',
  UpdateFormFail = '[Forms] Update Form Fail',

  RemoveForm = '[Forms] Remove Form Unit',
  RemoveFormSuccess = '[Forms] Remove Form Success',
  RemoveFormFail = '[Forms] Remove Form Fail',

  SendAction = '[Forms] Send Action Form Unit',
  SendActionSuccess = '[Forms] Send Action Form Success',
  SendActionFail = '[Forms] Send Action Form Fail'
}

export class CreateFormPayload {
  formId: string;
  loading?: boolean;
  initialFormData?: any;
}

export class CreateForm implements AggregatableAction {
  readonly type = FormsActionTypes.CreateForm;

  constructor(public payload: CreateFormPayload, public correlationParams?: CorrelationParams) {
  }
}

export class CreateFormSuccessPayload {
}

export class CreateFormSuccess implements AggregatableAction {
  readonly type = FormsActionTypes.CreateFormSuccess;

  constructor(public payload: CreateFormSuccessPayload, public correlationParams?: CorrelationParams) {
  }
}

export class CreateFormFailPayload {
}

export class CreateFormFail implements FailActionForAggregation {
  readonly type = FormsActionTypes.CreateFormFail;

  constructor(public payload: CreateFormFailPayload, public error?: any, public correlationParams?: CorrelationParams) {
  }
}

export class RemoveFormPayload {
  name?: string;
  formId?: string;
}

export class RemoveForm implements AggregatableAction {
  readonly type = FormsActionTypes.RemoveForm;

  constructor(public payload: RemoveFormPayload, public correlationParams?: CorrelationParams) {
  }
}

export class RemoveFormSuccessPayload {
}

export class RemoveFormSuccess implements AggregatableAction {
  readonly type = FormsActionTypes.RemoveFormSuccess;

  constructor(public payload: RemoveFormSuccessPayload, public correlationParams?: CorrelationParams) {
  }
}

export class RemoveFormFailPayload {
}

export class RemoveFormFail implements FailActionForAggregation {
  readonly type = FormsActionTypes.RemoveFormFail;

  constructor(public payload: RemoveFormFailPayload, public error?: any, public correlationParams?: CorrelationParams) {
  }
}

export class UpdateFormPayload {
  formId: string;
  loading?: boolean;
  field?: string;
  value?: string;
  values?: { field: string, value: string }[];
}

export class UpdateForm implements AggregatableAction {
  readonly type = FormsActionTypes.UpdateForm;

  constructor(public payload: UpdateFormPayload, public correlationParams?: CorrelationParams) {
  }
}

export class UpdateFormSuccessPayload {
}

export class UpdateFormSuccess implements AggregatableAction {
  readonly type = FormsActionTypes.UpdateFormSuccess;

  constructor(public payload: UpdateFormSuccessPayload, public correlationParams?: CorrelationParams) {
  }
}

export class UpdateFormFailPayload {
}

export class UpdateFormFail implements FailActionForAggregation {
  readonly type = FormsActionTypes.UpdateFormFail;

  constructor(public payload: UpdateFormFailPayload, public error?: any, public correlationParams?: CorrelationParams) {
  }
}

export class SendActionPayload {
  formId: string;
  field: string;
  action: 'MARK_AS_TOUCHED';
}

export class SendAction implements AggregatableAction {
  readonly type = FormsActionTypes.SendAction;

  constructor(public payload: SendActionPayload, public correlationParams?: CorrelationParams) {
  }
}

export class SendActionSuccessPayload {
}

export class SendActionSuccess implements AggregatableAction {
  readonly type = FormsActionTypes.SendActionSuccess;

  constructor(public payload: SendActionSuccessPayload, public correlationParams?: CorrelationParams) {
  }
}

export class SendActionFailPayload {
}

export class SendActionFail implements FailActionForAggregation {
  readonly type = FormsActionTypes.SendActionFail;

  constructor(public payload: SendActionFailPayload, public error?: any, public correlationParams?: CorrelationParams) {
  }
}

export type FormsActions =
  | CreateForm
  | CreateFormSuccess
  | CreateFormFail
  | RemoveForm
  | RemoveFormSuccess
  | RemoveFormFail
  | UpdateForm
  | SendAction
  | SendActionSuccess
  | SendActionFail;
