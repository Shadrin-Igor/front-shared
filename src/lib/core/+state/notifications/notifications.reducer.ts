import { NotificationsAction } from './notifications.actions';

export const NOTIFICATIONS_FEATURE_KEY = 'notifications';

/**
 * Interface for the 'Notifications' data used in
 *  - NotificationsState, and
 *  - usersReducer
 *
 *  Note: replace if already defined in another module
 */

export interface NotificationsState {
  list: any[];
  ids: string[];
}

export interface NotificationsStateData {
  notifications: NotificationsState
}

export interface NotificationsPartialState {
  readonly [NOTIFICATIONS_FEATURE_KEY]: NotificationsStateData;
}

export const initialNotificationsState = {
  list: [],
  ids: []
};

export function notificationsReducer(
  state: NotificationsState = initialNotificationsState,
  action: NotificationsAction
): NotificationsState {
  /*  switch (action.type) {
      case NotificationsActionTypes.NotificationAdd: {
        const {text, title, type, id} = action.payload;
        const data ={
          text,
          id,
          title,
          type
        };
        console.log('NotificationAdd', data);
        state = {
          ...ReducerService.upsertOne(state, data)
        };
        break;
      }
    }*/
  return state;
}
