import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { BaseEffects } from '@shared/state/base.effects';
import { map, tap } from 'rxjs/operators';
import {
  NotificationAdd,
  NotificationAddFail,
  NotificationsActionTypes
} from './notifications.actions';
import { Toast, ToasterService } from 'angular2-toaster';
import { NotificationsFacade } from '@shared/state/notifications/notifications.facade';
import { NotificationModel } from '@shared/models/models/notification';
import {
  FileDelete,
  FileDeleteFail,
  FileDeleteSuccess,
  FilesActionTypes,
  FilesSortFail,
  FilesSortSuccess,
  FileUpdateFail,
  FileUpdateSuccess
} from '@shared/state/files';
import { CoreState } from '../../../../../../core/src/lib/+state/core.reducer';

@Injectable()
export class NotificationsEffects extends BaseEffects {
  @Effect({ dispatch: false })
  notificationAdd$ = this.effect(NotificationsActionTypes.NotificationAdd, {
    operatorFunctions: (action: NotificationAdd) => [
      tap(() => {
        const { text, title, type, timeout } = action.payload;
        const toast: Toast = {
          body: text,
          title,
          type,
          timeout
        };
        this.toasterService.pop(toast);
      })
    ],
    onError: (action: NotificationAdd, error) => {
      return new NotificationAddFail({}, error, action.correlationParams);
    }
  });

  @Effect({ dispatch: false })
  fileDeleteSuccess$ = this.effect(FilesActionTypes.FileDeleteSuccess, {
    operatorFunctions: (action: FileDeleteSuccess) => [
      map(() => {
        const notification: NotificationModel = {
          id: 5,
          type: 'success',
          text: 'File delete successfully',
          timeout: 2000
        };
        this.notificationsFacade.addNotifications(notification);
      })
    ],
    onError: (action: FileDelete, error) => {
      return new FileDeleteFail({}, error, action.correlationParams);
    }
  });

  @Effect({ dispatch: false })
  fileUpdateSuccess$ = this.effect(FilesActionTypes.FileUpdateSuccess, {
    operatorFunctions: (action: FileUpdateSuccess) => [
      tap(() => {
        console.log('FileUpdateSuccess', action);
        // this.uiUnitsFacade.setUIUnits(UiUnitIds.file, { updating: false });
        const notification: NotificationModel = {
          id: 5,
          type: 'success',
          text: 'File updated successfully',
          timeout: 2000
        };
        this.notificationsFacade.addNotifications(notification);
      })
    ],
    onError: (action: FileUpdateSuccess, error) => {
      return new FileUpdateFail({}, error, action.correlationParams);
    }
  });

  @Effect({ dispatch: false })
  filesSortSuccess$ = this.effect(FilesActionTypes.FilesSortSuccess, {
    operatorFunctions: (action: FilesSortSuccess) => [
      tap(() => {
        // this.uiUnitsFacade.setUIUnits(UiUnitIds.file, { updating: false });
        const notification: NotificationModel = {
          id: 5,
          type: 'success',
          text: 'Image sort save successfully',
          timeout: 2000
        };
        this.notificationsFacade.addNotifications(notification);
      })
    ],
    onError: (action: FilesSortSuccess, error) => {
      return new FilesSortFail({}, error, action.correlationParams);
    }
  });

  constructor(
    protected actions$: Actions,
    protected store: Store<CoreState>,
    protected toasterService: ToasterService,
    protected notificationsFacade: NotificationsFacade
  ) {
    super(actions$, store);
  }
}
