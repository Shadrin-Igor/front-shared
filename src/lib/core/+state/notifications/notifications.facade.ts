import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';

import { guid } from '@shared/core/utils';
import { NotificationAdd, NotificationAddPayload } from './notifications.actions';

@Injectable()
export class NotificationsFacade {

  constructor(private store: Store<any>) {
  }

  addNotifications(params: NotificationAddPayload) {
    this.store.dispatch(new NotificationAdd({ ...params }, { correlationId: guid() }));
  }
}
