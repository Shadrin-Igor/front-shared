import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { mainQuery } from './main.selectors';
import { MainPartialState } from '@shared/state/main/main.reducer';

@Injectable()
export class MainFacade {
  constructor(private store: Store<MainPartialState>) {
  }

  getMetadata() {
    return this.store.pipe(select(mainQuery.getMetaData));
  }
}
