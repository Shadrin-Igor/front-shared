import { Injectable } from '@angular/core';
import { Actions } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { BaseEffects } from '@shared/state/base.effects';
import { CoreState } from '../../../../../../core/src/lib/+state/core.reducer';

@Injectable()
export class MainEffects extends BaseEffects {

  constructor(
    protected actions$: Actions,
    protected store: Store<CoreState>
  ) {
    super(actions$, store);
  }
}
