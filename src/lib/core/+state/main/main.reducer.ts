import { MainAction, MainActionTypes } from './main.actions';
import { MAIN_FEATURE_KEY } from '../feature.selectors';
import { MetaDataModel } from '@shared/core/index';

export interface MainState {
  metaData: MetaDataModel
}

export interface MainStateData {
  main: MainState;
}

export interface MainPartialState {
  readonly [MAIN_FEATURE_KEY]: MainStateData;
}

export const initialMainState = {
  metaData: {}
};

export function mainReducer(
  state: MainState = initialMainState,
  action: MainAction
): MainState {
  switch (action.type) {
    case MainActionTypes.UpdateMetaData: {
      const metaData: MetaDataModel = action.payload
      state = {
        ...state,
        metaData
      };
      break;
    }
  }
  return state;
}
