import { createFeatureSelector, createSelector } from '@ngrx/store';
import { MainState } from './main.reducer';
import { MAIN_FEATURE_KEY } from '@shared/state/feature.selectors';

const getState = createFeatureSelector<MainState>(MAIN_FEATURE_KEY);

const getMetaData = createSelector(
  getState,
  (state: MainState) => {
    return state.metaData;
  }
);

export const mainQuery = {
  getMetaData
};
