import {
  AggregatableAction,
  CorrelationParams,
  FailActionForAggregation
} from '@shared/state/aggregate';

import { MetaDataModel } from '@shared/core/index';

export enum MainActionTypes {
  UpdateMetaData = '[MetaData] Update MetaData',
  UpdateMetaDataSuccess = '[MetaData] Update MetaData Success',
  UpdateMetaDataFail = '[MetaData] Update MetaData Error'
}

export interface MetaDataPayment extends MetaDataModel {
}

export class UpdateMetaData implements AggregatableAction {
  readonly type = MainActionTypes.UpdateMetaData;

  constructor(public payload: MetaDataPayment, public correlationParams: CorrelationParams) {
  }
}

export interface EmptyPayload {
}

export class UpdateMetaDataSuccess implements AggregatableAction {
  readonly type = MainActionTypes.UpdateMetaDataSuccess;

  constructor(public payload: EmptyPayload, public correlationParams: CorrelationParams) {
  }
}

export class UpdateMetaFail implements FailActionForAggregation {
  readonly type = MainActionTypes.UpdateMetaDataFail;

  constructor(public payload: EmptyPayload, public error: any, public correlationParams: CorrelationParams) {
  }
}

export type MainAction =
  | UpdateMetaData
  | UpdateMetaDataSuccess
  | UpdateMetaFail;
