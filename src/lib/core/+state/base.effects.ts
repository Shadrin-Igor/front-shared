import { Actions, ofType } from '@ngrx/effects';
import { ROUTER_NAVIGATION } from '@ngrx/router-store';
import { MemoizedSelector, select, Store } from '@ngrx/store';
import { Observable, of, OperatorFunction, throwError } from 'rxjs';
import { pipeFromArray } from 'rxjs/internal/util/pipe';
import {
  catchError,
  concatMap,
  exhaustMap,
  filter,
  first,
  map,
  mergeMap,
  switchMap,
} from 'rxjs/operators';

import {
  AggregatableAction,
  aggregate,
  CorrelationParams,
  FailActionForAggregation,
} from './aggregate';
import { CoreState } from '../../../../../core/src/lib/+state/core.reducer';

export interface EffectHandlers {
  operatorFunctions: (
    ...action: AggregatableAction[]
  ) => OperatorFunction<any, any>[];
  onError?: (
    action: AggregatableAction,
    error: any
  ) => FailActionForAggregation;
}

export class UnhandledError implements FailActionForAggregation {
  readonly type = 'Unhandled Error';

  constructor(
    public error?: Error,
    public correlationParams?: CorrelationParams
  ) {
  }
}

export class BaseEffects {
  constructor(protected actions$: Actions, protected store: Store<CoreState>) {
  }

  effect(actionType: string | string[], effectHandlers: EffectHandlers) {
    const _ofType = Array.isArray(actionType) ? ofType(...actionType) : ofType(actionType)
    return this.actions$.pipe(
      _ofType,
      switchMap((action: AggregatableAction) => {
        return this._getEffectHandlers(action, effectHandlers);
      })
    );
  }

  effectMerge(actionType: string, effectHandlers: EffectHandlers) {
    return this.actions$.pipe(
      ofType(actionType),
      mergeMap((action: AggregatableAction) => {
        return this._getEffectHandlers(action, effectHandlers);
      })
    );
  }

  effectConcat(actionType: string, effectHandlers: EffectHandlers) {
    return this.actions$.pipe(
      ofType(actionType),
      concatMap((action: AggregatableAction) => {
        return this._getEffectHandlers(action, effectHandlers);
      })
    );
  }

  effectExhaust(actionType: string, effectHandlers: EffectHandlers) {
    return this.actions$.pipe(
      ofType(actionType),
      exhaustMap((action: AggregatableAction) => {
        return this._getEffectHandlers(action, effectHandlers);
      })
    );
  }

  private _getEffectHandlers(action: AggregatableAction, effectHandlers: EffectHandlers) {
    return of(null).pipe(
      pipeFromArray(effectHandlers.operatorFunctions(action)),
      catchError(
        error => {
          console.error('onError: ', action, error);
          return effectHandlers.onError
            ? of(effectHandlers.onError(action, error))
            : throwError(
              new UnhandledError(error, action.correlationParams)
            );
        }
      )
    );
  }

  aggregate(
    aggregatorActionType: string,
    aggregatableActionTypes: (string | string[])[],
    errorActionType: string,
    effectHandlers: EffectHandlers
  ): Observable<any> {
    const arr = aggregatableActionTypes.map(aggregatableActionType => {
      if (Array.isArray(aggregatableActionType)) {
        return this.actions$.pipe(ofType(...aggregatableActionType));
      } else {
        return this.actions$.pipe(ofType(aggregatableActionType));
      }
    });

    return this.actions$.pipe(
      ofType(aggregatorActionType),
      aggregate(arr, this.actions$.pipe(ofType(errorActionType))),
      switchMap(([action, actionArray]: [AggregatableAction, AggregatableAction[]]) => {
        const errorAction = actionArray.find((a: AggregatableAction) => a.type === errorActionType);
        const successActions = errorAction ? [null] : actionArray;
        return of(null)
          .pipe(
            pipeFromArray(effectHandlers.operatorFunctions(action, ...successActions, errorAction)),
            catchError(
              error => {
                console.error('onError aggregate: ', error);
                return effectHandlers.onError
                  ? of(effectHandlers.onError(action, error))
                  : throwError(
                    new UnhandledError(error, action.correlationParams)
                  );
              }
            )
          );
      })
      // catchError(error => {
      //   console.log('aggregate error: ', error)
      //   return throwError(
      //     new UnhandledError(error)
      //   )

      // })
    );
  }

  applySelector(selector: any): Observable<any> {
    return this.store.pipe(select(selector), first());
  }

  applySelectorWithPrevious(
    prevResult: any,
    selector: MemoizedSelector<object, any>
  ): Observable<any> {
    return this.store.pipe(
      select(selector),
      first(),
      map(selectorResult => {
        if (Array.isArray(prevResult)) {
          return [...prevResult, selectorResult];
        } else {
          return [prevResult, selectorResult];
        }
      })
    );
  }

  waitFor(selector: MemoizedSelector<object, any>): Observable<boolean> {
    return this.store.pipe(
      select(selector),
      filter((loaded: boolean) => loaded),
      first()
    );
  }

  handleNavigation(segment, effectHandlers: EffectHandlers) {
    return this.actions$.pipe(
      ofType(ROUTER_NAVIGATION),
      filter((action: any) => {
        const s = action.payload.routerState;
        return s.path === segment;
      }),
      switchMap((action: any) => {
        return of(null).pipe(
          pipeFromArray(effectHandlers.operatorFunctions(action)),
          catchError(
            error =>
              effectHandlers.onError
                ? of(effectHandlers.onError(action, error))
                : throwError(
                  new UnhandledError(error, action.correlationParams)
                )
          )
        );
      })
    );
  }
}
