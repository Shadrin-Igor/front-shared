import { animate, keyframes, style, transition, trigger } from '@angular/animations';

export const appearFromLeftWithDelay = trigger('appearFromLeftWithDelay', [
  transition(':enter', [
    animate('500ms 3ms', keyframes([
        style({ opacity: 0, transform: 'translateX(-50px)', offset: 0 }),
        style({ opacity: .7, transform: 'translateX(30px)', offset: 0.7 }),
        style({ opacity: 1,  transform: 'none', offset: 1 })
      ])
    )
  ]),
  transition(':leave', [
    animate('300ms', keyframes([
        style({ opacity: 1, transform: 'none', offset: 0 }),
        style({ opacity: .3, transform: 'translateX(20px)', offset: 0.3 }),
        style({ opacity: 0,  transform: 'translateX(-50px)', offset: 1 })
      ])
    )
  ])
])
