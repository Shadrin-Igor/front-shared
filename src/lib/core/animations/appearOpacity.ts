import { animate, keyframes, style, transition, trigger } from '@angular/animations';

export const appearOpacity = trigger('appearOpacity', [
  transition(':enter', [
    animate('500ms 3ms', keyframes([
        style({ opacity: 0, offset: 0 }),
        style({ opacity: 1,  offset: 0.5 }),
        style({ opacity: .7,  offset: 0.7 }),
        style({ opacity: 1,  transform: 'none', offset: 1 })
      ])
    )
  ]),
  transition(':leave', [
    animate('300ms', keyframes([
        style({ opacity: 1, offset: 0 }),
        style({ opacity: 0, offset: 1 })
      ])
    )
  ])
])
