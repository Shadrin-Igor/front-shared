import { Inject, Injectable, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser, isPlatformServer } from '@angular/common';
import { BehaviorSubject, Observable } from 'rxjs';
import { UserModel } from '../../../models/models/user/user-model';
import { ActivatedRoute } from '@angular/router';

const STORAGE_KEY = {
  currentUser: 'WT_CURRENT_USER',
  token: 'WT_TOKEN',
  tokenExp: 'WT_TOKEN_EXP',
  refreshToken: 'WT_REFRESH_TOKEN'
};

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
  haveLocalStorage = false;
  public currentUser: Observable<UserModel>;
  private currentUserSubject: BehaviorSubject<UserModel>;

  constructor(
    @Inject(PLATFORM_ID) private platformId: any,
    protected route: ActivatedRoute
    /*@Inject('LOCALSTORAGE') private localStorage: any*/
  ) {
    if (isPlatformBrowser(this.platformId)) {
      this.haveLocalStorage = true;
      this.currentUserSubject = new BehaviorSubject<UserModel>(JSON.parse(localStorage.getItem(STORAGE_KEY.currentUser)));
    }

    if (isPlatformServer(this.platformId)) {
      this.haveLocalStorage = false;
      this.currentUserSubject = new BehaviorSubject<UserModel>({ id: '' });
    }

    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get isGuest(): boolean {
    const user = this.currentUserValue;
    console.log('isGuest', user);
    return !!(!user || !user.id);
  }

  public get currentUserValue(): UserModel {
    return this.currentUserSubject.value;
  }

  loadAuthDataFromLocalStore() {
    if (isPlatformBrowser(this.platformId)) {
      const user: UserModel = JSON.parse(localStorage.getItem(STORAGE_KEY.currentUser));
      const token: string = localStorage.getItem(STORAGE_KEY.token);
      const tokenExp: number = +localStorage.getItem(STORAGE_KEY.tokenExp);
      const refreshToken: string = localStorage.getItem(STORAGE_KEY.refreshToken);
      return { ...user, token, tokenExp, refreshToken };
    }
    return { id: '', user: {}, token: '', tokenExp: null, refreshToken: '' };
  }

  saveAuthInfo(user: UserModel, remember = false) {
    if (this.haveLocalStorage) {
      // store user details and jwt token in local storage to keep user logged in between page refreshes
      localStorage.setItem(STORAGE_KEY.currentUser, JSON.stringify({
        id: user.id,
        name: user.name,
        email: user.email,
        lastName: user.lastName,
        role: user.role,
        avatar: user.avatar,
        sex: user.sex,
        right: user.rights
      }));
      localStorage.setItem(STORAGE_KEY.token, user.token);
      localStorage.setItem(STORAGE_KEY.tokenExp, user.tokenExp.toString());
      localStorage.setItem(STORAGE_KEY.refreshToken, user.refreshToken);
      this.currentUserSubject.next(user);
    }
  }

  saveRefreshToken(token: string, tokenExp: number, refreshToken: string) {
    if (this.haveLocalStorage) {
      localStorage.setItem(STORAGE_KEY.token, token);
      localStorage.setItem(STORAGE_KEY.tokenExp, tokenExp.toString());
      localStorage.setItem(STORAGE_KEY.refreshToken, refreshToken);
    }
  }

  clearData() {
    if (this.haveLocalStorage) {
      localStorage.removeItem(STORAGE_KEY.currentUser);
      localStorage.removeItem(STORAGE_KEY.token);
      localStorage.removeItem(STORAGE_KEY.tokenExp);
      localStorage.removeItem(STORAGE_KEY.refreshToken);
    }
  }

  getRedirectToUrl() {
    if (this.route.snapshot.queryParams && this.route.snapshot.queryParams.redirectTo) {
      return this.route.snapshot.queryParams.redirectTo;
    }
    return undefined;
  }

}
