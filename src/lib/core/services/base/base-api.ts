import { RequestConfig, Request, Response } from '../../../models/endpoints/http';

export abstract class BaseAPI {
  constructor(protected domain: string) {
  }

  abstract createRequest(paramsConfig: RequestConfig, endPoint: string): Request;

  abstract handleResponse(response: any, endPoint: string): Response
}
