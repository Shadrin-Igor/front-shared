import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { HttpClientResult } from '../../models';
import { CORE_CONFIG } from '@shared/core/tokens';
import { BaseAPIService } from '../base/base-api-service';
import { FormValidationApi } from './form-validation-api';
import { AppConfig } from '../../../models/config';
import { BaseResponse } from '@shared/models/endpoints';
import { FormValidationCheckRequestConfig } from '@shared/core/endpoints/form-validation/form-validation-check';
import { FormValidationEndpointTypes } from '@shared/core/endpoints/form-validation/form-validation-endpoint-types';

@Injectable({
  providedIn: 'root'
})
export class FormValidationService extends BaseAPIService {
  constructor(protected httpClient: HttpClient, @Inject(CORE_CONFIG) private config: AppConfig) {
    super(httpClient);
    this.api = new FormValidationApi(this.config.apiUrl);
  }

  formValidationCheck(config: FormValidationCheckRequestConfig): Observable<HttpClientResult<BaseResponse>> {
    return this.doRequest(config, FormValidationEndpointTypes.CHECK);
  }
}

