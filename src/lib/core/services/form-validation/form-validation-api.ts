import { BaseAPI } from '@shared/core/services/base/base-api';

import { Request } from '@shared/models/endpoints/http';
import { FormValidationRequestConfig } from '@shared/core/endpoints/form-validation/form-validation-request-config';
import { FormValidationEndpointTypes } from '@shared/core/endpoints/form-validation/form-validation-endpoint-types';
import { BaseResponse, BaseResponseFactory } from '@shared/models/endpoints';
import {
  FormValidationCheckRequest,
  FormValidationCheckRequestConfig
} from '@shared/core/endpoints/form-validation/form-validation-check';

export class FormValidationApi extends BaseAPI {
  constructor(protected domain: string) {
    super(domain);
  }

  createRequest(paramsConfig: FormValidationRequestConfig, endPointType: string) {
    let request: Request;
    switch (endPointType) {
      case FormValidationEndpointTypes.CHECK:
        request = new FormValidationCheckRequest(this.domain, <FormValidationCheckRequestConfig> paramsConfig);
        break;
      default:
        throw new Error(`Request for ${endPointType} is undefined `);
    }
    return request;
  }

  handleResponse(response: any, endPointType: string) {
    let resResponse: BaseResponse;
    switch (endPointType) {
      case FormValidationEndpointTypes.CHECK:
        resResponse = BaseResponseFactory.createResponse(response);
        break;
      default:
        throw new Error(`Response for ${endPointType} is undefined `);
    }
    return resResponse;
  }
}
