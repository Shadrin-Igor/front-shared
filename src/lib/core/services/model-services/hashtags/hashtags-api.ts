import {
  HashtagsEndpointTypes,
  HashtagsRequestConfig,
  HashtagsResponse
} from '@shared/models/endpoints/hashtags';
import { BaseAPI } from '../../base/base-api';
import { HashtagsLoadRequest } from '@shared/models/endpoints/hashtags/hashtags-load/request/hashtags-load-request';
import { HashtagsLoadRequestConfig } from '@shared/models/endpoints/hashtags/hashtags-load/request/hashtags-load-request-config';
import { HashtagsLoadResponseFactory } from '@shared/models/endpoints/hashtags/hashtags-load/response/hashtags-load-response-factory';
import { Request } from '@shared/models/endpoints';

export class HashtagsApi extends BaseAPI {
  constructor(protected domain: string) {
    super(domain);
  }

  createRequest(paramsConfig: HashtagsRequestConfig, endPointType: string) {
    let request: Request;
    switch (endPointType) {
      case HashtagsEndpointTypes.LOAD:
        request = new HashtagsLoadRequest(this.domain, <HashtagsLoadRequestConfig> paramsConfig);
        break;
      default:
        throw new Error(`Request for ${endPointType} is undefined `);
    }
    return request;
  }

  handleResponse(response: any, endPointType: string) {
    let courseResponse: HashtagsResponse;
    switch (endPointType) {
      case HashtagsEndpointTypes.LOAD:
        courseResponse = HashtagsLoadResponseFactory.createResponse(response);
        break;
      default:
        throw new Error(`Response for ${endPointType} is undefined `);
    }
    return courseResponse;
  }
}
