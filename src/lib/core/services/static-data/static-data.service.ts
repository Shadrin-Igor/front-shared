import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { HttpClientResult } from '../../models';
import { CORE_CONFIG } from '@shared/core/tokens';
import { BaseAPIService } from '../base/base-api-service';
import { StaticDataApi } from './static-data-api';
import { AppConfig } from '../../../models/config';
import { StaticDataLoadRequestConfig } from '@shared/models/endpoints/static-data/static-data-load/request/static-data-load-request-config';
import { StaticDataEndpointTypes } from '@shared/models/endpoints/static-data';
import { BaseResponse } from '@shared/models/endpoints';


@Injectable({
  providedIn: 'root'
})
export class StaticDataService extends BaseAPIService {
  constructor(protected httpClient: HttpClient, @Inject(CORE_CONFIG) private config: AppConfig) {
    super(httpClient);
    this.api = new StaticDataApi(this.config.apiUrl);
  }

  staticDataLoad(config: StaticDataLoadRequestConfig): Observable<HttpClientResult<BaseResponse>> {
    return this.doRequest(config, StaticDataEndpointTypes.LOAD);
  }
}

