import { BaseAPI } from '@shared/core/services/base/base-api';

import { Request } from '@shared/models/endpoints/http';
import {
  StaticDataEndpointTypes,
  StaticDataRequestConfig
} from '@shared/models/endpoints/static-data';
import { StaticDataLoadRequest } from '@shared/models/endpoints/static-data/static-data-load/request/static-data-load-request';
import { BaseResponse, BaseResponseFactory } from '@shared/models/endpoints';
import { StaticDataLoadRequestConfig } from '@shared/models/endpoints/static-data/static-data-load/request/static-data-load-request-config';

export class StaticDataApi extends BaseAPI {
  constructor(protected domain: string) {
    super(domain);
  }

  createRequest(paramsConfig: StaticDataRequestConfig, endPointType: string) {
    let request: Request;
    switch (endPointType) {
      case StaticDataEndpointTypes.LOAD:
        request = new StaticDataLoadRequest(this.domain, <StaticDataLoadRequestConfig> paramsConfig);
        break;
      default:
        throw new Error(`Request for ${endPointType} is undefined `);
    }
    return request;
  }

  handleResponse(response: any, endPointType: string) {
    let courseResponse: BaseResponse;
    switch (endPointType) {
      case StaticDataEndpointTypes.LOAD:
        courseResponse = BaseResponseFactory.createResponse(response);
        break;
      default:
        throw new Error(`Response for ${endPointType} is undefined `);
    }
    return courseResponse;
  }
}
