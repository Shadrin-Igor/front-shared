import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { HttpClientResult } from '../../models';
import { CORE_CONFIG } from '../../tokens';
import { BaseAPIService } from '../base/base-api-service';
import { UsersApi } from './users-api';
import { LoginSendCreateRequestConfig } from '../../../models/endpoints/auth/login-send';
import { AppConfig } from '../../../models/config';
import { AuthEndpointTypes, AuthResponse } from '../../../models/endpoints/auth';
import { RefreshTokenRequestConfig } from '../../../models/endpoints/auth/refresh-token';

@Injectable({
  providedIn: 'root'
})
export class UsersService extends BaseAPIService {
  constructor(protected httpClient: HttpClient, @Inject(CORE_CONFIG) private config: AppConfig) {
    super(httpClient);
    this.api = new UsersApi(this.config.apiUrl);
  }

  loginSend(config: LoginSendCreateRequestConfig): Observable<HttpClientResult<AuthResponse>> {
    return this.doRequest(config, AuthEndpointTypes.LOGIN_SEND);
  }

  refreshToken(config: RefreshTokenRequestConfig): Observable<HttpClientResult<AuthResponse>> {
    return this.doRequest(config, AuthEndpointTypes.REFRESH_TOKEN);
  }
}
