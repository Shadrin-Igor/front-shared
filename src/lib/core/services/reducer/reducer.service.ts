export class ReducerService {

  static clearAll(initState) {

  }

  static upsertOne(state, item, id?: string | number) {
    const newState = { ...state };
    const itemId = id ? id : item.id;
    const foundIndex = this.findItemIndex(state, itemId);
    if (foundIndex || foundIndex === 0) {
      newState.list[foundIndex] = {...newState.list[foundIndex],...item};
    } else {
      if (Array.isArray(newState.list)) {
        newState.list.push(item);
      } else {
        newState.list[item.id] = item;
      }
      newState.ids.push(itemId);
    }
    return newState;
  }

  static upsertMeny(state, items) {
    let newState = { ...state };
    items.forEach((item) => {
      newState = this.upsertOne(state, item);
    });
    return newState;
  }

  static findItemIndex(state, itemId: number | string) {
    const itemIdChecked = itemId;
    let foundIndex = null;
    if (state.ids.indexOf(itemIdChecked) !== -1) {
      state.list.forEach((item: any, index: number) => {
        if (item.id === itemIdChecked) {
          foundIndex = index;
        }
      });
    }
    return foundIndex;
  }

  static deleteOne(state, itemId: number | string) {
    if (state.ids.indexOf(itemId) !== -1) {
      state = this.deleteItemsByField(state, 'id', itemId);
    }
    return state;
  }

  static deleteItemsByField(state, field: string, value: number | string) {
    state.list = state.list.filter((item) => item[field] !== value);
    state.ids = state.ids.filter((item) => item[field] !== value);

    return state;
  }
}
