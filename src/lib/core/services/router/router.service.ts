import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';

import { Go } from '@shared/state/router';
import { CoreState } from '../../../../../../core/src/lib/+state/core.reducer';

@Injectable({providedIn: 'root'})
export class RouterService {
  constructor(private store: Store<CoreState>,) {

  }

  goTo(src) {
    this.store.dispatch(new Go({path: [src]}));
  }
}
