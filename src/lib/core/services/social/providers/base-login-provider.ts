import { BehaviorSubject } from 'rxjs';

import { LoginProvider } from './login-provider';
import { SocialUser } from '@shared/models/social/social-user';

export abstract class BaseLoginProvider implements LoginProvider {

    protected _readyState: BehaviorSubject<boolean> = new BehaviorSubject(false);

    constructor() { }

    protected onReady(): Promise<void> {
        return new Promise((resolve, reject) => {
            this._readyState.subscribe((isReady) => {
                if (isReady) {
                    resolve();
                }
            })
        });
    }

    abstract initialize(): Promise<string>;
    abstract getLoginStatus(): Promise<SocialUser>;
    abstract signIn(): Promise<SocialUser>;
    abstract signOut(): Promise<any>;

    loadScript(id: string, src: string, onload: any, async = true, inner_text_content = ''): void {
        const fjs = document.getElementsByTagName('script')[0];

        if (document.getElementById(id)) { return; }

        const js = document.createElement('script');
        js.id = id;
        js.async = async;
        js.src = src;
        js.onload = onload;
        fjs.parentNode.insertBefore(js, fjs);
        //document.head.appendChild(js);
    }
}
