
import { BaseLoginProvider } from './base-login-provider';
import { FbLoginOptions, SocialUser } from '@shared/models/social';

declare let FB: any;


export class FbLoginProvider extends BaseLoginProvider {

    public static readonly PROVIDER_ID = 'facebook-jssdk';



    constructor(
        private clientId: string,
        private opt: FbLoginOptions = { scope: 'email,public_profile' },
        private locale: string = 'en_US',
        private fields: string = 'name,email,picture,first_name,last_name',
        private version: string = 'v3.1'
    ) { super(); }

    initialize(): Promise<string> {
        return new Promise((resolve, reject) => {
            this.loadScript(FbLoginProvider.PROVIDER_ID,
                `https://connect.facebook.net/${this.locale}/sdk.js`,
                () => {
                    FB.init({
                        appId: this.clientId,
                        autoLogAppEvents: true,
                        cookie: true,
                        xfbml: true,
                        version: this.version
                    });
                    // FB.AppEvents.logPageView();
                    this._readyState.next(true);
                    resolve();
                });
        });
    }

    getLoginStatus(): Promise<SocialUser> {
        return new Promise((resolve, reject) => {
            this.onReady().then(() => {
                FB.getLoginStatus(function (response: any) {
                    if (response.status === 'connected') {
                        const authResponse = response.authResponse;
                        FB.api('/me?fields=name,email,picture,first_name,last_name', (fbUser: any) => {
                            const user: SocialUser = {
                                id: fbUser.id,
                                name: fbUser.name,
                                email: fbUser.email,
                                photoUrl: 'https://graph.facebook.com/' + fbUser.id + '/picture?type=normal',
                                firstName: fbUser.first_name,
                                lastName: fbUser.last_name,
                                authToken: authResponse.accessToken,
                                provider: FbLoginProvider.PROVIDER_ID
                            }

                            resolve(user);
                        });
                    } else {
                        resolve(null);
                    }
                });
            });
        });

    }

    signIn(opt?: FbLoginOptions): Promise<SocialUser> {
        return new Promise((resolve, reject) => {
            this.onReady().then(() => {
                FB.login((response: any) => {
                    if (response.authResponse) {
                        const authResponse = response.authResponse;
                        // FB.api(`/me?fields=${this.fields}`, (fbUser: any) => {
                        //     const user: SocialUser = {
                        //         provider: FbLoginProvider.PROVIDER_ID,
                        //         id: fbUser.id,
                        //         name: fbUser.name,
                        //         email: fbUser.email,
                        //         photoUrl: 'https://graph.facebook.com/' + fbUser.id + '/picture?type=normal',
                        //         firstName: fbUser.first_name,
                        //         lastName: fbUser.last_name,
                        //         authToken: authResponse.accessToken,
                        //         facebook: fbUser
                        //     }

                        //     resolve(user);
                        // });
                        resolve({ provider: FbLoginProvider.PROVIDER_ID, authToken: authResponse.accessToken });
                    } else {
                        reject('User cancelled login or did not fully authorize.');
                    }
                }, this.opt);
            });
        });
    }

    signOut(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.onReady().then(() => {
                FB.logout((response: any) => {
                    resolve();
                });
            });
        });
    }

}
