import { LoginOptions, SocialUser } from '@shared/models/social';

export interface LoginProvider {
    initialize(): Promise<string>;
    getLoginStatus(): Promise<SocialUser>;
    signIn(opt?: LoginOptions): Promise<SocialUser>;
    signOut(): Promise<any>;
}
