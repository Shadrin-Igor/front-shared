import { isPlatformBrowser } from '@angular/common';
import { Inject, Injectable, Optional, PLATFORM_ID } from '@angular/core';
// import { LoginOptions, SocialUser } from '@wt/model';
import { BehaviorSubject, from, Observable, race, throwError } from 'rxjs';
import { catchError, switchMap } from 'rxjs/operators';

import { AUTH_SERVICE_CONFIG } from '../../tokens';
// import { LoginProvider } from './providers/login-provider';
import { SocialAuthServiceConfig } from './social-auth-service-config';

@Injectable()
export class SocialAuthService {

    private static readonly ERR_LOGIN_PROVIDER_NOT_FOUND = 'Login provider not found';
    private static readonly ERR_NOT_LOGGED_IN = 'Not logged in';

    // private providers: Map<string, LoginProvider>;

    private _readyState: BehaviorSubject<string[]> = new BehaviorSubject([]);

    constructor(
        @Inject(PLATFORM_ID) private platformId: object,
        @Optional() @Inject(AUTH_SERVICE_CONFIG) private config: SocialAuthServiceConfig
    ) {
        if (isPlatformBrowser(platformId) && config) {
/*            this.providers = config.providers;
            this.providers.forEach((provider: LoginProvider, key: string) => {
                provider.initialize().then(() => {
                    const readyProviders = this._readyState.getValue();
                    readyProviders.push(key);
                    this._readyState.next(readyProviders);
                }).catch((err) => {
                    console.log('initialize error: ', err)
                    // this._authState.next(null);
                });
            });*/
        }

    }

    /*private getLoginStatus(): Observable<SocialUser> {
        const arr = [];


        this.providers.forEach((provider: LoginProvider, key: string) => {

            arr.push(from(provider.initialize()).pipe(
                switchMap(() => {
                    const readyProviders = this._readyState.getValue();
                    readyProviders.push(key);
                    this._readyState.next(readyProviders);
                    return from(provider.getLoginStatus());
                }),
                catchError(error => {
                    return throwError('getLoginStatus error: ' + error);
                })
            ))
        });

        return race(arr);
    }

    public signIn(providerId: string, opt?: LoginOptions): Observable<SocialUser> {
        const providerObject = this.providers.get(providerId);
        if (providerObject) {
            return from(providerObject.signIn(opt))
        } else {
            return throwError(SocialAuthService.ERR_LOGIN_PROVIDER_NOT_FOUND)
        }
    }
*/
    public signOut(providerId: string): Observable<any> {
/*        const providerObject = this.providers.get(providerId);
        if (providerObject) {
            return from(providerObject.signOut())
        } else {
            return throwError(SocialAuthService.ERR_LOGIN_PROVIDER_NOT_FOUND)
        }*/
      return throwError(SocialAuthService.ERR_LOGIN_PROVIDER_NOT_FOUND)
    }
}
