import { APP_BASE_HREF } from '@angular/common';
import { Injectable, Injector } from '@angular/core';

import { StorageService } from '../storage/storage.service';
import { COOKIES_OPTIONS, CookiesOptions } from './cookies-options';
import { mergeOptions, safeJsonParse } from './utils';

@Injectable()
export class CookiesService implements StorageService {
    protected options: CookiesOptions;
    private defaultOptions: CookiesOptions;

    constructor(protected injector: Injector) {
        const cookiesOptions = this.injector.get(COOKIES_OPTIONS, {})
        this.defaultOptions = {
            path: this.injector.get(APP_BASE_HREF, '/'),
            domain: null,
            expires: null,
            secure: false
        };
        this.options = mergeOptions(this.defaultOptions, cookiesOptions);
    }

    setItem(key: string, value: string, options?: CookiesOptions): void {
        this.cookiesWriter()(key, value, options || this.options);
    }

    putObject(key: string, value: Object, options?: CookiesOptions): void {
        this.setItem(key, JSON.stringify(value), options || this.options);
    }

    getItem(key: string): string {
        return (<any>this.cookiesReader())[key];
    }

    getObject(key: string): { [key: string]: string } | string {
        const value = this.getItem(key);
        return value ? safeJsonParse(value) : value;
    }

    getAll(): { [key: string]: string } {
        return <any>this.cookiesReader();
    }

    removeItem(key: string, options?: CookiesOptions): void {
        this.cookiesWriter()(key, '', options);
    }

    clear(): void {
        const cookies = this.getAll();
        Object.keys(cookies).forEach(key => {
            this.removeItem(key);
        });
    }

    protected cookiesReader(): { [key: string]: any } {
        return {};
    }

    protected cookiesWriter(): (name: string, value: string | undefined, options?: CookiesOptions) => void {
        return () => { };
    }
}