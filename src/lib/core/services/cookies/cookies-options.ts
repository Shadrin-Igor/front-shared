import { InjectionToken } from '@angular/core';

export interface CookiesOptions {
    path?: string | null;
    domain?: string | null;
    expires?: string | Date | null;
    secure?: boolean | null;
    httpOnly?: boolean | null;
}

export const COOKIES_OPTIONS = new InjectionToken<CookiesOptions>('COOKIES_OPTIONS');
