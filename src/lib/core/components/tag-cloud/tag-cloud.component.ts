import {ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {trackById} from '@shared/core/utils';
// import {HashtagsFacade} from '@wt/core/lib/+state/hashtags';
import {Observable} from 'rxjs';
// import {HashtagModel} from '@shared/models/models/hashtag';
import {filter, tap} from 'rxjs/operators';

@Component({
  selector: 'shared-tag-cloud',
  templateUrl: './tag-cloud.component.html',
  styleUrls: ['./tag-cloud.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TagCloudComponent implements OnInit, OnDestroy {
  loaded = false;
  getHashtags$: Observable<any[]>;
  trackByFn = trackById;

  constructor() {
  }

  ngOnInit() {
    /*this.hashtagsFacade.loadHashtags();
    this.getHashtags$ = this.hashtagsFacade.getHashtags$.pipe(
      filter(items => !!items),
      tap(items => {
        if (items.length) {
          this.loaded = true;
        }
      })
    );*/
  }

  ngOnDestroy(): void {
  }

}
