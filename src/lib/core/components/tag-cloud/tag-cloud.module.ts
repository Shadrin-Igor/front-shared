import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TagCloudComponent } from './tag-cloud.component';
import { SharedSpinnerModule } from '@shared/components/spinner/spinner.module';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [TagCloudComponent],
  imports: [
    CommonModule,
    SharedSpinnerModule,
    RouterModule
  ],
  exports: [TagCloudComponent]
})
export class TagCloudModule {
}
