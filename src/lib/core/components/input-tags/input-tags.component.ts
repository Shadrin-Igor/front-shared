import { Component, OnInit } from '@angular/core';

interface Tag {
  id: number;
  title: string;
}

@Component({
  selector: 'shared-input-tags',
  templateUrl: './input-tags.component.html',
  styleUrls: ['./input-tags.component.scss']
})
export class InputTagsComponent implements OnInit {
  allTags: Tag[] = [
    {id: 1, title: 'Apple'},
    {id: 2, title: 'Lemon'},
    {id: 3, title: 'Lime'},
    {id: 4, title: 'Orange'},
    {id: 5, title: 'Strawberry'}
  ];
  tags: Tag[] = [{id: 2, title: 'Lemon'}];

  constructor() { }

  ngOnInit() {
  }

  changedText(tags) {
    console.log('changedText', tags);
  }

  changedList(tags) {
    console.log('changedList', tags);
  }
}
