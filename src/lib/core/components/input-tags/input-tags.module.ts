import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InputTagsComponent } from '@shared/components/input-tags/input-tags.component';
import { ChipsInputModule } from '@shared/components/chips-input/chips-input.module';

@NgModule({
  declarations: [InputTagsComponent],
  exports: [InputTagsComponent],
  imports: [
    CommonModule,
    ChipsInputModule
  ]
})
export class InputTagsModule {
}
