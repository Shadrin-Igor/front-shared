import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PanelHeaderComponent } from '@shared/components/panel-header/panel-header.component';
import { MatIconModule } from '@angular/material/icon';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { FlexLayoutModule } from '@angular/flex-layout';

@NgModule({
  declarations: [PanelHeaderComponent],
  exports: [PanelHeaderComponent],
  imports: [
    CommonModule,
    MatIconModule,
    MatFormFieldModule,
    MatButtonModule,
    MatInputModule,
    FlexLayoutModule
  ]
})
export class PanelHeaderModule {
}
