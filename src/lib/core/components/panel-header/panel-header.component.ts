import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'shared-panel-header',
  templateUrl: './panel-header.component.html',
  styleUrls: ['./panel-header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PanelHeaderComponent implements OnInit {

  @Input() title: string;
  constructor() { }

  ngOnInit() {
  }

}
