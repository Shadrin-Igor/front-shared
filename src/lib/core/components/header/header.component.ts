import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'shared-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  @Input() title = 'Travel BloGG';
  @Input() subTitle: string;
  @Input() description: string;
  @Input() image: string;
  @Input() small = false;
  constructor() { }

  ngOnInit() {
  }

}
