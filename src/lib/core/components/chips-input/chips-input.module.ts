import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ChipsInputComponent} from './chips-input.component';
import {
  MatAutocompleteModule,
  MatChipsModule,
  MatFormFieldModule,
  MatIconModule
} from '@angular/material';
import {ReactiveFormsModule} from '@angular/forms';

@NgModule({
  declarations: [ChipsInputComponent],
  exports: [ChipsInputComponent],
  imports: [
    CommonModule,
    MatFormFieldModule,
    MatChipsModule,
    MatAutocompleteModule,
    MatIconModule,
    ReactiveFormsModule
  ]
})
export class ChipsInputModule {
}
