import { ChangeDetectionStrategy, Component, Input, OnChanges, SimpleChanges } from '@angular/core';

export type IconSize = 'auto' | 'large' | 'normal' | 'small' | 'xs';
export type IconColor = 'inherit' | 'yellow' | 'orange_gray' | 'red' | 'white';

/**
 * Icon component.
 * Add svg icon to any place.
 * By default it uses size and color of host element or you can set predefined values by settings inputs size and color
 *
 * Usage:
 * ````
 * <shared-ui-icon icon="add"></app-ui-icon>
 * <shared-ui-icon icon="add" size="xs" color="primary"></app-ui-icon>
 * ````
 */
@Component({
  selector: 'shared-ui-icon',
  templateUrl: './icon.component.html',
  styleUrls: ['./icon.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class IconComponent implements OnChanges {
  @Input() icon: string;
  @Input() size: IconSize = 'normal';
  @Input() color: IconColor = 'inherit';

  iconSize: string = 's-22';

  ngOnChanges(changes: SimpleChanges) {
    if (this.size === 'normal') {
      this.iconSize = `s-22`;
    }
    if (this.size === 'small') {
      this.iconSize = `s-18`;
    }
  }
}
