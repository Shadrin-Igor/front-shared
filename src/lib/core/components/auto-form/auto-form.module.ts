import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  MatFormFieldModule,
  MatInputModule,
  MatSelectModule,
  MatSlideToggleModule
} from '@angular/material';
import { NgrxFormsModule } from 'ngrx-forms';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { FormFieldComponent } from '@shared/components/auto-form/containers/form-field/form-field.component';
import { FormFieldErrorModule } from '@shared/components/form-field-error/form-field-error.module';
import { SharedSpinnerModule } from '@shared/components/spinner/spinner.module';
import { FormFieldSpinnerComponent } from './components/form-field-spinner/form-field-spinner.component';
import { FormFieldSelectComponent } from './components/form-field-select/form-field-select.component';
import { FormFieldSlideToggleComponent } from './components/form-field-slide-toggle/form-field-slide-toggle.component';
import { FormFieldAutoFormComponent } from './containers/form-field-auto-form/form-field-auto-form.component';
import { FormFieldShowAutoFormComponent } from '@shared/components/auto-form/containers/form-field-show-auto-form/form-field-show-auto-form.component';
import { FormFieldShowComponent } from '@shared/components/auto-form/containers/form-field-show/form-field-show.component';
import { FormFieldSelectShowComponent } from '@shared/components/auto-form/components/form-field-select-show/form-field-select-show.component';
import { FlexModule } from '@angular/flex-layout';
import { FormFieldInputComponent } from './components/form-field-input/form-field-input.component';
import { NgxMaskModule } from 'ngx-mask';
import { FormMessageBoxComponent } from './components/form-message-box/form-message-box.component';
import { MatIconModule } from '@angular/material/icon';
import { MatTooltipModule } from '@angular/material/tooltip';
import { FormFieldFileComponent } from '@shared/components/auto-form/components/form-field-file/form-field-file.component';
import { FormFieldFilesComponent } from '@shared/components/auto-form/components/form-field-files/form-field-files.component';

@NgModule({
  declarations: [
    FormFieldComponent,
    FormFieldShowComponent,
    FormFieldSpinnerComponent,
    FormFieldSelectComponent,
    FormFieldSelectShowComponent,
    FormFieldSlideToggleComponent,
    FormFieldAutoFormComponent,
    FormFieldShowAutoFormComponent,
    FormFieldShowComponent,
    FormFieldInputComponent,
    FormMessageBoxComponent,
    FormFieldFileComponent,
    FormFieldFilesComponent
  ],
  imports: [
    CommonModule,
    FormFieldErrorModule,
    MatFormFieldModule,
    MatInputModule,
    MatSlideToggleModule,
    MatSelectModule,
    NgrxFormsModule,
    AngularEditorModule,
    FormsModule,
    SharedSpinnerModule,
    ReactiveFormsModule,
    FlexModule,
    NgxMaskModule,
    MatIconModule,
    MatTooltipModule
  ],
  exports: [
    FormFieldAutoFormComponent,
    FormFieldShowAutoFormComponent
  ]
})
export class AutoFormModule {
}
