import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output
} from '@angular/core';
import { FormGroup } from '@angular/forms';
import { IFormField } from '@shared/components/auto-form/interfaces/form-field';

@Component({
  selector: 'shared-form-field-show-auto-form',
  templateUrl: './form-field-show-auto-form.component.html',
  styleUrls: ['./form-field-show-auto-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FormFieldShowAutoFormComponent implements OnInit {
  @Input() fields: IFormField[];
  @Input() form: FormGroup;
  @Input() editorImageUploadUrl = '';
  @Output() changeForm = new EventEmitter<{ [field: string]: string }>();

  value: any;

  ngOnInit() {
  }

  truckByFieldName(item: IFormField) {
    return item.field;
  }
}
