import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnDestroy,
  OnInit
} from '@angular/core';
import { FormGroup } from '@angular/forms';
import { IFormField } from '@shared/components/auto-form/interfaces/form-field';
import { ERRORS } from '@shared/components/auto-form/constants/errors';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { appearFromLeft } from '@shared/core/animations/appearFromLeft';


@Component({
  selector: 'shred-form-message-box',
  templateUrl: './form-message-box.component.html',
  styleUrls: ['./form-message-box.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [appearFromLeft]
})
export class FormMessageBoxComponent implements OnInit, OnDestroy {
  @Input() form: FormGroup;
  @Input() fields: IFormField[];
  @Input() errorMessages: {[key: string]: string};

  listErrors: string[] = [];
  invalidFieldValidStatus = 'INVALID';
  fieldValidStatus = 'VALID';
  title: string;
  private titleUpdate = 'Внимание! Для сохранение нужно исправить:'
  private titleAdd = 'Внимание! Для создание нужно исправить:'
  unSubscribeSubject$ = new Subject();
  fullPrefix = '_FULL';

  constructor(private cd: ChangeDetectorRef) {
  }

  ngOnInit() {
    this.checkFormField();

    this.form.statusChanges
      .pipe(
        takeUntil(this.unSubscribeSubject$)
      )
      .subscribe(() => {
        this.checkFormField();
      });
    this.title = this.form.value.id ? this.titleUpdate : this.titleAdd;
  }

  ngOnDestroy() {
    this.unSubscribeSubject$.next();
    this.unSubscribeSubject$.complete();
  }

  private checkFormField(): void {
    this.listErrors = [];
    const fields = this.getPreparedFields();

    Object.keys(this.form.controls)
      .filter(controlKey => this.form.controls[controlKey].status === this.invalidFieldValidStatus)
      .forEach(controlKey => {
        const errors = this.form.controls[controlKey].errors;
        Object.keys(errors)
          .forEach(errorKey => {
            const message = this.getErrorMessage(errorKey, fields[controlKey]);
            this.listErrors.push(message);
          })
      });
    this.cd.markForCheck();
  }

  private getPreparedFields(): { [key: string]: IFormField } {
    const result = {};
    this.fields.forEach(fieldData => {
      result[fieldData.field] = fieldData;
    });

    return result;
  };

  private getErrorMessage(errorKey: string, field: IFormField): string {
    let baseErrorMessage = '';
    if (ERRORS[errorKey]) {
      baseErrorMessage = ERRORS[errorKey] ? ERRORS[errorKey].message2 : '';
    }

    if (!baseErrorMessage && this.errorMessages && this.errorMessages[errorKey]) {
      baseErrorMessage = this.errorMessages[`${errorKey}${this.fullPrefix}`] || this.errorMessages[errorKey];
    }

    if (!baseErrorMessage) {
      baseErrorMessage = errorKey;
    }

    return baseErrorMessage.replace(':field', field.title);
  }

}
