import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output
} from '@angular/core';
import { MatSlideToggleChange } from '@angular/material';
import { FormGroup } from '@angular/forms';
import { IFormField } from '@shared/components/auto-form/interfaces/form-field';

@Component({
  selector: 'shared-form-field-slide-toggle',
  templateUrl: './form-field-slide-toggle.component.html',
  styleUrls: ['./form-field-slide-toggle.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FormFieldSlideToggleComponent {
  @Input() fieldData: IFormField;
  @Input() form: FormGroup;
}
