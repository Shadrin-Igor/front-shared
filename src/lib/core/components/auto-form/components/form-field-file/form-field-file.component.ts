import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef, Inject,
  Input, OnChanges,
  OnDestroy,
  OnInit, SimpleChanges,
  ViewChild
} from '@angular/core';
import { IFormField } from '@shared/components/auto-form/interfaces/form-field';
import { FormGroup } from '@angular/forms';
import { fromEvent, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, map, takeUntil } from 'rxjs/operators';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { appearFromLeft } from '@shared/core/animations/appearFromLeft';
import { CORE_CONFIG } from '@shared/core/tokens';
import { AppConfig } from '@shared/models/config';

@Component({
  selector: 'shared-form-field-file',
  templateUrl: './form-field-file.component.html',
  styleUrls: ['./form-field-file.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    appearFromLeft,
    trigger('inputAnim', [
      state('0', style({
        opacity: 0,
      })),
      state('1', style({
        opacity: 1,
      })),
      transition('0 => 1', [
        animate('500ms')
      ]),
      transition('1 => 0', [
        animate('500ms')
      ])
    ])
  ]
})
export class FormFieldFileComponent implements OnInit, OnDestroy, AfterViewInit, OnChanges {
  @Input() fieldData: IFormField;
  @Input() form: FormGroup;

  @ViewChild('input', { static: false }) input: ElementRef<HTMLInputElement>;

  unSubscribeSubject$ = new Subject();
  imageSource: string | ArrayBuffer;
  inputStatus = '1';
  publicUrl = this.config.publicUrl;

  constructor(private cd: ChangeDetectorRef,
              @Inject(CORE_CONFIG) private config: AppConfig) {
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes) {
      const field = this.fieldData.field;
      if (field && this.form) {
        this.imageSource = this.getControlValue() || '';

        if (this.imageSource) {
          this.inputStatus = '0';
        }
      }
    }
  }

  ngOnDestroy() {
    this.unSubscribeSubject$.next();
    this.unSubscribeSubject$.complete();
  }

  ngAfterViewInit() {
    this.form.get(this.fieldData.field).statusChanges
      .pipe(
        distinctUntilChanged(),
        takeUntil(this.unSubscribeSubject$)
      )
      .subscribe(() => {
        this.cd.markForCheck();
      });
  }

  private getControlValue(): string {
   const field = this.fieldData.field;
    let value: string;
    const controlValue = this.form.get(field).value;
    if (typeof controlValue === 'object' ) {
      if (controlValue[2]) {
        value = controlValue[2];
      }
    } else value = controlValue;

    return value;
  }

  onFileChange(event): void {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.form.get(this.fieldData.field).setValue(file);
      const reader = new FileReader();

      reader.readAsDataURL(file);
      reader.onload = (_event) => {
        this.imageSource = reader.result;
        this.inputStatus = '0';
        this.cd.markForCheck();
      }
    }
  }

  deleteImage(): void {
    this.form.get(this.fieldData.field).setValue('');
    this.input.nativeElement.value = '';
    this.imageSource = '';
    this.inputStatus = '1';
    this.cd.markForCheck();
  }
}
