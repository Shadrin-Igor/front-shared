export interface IFormFieldOption {
  id: number;
  name: string;
}
