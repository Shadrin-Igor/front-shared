export enum InputType {
  Text = 'text',
  Email = 'email',
  Tel = 'tel',
  Number = 'number',
  Url = 'url',
  Range = 'range',
}
