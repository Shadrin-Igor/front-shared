import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output
} from '@angular/core';

@Component({
  selector: 'shared-image',
  templateUrl: './image.component.html',
  styleUrls: ['./image.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ImageComponent implements OnInit {
  @Input() versions: { [key: number]: string };
  @Input() version: number;
  @Input() title: string;
  @Input() index: number;
  @Input() haveZoom = false;
  @Output() clickEvent = new EventEmitter<number>();
  src: string;

  constructor() {
  }

  ngOnInit() {
    if (this.versions && this.version) {
      this.src = this.versions[this.version];
    }
  }

  clickHandler() {
    this.clickEvent.emit(this.index);
  }
}
