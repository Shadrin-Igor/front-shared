import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'shared-table-menu',
  templateUrl: './table-menu.component.html',
  styleUrls: ['./table-menu.component.css']
})
export class TableMenuComponent implements OnInit {

  @Output() edit = new EventEmitter();
  @Output() delete = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }

  editAction() {
    this.edit.emit();
  }

  deleteAction() {
    this.delete.emit();
  }
}
