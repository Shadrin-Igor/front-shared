import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TableMenuComponent} from '@shared/components/table-menu/table-menu.component';
import {MatIconModule, MatMenuModule} from '@angular/material';

@NgModule({
  declarations: [TableMenuComponent],
  imports: [
    CommonModule,
    MatIconModule,
    MatMenuModule
  ],
  exports: [
    TableMenuComponent,
    MatIconModule,
    MatMenuModule
  ]
})
export class TableMenuModule {
}
