import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MatFormFieldModule} from '@angular/material';

import {FormFieldErrorComponent} from '@shared/components/form-field-error/form-field-error.component';

@NgModule({
  declarations: [FormFieldErrorComponent],
  exports: [
    FormFieldErrorComponent
  ],
  imports: [
    CommonModule,
    MatFormFieldModule
  ]
})
export class FormFieldErrorModule {
}
