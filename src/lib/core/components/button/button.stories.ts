import { moduleMetadata } from '@storybook/angular';
import { ButtonModule } from './button.module';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import availableIcons from '../icon/configs/icons.json';

export default {
  title: 'Primitives/Buttons',
  decorators: [
    moduleMetadata({
      imports: [
        ButtonModule,
        NoopAnimationsModule
      ]
    })
  ]
};

export const Button = () => ({
  props: {
    icons: [
      ...Object.keys(availableIcons)
    ]
  },
  template: `
<div class="sb container">
  <div class="row">
    <style>
      .bg-darkgrey {
        background: #404040;
        padding: 10px 5px;
      }
    </style>
    <div class="box column spread">
      <h2 class="mat-title">Themes</h2>
      <app-ui-button>Primary</app-ui-button>
      <app-ui-button theme="secondary">Secondary</app-ui-button>
      <app-ui-button theme="general">General</app-ui-button>
      <app-ui-button theme="light">Light</app-ui-button>
      <app-ui-button theme="outline-icon">Outline-icon</app-ui-button>
      <div class="bg-darkgrey">
        <app-ui-button theme="outline-border">Outline-border</app-ui-button>
      </div>
      <app-ui-button theme="icon" icon="v-profile"></app-ui-button>
    </div>

    <div class="box column spread" *ngFor="let size of ['small', 'normal', 'large']">
      <h2 class="mat-title">Size="{{size}}"</h2>
      <app-ui-button [size]="size">Primary</app-ui-button>
      <app-ui-button [size]="size" theme="secondary">Secondary</app-ui-button>
      <app-ui-button [size]="size" theme="general">General</app-ui-button>
      <app-ui-button [size]="size" theme="light">Light</app-ui-button>
      <app-ui-button [size]="size" theme="outline-icon">Outline-icon</app-ui-button>
      <div class="bg-darkgrey">
        <app-ui-button [size]="size" theme="outline-border">Outline-border</app-ui-button>
      </div>
      <app-ui-button [size]="size" theme="icon" icon="v-profile"></app-ui-button>
    </div>

    <div class="box column spread">
      <h2 class="mat-title">Loading state</h2>
      <app-ui-button [loading]="true">Primary</app-ui-button>
      <app-ui-button [loading]="true" theme="secondary">Secondary</app-ui-button>
      <app-ui-button [loading]="true" theme="general">General</app-ui-button>
      <app-ui-button [loading]="true" theme="light">Light</app-ui-button>
      <app-ui-button [loading]="true" theme="outline-icon">Outline-icon</app-ui-button>
      <div class="bg-darkgrey">
        <app-ui-button [loading]="true" theme="outline-border">Outline-border</app-ui-button>
      </div>
      <app-ui-button [loading]="true" theme="icon" icon="v-profile"></app-ui-button>
    </div>

    <div class="box column spread">
      <h2 class="mat-title">Disabled and loading</h2>
      <app-ui-button [disabled]="true" [loading]="true">Primary</app-ui-button>
      <app-ui-button [disabled]="true" [loading]="true" theme="secondary">Secondary</app-ui-button>
      <app-ui-button [disabled]="true" [loading]="true" theme="general">General</app-ui-button>
      <app-ui-button [disabled]="true" [loading]="true" theme="light">Light</app-ui-button>
      <app-ui-button [disabled]="true" [loading]="true" theme="outline-icon">Outline-icon</app-ui-button>
      <div class="bg-darkgrey">
        <app-ui-button [disabled]="true" [loading]="true" theme="outline-border">Outline-border</app-ui-button>
      </div>
      <app-ui-button [disabled]="true" [loading]="true" theme="icon" icon="v-profile"></app-ui-button>
    </div>

    <div class="box column spread">
      <h2 class="mat-title">Disabled state</h2>
      <app-ui-button [disabled]="true">Primary</app-ui-button>
      <app-ui-button [disabled]="true" theme="secondary">Secondary</app-ui-button>
      <app-ui-button [disabled]="true" theme="general">General</app-ui-button>
      <app-ui-button [disabled]="true" theme="light">Light</app-ui-button>
      <app-ui-button [disabled]="true" theme="outline-icon">Outline-icon</app-ui-button>
      <div class="bg-darkgrey">
        <app-ui-button [disabled]="true" theme="outline-border">Outline-border</app-ui-button>
      </div>
      <app-ui-button [disabled]="true" theme="icon" icon="v-profile"></app-ui-button>
    </div>

    <div class="box column spread">
      <h2 class="mat-title">With Tooltip</h2>
      <app-ui-button tooltip="Tooltip text">Primary</app-ui-button>
      <app-ui-button tooltip="Tooltip text" theme="secondary">Secondary</app-ui-button>
      <app-ui-button tooltip="Tooltip text" theme="general">General</app-ui-button>
      <app-ui-button tooltip="Tooltip text" theme="light">Light</app-ui-button>
      <app-ui-button tooltip="Tooltip text" theme="outline-icon">Outline-icon</app-ui-button>
      <div class="bg-darkgrey">
        <app-ui-button tooltip="Tooltip text" theme="outline-border">Outline-border</app-ui-button>
      </div>
      <app-ui-button tooltip="Tooltip text" theme="icon" icon="v-profile"></app-ui-button>
    </div>

    <!-- @TODO Временно скрыли, пока нет дизайна для кнопок с иконками <div class="box column spread">
      <h2 class="mat-title">Button with icon</h2>
      <app-ui-button icon="v-profile">Button</app-ui-button>
      <app-ui-button theme="secondary" icon="v-profile">Button</app-ui-button>
      <app-ui-button theme="light" icon="v-profile">Button</app-ui-button>
      <app-ui-button theme="general" icon="v-profile">Button</app-ui-button>
      <app-ui-button theme="general" icon="v-profile" size="small">Small</app-ui-button>
      <app-ui-button theme="general" icon="v-profile" size="normal">Normal</app-ui-button>
      <app-ui-button theme="general" icon="v-profile" size="large">Large</app-ui-button>
    </div>-->

    <div class="box column spread" style="max-width: 400px;">
      <h2 class="mat-title">Button icons</h2>
      <div class="row spread">
        <app-ui-button *ngFor="let icon of icons" [icon]="icon" theme="icon"></app-ui-button>
      </div>
    </div>

  </div>
</div>
`
});
