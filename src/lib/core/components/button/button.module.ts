import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ButtonComponent } from './button.component';
import { SharedIconModule } from '@shared/components/icon/icon.module';
import { SharedSpinnerModule } from '@shared/components/spinner/spinner.module';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MatTooltipModule,
    SharedIconModule,
    SharedSpinnerModule,
    MatButtonModule
  ],
  declarations: [ButtonComponent],
  exports: [ButtonComponent]
})
export class ButtonModule {
}
