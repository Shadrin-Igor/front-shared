import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output, SimpleChanges
} from '@angular/core';
import { ButtonTheme } from './ButtonTheme';
import { ButtonSize } from './ButtonSize';
import { TooltipPosition } from '@angular/material/tooltip';
import { IconColor } from '@shared/components/icon';
import { IconSize } from '@shared/components/icon';
import { ButtonStyle } from '@shared/components/button/ButtonStyle';
import { animate, keyframes, style, transition, trigger } from '@angular/animations';
import { appearOpacity } from '@shared/core/animations/appearOpacity';

@Component({
  selector: 'shared-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    trigger('spinnerAnimation', [
      transition(':enter', [
        animate('300ms', keyframes([
          style({opacity: 0, transform: 'translateY(-10px)', offset: 0}),
          style({opacity: 1, transform: 'translateY(0)', offset: 1}),
        ]))
      ]),
      transition(':leave', [
        animate('200ms', keyframes([
          style({opacity: 1, transform: 'translateY(0)', offset: 0}),
          style({opacity: 0, transform: 'translateY(-10px)', offset: 1}),
        ]))
      ])
    ]),
    appearOpacity
  ]
})
export class ButtonComponent implements OnInit, OnChanges {
  @Input() icon = '';
  @Input() iconColor: IconColor;
  @Input() iconSize: IconSize;
  @Input() rightIcon = '';
  @Input() disabled = false;
  @Input() loading = false;
  @Input() active = false;
  @Input() stopPropagation = false;
  @Input() type: 'button' | 'submit' | 'reset' = 'button';
  @Input() color: ButtonTheme = '';
  @Input() size: ButtonSize = 'normal';
  @Input() styleTheme: ButtonStyle = 'basic';
  @Input() tooltip: string;
  @Input() tooltipPosition: TooltipPosition = 'above';
  @Output() clickButton: EventEmitter<boolean> = new EventEmitter<boolean>();

  context: any = {size: 'normal', iconSize: 'normal'};

  buttonIconSize;
  spinnerDiameter = 30;

  constructor() {
  }

  ngOnInit() {
    this.context.buttonIconSize = this.iconSize ? this.iconSize : this.size;
    this.calculateSpinnerDiameter();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.loading) {
      this.context.loading = changes.loading.currentValue;
    }
    if (changes.iconColor && changes.iconColor.currentValue) {
      this.context.iconColor = changes.iconColor.currentValue;
    }
    if (changes.color && changes.color.currentValue) {
      this.context.color = changes.color.currentValue;
    }
    if (changes.buttonIconSize && changes.buttonIconSize.currentValue) {
      this.context.buttonIconSize = changes.buttonIconSize.currentValue;
    }

    if (changes.icon && changes.icon.currentValue) {
      this.context.icon = changes.icon.currentValue;
    }
  }

  private calculateSpinnerDiameter() {
    switch(this.size) {
      case 'normal':
        this.spinnerDiameter = 30;
        break;
    }
  }

  public onClick(evt: any): void {
    if (this.stopPropagation) {
      evt.preventDefault();
      evt.stopPropagation();
    }
    if (!this.disabled) {
      this.clickButton.emit(true);
    }
  }
}
