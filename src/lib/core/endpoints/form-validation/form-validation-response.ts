import { Response } from '@shared/models/endpoints/http/response';

export interface FormValidationResponse extends Response { }
