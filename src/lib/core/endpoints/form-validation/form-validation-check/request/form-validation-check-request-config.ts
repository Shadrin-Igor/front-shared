import { FormValidationRequestConfig } from '@shared/core/endpoints/form-validation/form-validation-request-config';

export interface FormValidationCheckRequestConfig extends FormValidationRequestConfig {
  type: string;
  value: string;
  itemId?: number;
}
