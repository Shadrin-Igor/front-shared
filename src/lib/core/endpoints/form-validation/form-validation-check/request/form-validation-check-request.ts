import { GetRequest } from '@shared/models/index';
import { FormValidationCheckRequestConfig } from '@shared/core/endpoints/form-validation/form-validation-check/request/form-validation-check-request-config';

export class FormValidationCheckRequest extends GetRequest {
  constructor(protected domain: string, private config: FormValidationCheckRequestConfig) {
    super(domain);
  }

  get url() {
    const { type, value, itemId } = this.config;
    return `${this.domain}/api/form-validation/${type}/${value}/${itemId || ''}`;
  }

  get headers() {
    return null;
  }

  get params() {
    return null;
  }

  get body() {
    return null;
  }
}
