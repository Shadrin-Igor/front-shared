import { RequestConfig } from '@shared/models/index';

export interface FormValidationRequestConfig extends RequestConfig { }
