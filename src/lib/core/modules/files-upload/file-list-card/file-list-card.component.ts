import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FilesFacade } from '@shared/state/files/files.facade';
import { untilComponentDestroyed } from '@shared/core/utils';
import {
  FileListMode,
  FileListModeSort,
  IFile
} from '@shared/core/modules/files-upload/files-upload-list-images/files-upload-list-images.component';
import { DialogService } from '@shared/core/dialogs/dialog.service';

@Component({
  selector: 'wt-file-list-card',
  templateUrl: './file-list-card.component.html',
  styleUrls: ['./file-list-card.component.scss']
})
export class FileListCardComponent implements OnInit, OnDestroy {
  @Input() file: IFile;
  @Input() section: string;
  @Input() itemId: number;
  @Input() mode: FileListMode;

  ModeSort = FileListModeSort;

  constructor(private filesFacade: FilesFacade,
              private dialogService: DialogService) {
  }

  ngOnInit() {
  }

  ngOnDestroy(): void {
  }

  changeTitle(event, file) {
    file.title = event.target.value;
  }

  saveTitle(file) {
    this.fileUpdate(file.id, { title: file.title });
  }

  fileUpdate(id, params) {
    this.filesFacade.filesUpdate({ ...params, id, section: this.section, itemId: this.itemId });
  }

  fileDelete(id: number) {
    this.dialogService.openDeleteDialog()
      .pipe(
        untilComponentDestroyed(this)
      )
      .subscribe((result) => {
        if (result) {
          this.filesFacade.filesDelete(id);
        }
      });
  }

  setMain(id: number, main: boolean) {
    if (!main) {
      this.fileUpdate(id, { main: true });
    }
  }

}
