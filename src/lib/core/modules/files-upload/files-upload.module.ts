import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// import { SortablejsModule } from 'angular-sortablejs';
import { FileUploadModule } from 'ng2-file-upload';
import { MatIconModule, MatProgressBarModule } from '@angular/material';
import { DragDropModule } from '@angular/cdk/drag-drop';

import { FilesUploadComponent } from './files-upload/files-upload.component';
//import { CoreModule } from '@wt/core';
import { FilesUploadListImagesComponent } from './files-upload-list-images/files-upload-list-images.component';
import { FileListCardComponent } from './file-list-card/file-list-card.component';
import { SharedSpinnerModule } from '@shared/core/components/spinner/spinner.module';

@NgModule({
  imports: [
    CommonModule,

    MatIconModule,
    MatProgressBarModule,

    // CoreModule,
    FileUploadModule,

    DragDropModule,
    SharedSpinnerModule
    // SortablejsModule
  ],
  declarations: [FilesUploadComponent, FilesUploadListImagesComponent, FileListCardComponent],
  exports: [FilesUploadComponent, DragDropModule]
})
export class FilesUploadModule {
}
