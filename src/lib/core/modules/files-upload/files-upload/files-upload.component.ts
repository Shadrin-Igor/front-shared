import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  Input,
  OnDestroy,
  OnInit
} from '@angular/core';
import { FileItem, FileUploader, ParsedResponseHeaders } from 'ng2-file-upload';
import { animate, state, style, transition, trigger } from '@angular/animations';

import { FilesFacade } from '@shared/state/files/files.facade';
import { NotificationsFacade } from '@shared/state/notifications';
import { CORE_CONFIG } from '@shared/core/tokens';
import { AppConfig } from '@shared/models/index';
import {
  FileListMode, FileListModeList, FileListModeSort,
  IFilesOrders
} from '@shared/core/modules/files-upload/files-upload-list-images/files-upload-list-images.component';
import { AuthenticationService } from '@shared/core/services/auth/authentication.service';

@Component({
  selector: 'shared-files-upload',
  templateUrl: './files-upload.component.html',
  styleUrls: ['./files-upload.component.scss'],
  animations: [
    trigger('uploadFiles', [
      state('open', style({
        opacity: 1,
        height: 'auto'
      })),
      state('close', style({
        opacity: 0,
        height: 0
      })),
      transition('open => close', animate('100ms ease-in')),
      transition('close => open', animate('100ms ease-out'))
    ]),
    trigger('showButton', [
      state('show', style({
        opacity: 1,
        'margin-top': 0
      })),
      state('hide', style({
        opacity: 0,
        'margin-top': '-50px'
      })),
      transition('show => hide', animate('150ms ease-in')),
      transition('hide => show', animate('200ms ease-in'))
    ])
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FilesUploadComponent implements OnInit, OnDestroy {
  @Input() section: string;
  @Input() itemId: number;
  public uploader: FileUploader;
  public hasBaseDropZoneOver = false;

  url: string;
  publicUrl: string;
  uploadFilesState = 'close';
  loaded = false;
  mode: FileListMode = FileListModeList;
  fileListModeSort = FileListModeSort;
  fileOrders: IFilesOrders[];
  showButtonState: 'show' | 'hide' = 'hide';

  constructor(private filesFacade: FilesFacade,
              private notificationsFacade: NotificationsFacade,
              private changeDetectorRef: ChangeDetectorRef,
              private authenticationService: AuthenticationService,
              @Inject(CORE_CONFIG) private config: AppConfig
  ) {
  }

  ngOnInit() {
    this.publicUrl = this.config.publicUrl;
    const apiUrl = this.config.apiUrl;
    const { token } = this.authenticationService.loadAuthDataFromLocalStore();
    const uploadUrl = `${apiUrl}/api/files/${this.section}/${this.itemId}`;
    this.url = `${this.publicUrl}/${this.section}/${this.itemId}`;

    this.uploader = new FileUploader({
      url: uploadUrl,
      authToken: `JWT ${token}`,
      isHTML5: true,
      autoUpload: true
    });
    this.uploader.onErrorItem = (item, response, status, headers) => this.onErrorItem(item, response, status, headers);
    this.uploader.onCompleteAll = this.onCompleteAll.bind(this);
  }

  onCompleteAll(): any {
    console.log('onCompleteAll');
    this.notificationsFacade.addNotifications({
      type: 'success',
      text: 'Картинки успешно загруженны',
      id: 'images.uploads',
      timeout: 2000
    });
    this.filesFacade.loadFiles({ itemId: this.itemId, section: this.section });
  }

  onErrorItem(item: FileItem, response: string, status: number, headers: ParsedResponseHeaders): any {
    // let error = JSON.parse(response); //error server response
    console.log('onErrorItem', { response, item, status });
    this.notificationsFacade.addNotifications({
      type: 'error',
      text: 'При загрузке возникли ошибки, попробуйте еще раз',
      title: 'Ошибка',
      id: 'images.uploads'
    });
  }

  ngOnDestroy(): void {
  }

  fileOverBase(e: any): void {
    this.hasBaseDropZoneOver = e;
  }

  uploadFilesToggle() {
    this.uploadFilesState = this.uploadFilesState === 'open' ? 'close' : 'open';
  }

  onToggleImageBlockDone(e) {
    if (e.toState === 'open') {
    }
  }

  sortFilesOrSave() {
    this.mode = FileListModeSort;
    this.showButtonState = 'show';
  }

  saveSort() {
    this.cancelSort();
    this.filesFacade.filesSort(this.fileOrders);
  }

  changeSort(fileOrders: IFilesOrders[]) {
    this.fileOrders = fileOrders;
  }

  cancelSort() {
    this.mode = FileListModeList;
    this.showButtonState = 'hide';
  }

}


