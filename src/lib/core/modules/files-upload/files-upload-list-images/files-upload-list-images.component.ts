import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output
} from '@angular/core';
import { map, tap } from 'rxjs/operators';
import { FilesFacade } from '@shared/state/files';
import { FileModel } from '@shared/models/models/file/file-model';
import { untilComponentDestroyed } from '@shared/core/utils';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';

export interface IFile extends FileModel {
  edit?: boolean;
}

export const FileListModeList = 'list';
export const FileListModeSort = 'sort';
export type FileListMode = 'list' | 'sort';

export interface IFilesOrders {
  id: number;
  order: number
}

@Component({
  selector: 'shared-files-upload-list-images',
  templateUrl: './files-upload-list-images.component.html',
  styleUrls: ['./files-upload-list-images.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FilesUploadListImagesComponent implements OnInit, OnDestroy {
  @Input() section: string;
  @Input() itemId: number;
  @Input() mode: FileListMode = FileListModeList;
  @Output() changeSort = new EventEmitter<IFilesOrders[]>();
  files: IFile[];
  loaded = false;
  hasAnotherDropZoneOver = false;
  publicUrl: string;
  ModeSort = FileListModeSort;
  ModeList = FileListModeList;

  constructor(private filesFacade: FilesFacade,
              private changeDetectorRef: ChangeDetectorRef) {
  }

  ngOnInit() {
    this.filesFacade.getFiles({ itemId: this.itemId, section: this.section })
      .pipe(
        tap(data => {
          if (!data || !data.length) {
            this.filesFacade.loadFiles({ itemId: this.itemId, section: this.section });
          }
        }),
        map((files: IFile[]) => {
          let mapFiles;

          if (files) {
            mapFiles = files.sort((a: any, b: any) => a.sort - b.sort);

            mapFiles.forEach(item => {
              item.edit = false;
              if (!item.title) {
                item.title = '';
              }
            });
          }
          return mapFiles;
        }),
        untilComponentDestroyed(this)
      )
      .subscribe((files: FileModel[]) => {
        this.files = files;
        this.loaded = true;
        this.changeDetectorRef.detectChanges();
      });
  }

  ngOnDestroy(): void {
  }


  fileUpdate(id, params) {
    this.filesFacade.filesUpdate({ ...params, id, section: this.section, itemId: this.itemId });
  }


  fileOverAnother(e: any): void {
    this.hasAnotherDropZoneOver = e;
  }

  clearEditableTitle() {
    /*    this.files.forEach(item => {
          item.edit = false;
        });*/
  }

  /*  drop() {
      const listSort: { id: number, order: number }[] = [];
      this.files.forEach((item: IFile, index: number) => {
        listSort.push({id: item.id, order: index});
      });
      this.filesFacade.filesSort(listSort);
    }*/

  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.files, event.previousIndex, event.currentIndex);
    const listSort: { id: number, order: number }[] = [];
    this.files.forEach((item: IFile, index: number) => {
      listSort.push({ id: item.id, order: index });
    });
    this.changeSort.emit(listSort);
  }
}
