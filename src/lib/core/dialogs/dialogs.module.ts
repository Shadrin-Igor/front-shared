import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatDialogModule } from '@angular/material';

import { DialogService } from './dialog.service';
import { DeleteComponent } from './delete/delete.component';

@NgModule({
  imports: [
    CommonModule,
    MatDialogModule
  ],
  declarations: [DeleteComponent],
  entryComponents: [DeleteComponent],
  providers: [DialogService]
})
export class DialogsModule {
}
