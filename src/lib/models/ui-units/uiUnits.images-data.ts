export interface UiUnitsImagesData {
  updating: boolean;
  lastCreatedId?: number | string;
  error?: string;
}
