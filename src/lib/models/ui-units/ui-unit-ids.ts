export enum UiUnitIds {
  FIRMS = 'firmsUiUnit',
  FIRMS_CATEGORIES = 'firmsCategoriesUiUnit',
  FIRMS_OPTIONS = 'firmsOptionsUiUnit',
  FIRMS_OPTIONS_TYPES = 'firmsOptionsTypesUiUnit',
  FIRM_FORM = 'firm_form',
  TOURS = 'toursUiUnit',
  TOURS_CATEGORIES = 'toursCategoriesUiUnit',
  TOURS_OPTIONS = 'toursOptionsUiUnit',
  TOURS_OPTIONS_TYPES = 'toursOptionsTypesUiUnit',
}
