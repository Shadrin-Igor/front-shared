import {UiUnitIds} from './ui-unit-ids';
import {UiUnitTypes} from './ui-unit-types';

export interface UiUnitItem {
  id: UiUnitIds;
  type?: UiUnitTypes;
  data: {
    [key: string]: any
  };
}
