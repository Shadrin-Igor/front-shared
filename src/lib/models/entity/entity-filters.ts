export interface EntityFilters {
  [key: string]: any;
}
