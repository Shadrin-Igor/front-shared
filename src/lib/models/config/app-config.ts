export interface AppConfig {
  apiUrl: string;
  publicUrl: string;
  authApiUrl?: string;
  uploadApiUrl?: string;
}
