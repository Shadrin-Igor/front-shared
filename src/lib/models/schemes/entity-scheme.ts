import { Schema } from '@shared/models/mapper/mapper';
import { Entity } from '@shared/core/models/entity';

export const entityScheme: Schema<Entity> = {
  id: 'id'
};
