// @ts-ignore
import { get } from 'lodash';

export interface Source {
  [key: string]: any;
}

export type Result<T> = {
  [K in keyof T]: any
};

type Path = string;
type Modifier = (value: any, source: any) => any;
type Include = (value: any, source: any) => boolean;

export type SchemaEntry = Path | { path: Path, modifier?: Modifier, include?: Include };

export type Schema<Target> = {
  [destinationProperty in keyof Target]?: SchemaEntry
};

export interface MapperOptions {
  strict?: boolean;
  greedy?: boolean;
  name?: string;
  include?: Include;
}

function identity<T>(val: T) {
  return val;
}

function always(val: any) {
  return true;
}

export function mappet<Target>(schema: Schema<Target>, options: MapperOptions = {}): (source: Source) => Result<Target> {
  const { strict, name = 'Mapper', greedy, include } = options;
  return (source: Source): Result<Target> =>
    Object.keys(schema)
      .reduce((result, key) => {
        const schemaEntry = schema[key];
        const _include = typeof schemaEntry === 'string' ? include || always : schemaEntry.include || include || always;
        const path = typeof schemaEntry === 'string' ? schemaEntry : schemaEntry.path;
        const value = get(source, path);

        if (!_include(value, source)) {
          return result;
        }

        const modifier = typeof schemaEntry === 'string' ? identity : schemaEntry.modifier || identity;

        if (strict && value === undefined) {
          throw new Error(`${name}: ${path} not found`);
        }

        return ({ ...result, [key]: modifier(value, source) });
      }, greedy ? { ...source } : {}) as Result<Target>;
}
