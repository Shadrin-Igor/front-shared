import { MapperOptions, mappet, Result, Schema, Source } from './mapper';


export interface Constructable<T> {
  new(...args: any[]): T;
}


export interface IMapperRegistry {
  mappers: Map<any, any>;
  register<Target, TSchema extends Schema<Target>>(type: Constructable<Target>, schema?: TSchema, opt?: MapperOptions): void;
  map<Target, S>(type: Constructable<Target>, data: S): Target;
  getMapper<Target>(type: Constructable<Target>): (source: Source) => Result<Target>;
  setMapper<Target, TSchema extends Schema<Target>>(type: Constructable<Target>, schema: TSchema): (source: Source) => Result<Target>;
  deleteMapper<Target>(type: Constructable<Target>): any;
  exists<Target>(type: Target): boolean;
}

export class MapperRegistry implements IMapperRegistry {
  private _registry: any = null;
  constructor(cache?: Map<any, any> | WeakMap<any, any>) {
    if (!cache) {
      this._registry = { cache: new Map() };
    } else {
      this._registry = cache;
    }
  }

  register<Target, TSchema extends Schema<Target>>(type: Constructable<Target>, schema?: TSchema, opt?: MapperOptions): void {
    if (!type && !schema) {
      throw new Error('type paramater is required when register a mapping');
    } else if (this.exists(type)) {
      throw new Error(`A mapper for ${type.name} has already been registered`);
    }
    const mapper = mappet<Target>(schema, opt);
    this._registry.cache.set(type, mapper);
  }

  map<Target, S>(type: Constructable<Target>, data: S): Target {
    if (!this.exists(type)) {
      throw new Error(`A mapper for ${type.name} not found. Please register mapper or use mappet function`);
    }
    const mapper = this.getMapper(type);
    return mapper(data) as Target;
  }

  getMapper<Target>(type: Constructable<Target>): (source: Source) => Result<Target> {
    return this._registry.cache.get(type);
  }

  setMapper<Target>(type: Constructable<Target>, schema: Schema<Target>, opt?: MapperOptions): (source: Source) => Result<Target> {
    if (!schema) {
      throw new Error(`The schema must be an Object. Found ${schema}`);
    } else if (!this.exists(type)) {
      throw new Error(`The type ${type.name} is not registered. Register it using \`Mophism.register(${type.name}, schema)\``);
    } else {
      const fn = mappet(schema, opt);
      this._registry.cache.set(type, fn);
      return fn;
    }
  }

  deleteMapper(type: any) {
    return this._registry.cache.delete(type);
  }

  exists(type: any) {
    return this._registry.cache.has(type);
  }

  get mappers() {
    return this._registry.cache as Map<any, any>;
  }
}

const mapperRegistry = new MapperRegistry();
const mapperMixin: typeof mappet & any = mappet;
mapperMixin.register = (t: any, s: any, opt: any) => mapperRegistry.register(t, s, opt);
mapperMixin.map = (t: any, d: any) => mapperRegistry.map(t, d);
mapperMixin.getMapper = (t: any) => mapperRegistry.getMapper(t);
mapperMixin.setMapper = (t: any, s: any, opt: any) => mapperRegistry.setMapper(t, s, opt);
mapperMixin.deleteMapper = (t: any) => mapperRegistry.deleteMapper(t);
mapperMixin.mappers = mapperRegistry.mappers;

export const MapperUtil: typeof mappet & IMapperRegistry = mapperMixin;
