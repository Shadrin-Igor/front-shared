import { Constructable, MapperUtil } from '@shared/models/mapper';
import { userScheme } from '@shared/models/models/user/user-scheme';
import { FileModel } from '../models/file/file-model';
import { fileScheme } from '@shared/models/models/file/file-scheme';
import { UserModel } from '@shared/models/models';
import { CategoryModel } from '../../../../../models/src/lib/models/category';
import { categoryScheme } from '../../../../../models/src/lib/models/category/category-scheme';

const includeFn = (value: any, source: any) => {
  return value !== undefined;
};

// @dynamic
export class BaseModelAdapter {

  static _modelAdapter: BaseModelAdapter;

  constructor() {
    MapperUtil.register(UserModel, userScheme, { include: includeFn });
    MapperUtil.register(FileModel, fileScheme, { include: includeFn });
    MapperUtil.register(CategoryModel, categoryScheme, { include: includeFn });

  }

  convertItem<T, Source>(type: Constructable<T>, item: Source): T {
    return MapperUtil.map(type, item);
  }

  convertItems<T, Source>(type: Constructable<T>, items: Source[]): T[] {
    return items.map(item => this.convertItem(type, item));
  }
}
