import { GetRequest } from '@shared/models/endpoints/http/request/get-request';
import { RefreshTokenRequestConfig } from './refresh-token-request-config';

export class RefreshTokenRequest extends GetRequest {
  constructor(protected domain: string, private config: RefreshTokenRequestConfig) {
    super(domain);
  }

  get url() {
    return `${this.domain}/api/auth/refresh-token/${this.config.refreshToken}`;
  }

  get headers() {
    return null; // this.addAuthHeaders(this.config.headers);
  }

  get params() {
    return null;
  }

  get body() {
    return null;
  }
}
