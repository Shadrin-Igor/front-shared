import { AuthResponse } from '../../auth-response';

export interface RefreshTokenResponse extends AuthResponse { }
