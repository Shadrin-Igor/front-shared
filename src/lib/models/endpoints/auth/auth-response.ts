import { Response } from '@shared/models/endpoints/http/response';

export interface AuthResponse extends Response { }
