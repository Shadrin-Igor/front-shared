import { PostRequest } from '@shared/models/endpoints/http/request/post-request';
import { LoginSendCreateRequestConfig } from './login-send-create-request-config';

export class LoginSendCreateRequest extends PostRequest {
  constructor(protected domain: string, private config: LoginSendCreateRequestConfig) {
    super(domain);
  }

  get url() {
    return `${this.domain}/api/auth`;
  }

  get headers() {
    return null; // this.addAuthHeaders(this.config.headers);
  }

  get params() {
    return null;
  }

  get body() {
    const { email, password } = this.config;
    return {
      email,
      password
    };
  }
}
