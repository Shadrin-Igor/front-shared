import { ResponseStatus } from '@shared/models/endpoints/http/response/response-status';
import { AuthResponse } from '../../auth-response';
import { LoginSendCreateResponse } from './login-send-create-response';
import { LoginSendCreate201 } from './login-send-create-201';

export class LoginSendCreateResponseFactory {
    static createResponse(httpResponse: any): AuthResponse {
        let response: AuthResponse;
        switch (httpResponse.status) {
            case ResponseStatus.STATUS_201:
                const response201: LoginSendCreate201 = {
                    status: ResponseStatus.STATUS_201,
                    body: { ...httpResponse.body }
                };
                response = response201;
                break;
            case ResponseStatus.STATUS_422:
            case ResponseStatus.STATUS_400:
            case ResponseStatus.STATUS_404:
                response = {
                    status: httpResponse.status,
                    body: httpResponse.error
                } as LoginSendCreateResponse;
                break;
            default:
                response = null;
                break;

        }
        return response;
    }
}
