import { RequestConfig } from '@shared/models/endpoints/http/request/request-config';

export interface AuthRequestConfig extends RequestConfig { }
