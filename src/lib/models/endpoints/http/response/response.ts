import { MetaModel } from '@shared/models/models';

export interface Response {
    status: number;
    headers?: any;
    body: {
        meta?: MetaModel;
        data: any;
        error: any;
    } | any;

}
