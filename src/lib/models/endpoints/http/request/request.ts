import { HttpHeaders } from '@angular/common/http';

import { RequestHeaders } from './request-headers';
import { RequestMethod } from './request-method';
import { PagingType } from '../../../entity';

export abstract class Request {
  httpMethod: RequestMethod;

  constructor(protected domain: string) {
  }

  abstract get url(): string;

  abstract get headers(): RequestHeaders | HttpHeaders;

  abstract get params(): {};

  addAuthHeaders(headers: { token: string, uid: string }): RequestHeaders {
    if (!!headers && !!headers.token && !!headers.uid) {
      return {
        'Uid': headers.uid,
        'Authorization': `JWT ${headers.token}`
      };
    } else {
      return null;
    }
  }

  cleanTextParams(text: string): string {
    if (!text) return;

    return text.replace(/"/, '\"');
  }

  cleanParams(obj) {
    return Object.keys(obj).reduce((acc, key) => {
      const _acc = acc;
      if (obj[key] !== undefined && obj[key] !== '' && obj[key] !== null) {
        const isObject = typeof obj[key] === 'object';
        if (obj[key] instanceof File) {
          _acc[key] = obj[key];
        } else {
          const value = isObject && !Array.isArray(obj[key]) ? this.cleanParams(obj[key]) : obj[key];
          if (!isObject || Object.keys(value).length > 0) {
            _acc[key] = value;
          }
        }
      }
      return _acc;
    }, {});
  }

  makeFormData(data: { [key: string]: string | Blob }): FormData {
    const formData = new FormData();

    Object.keys(data)
      .forEach((field: string) => {
        formData.append(field, data[field]);
      });

    return formData;
  }

  convertFieldsToString(fields: any = {}, level = 0) {

    if (level === 6) {
      return;
    }

    let url = '';
    if (fields.fields) {
      url = fields.fields.join(',');
    }
    Object.keys(fields).forEach(field => {
      if (field !== 'fields') {
        if (url) {
          url += ',';
        }
        url += `${field}(${this.convertFieldsToString(fields[field], level + 1)})`;
      }
    });

    return `${level === 0 && url ? 'fields=' : ''}${url}`;
  }

  convertPagingToString(paging: PagingType) {
    let res = '';
    if (paging) {
      if (paging.page) {
        res = `page=${paging.page}`;
      }
      if (paging.perPage) {
        res += `${res ? '&' : ''}per_page=${paging.perPage}`;
      }
    }

    return res;
  }

}
