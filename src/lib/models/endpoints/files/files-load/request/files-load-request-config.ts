import { UserAuthHeaders } from '../../../../models/user';
import { FilesRequestConfig } from '../../index';
import { FieldsModel } from '../../../../fields';

export interface FilesLoadRequestConfig extends FilesRequestConfig {
  section: string;
  itemId: number;
  headers: UserAuthHeaders;
  fields?: FieldsModel;
}
