import { ResponseStatus } from '@shared/models/endpoints/http/response/response-status';
import { FilesResponse } from '../../files-response';
import { FilesLoadResponse } from './files-load-response';
import { FilesLoad200 } from './files-load-200';

export class FilesLoadResponseFactory {
    static createResponse(httpResponse: any): FilesResponse {
        let response: FilesResponse;
        switch (httpResponse.status) {
            case ResponseStatus.STATUS_200:
                const response201: FilesLoad200 = {
                    status: ResponseStatus.STATUS_200,
                    body: { ...httpResponse.body }
                };
                response = response201;
                break;
            case ResponseStatus.STATUS_422:
            case ResponseStatus.STATUS_400:
            case ResponseStatus.STATUS_404:
                response = {
                    status: httpResponse.status,
                    body: httpResponse.error
                } as FilesLoadResponse;
                break;
            default:
                response = null;
                break;

        }
        return response;
    }
}
