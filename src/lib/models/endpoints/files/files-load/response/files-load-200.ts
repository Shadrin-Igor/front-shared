import { ResponseStatus } from '@shared/models/endpoints/http/response/response-status';
import { FilesLoadResponse } from './files-load-response';

export interface FilesLoad200 extends FilesLoadResponse {
  status: ResponseStatus.STATUS_200;

  body: {
    data: any;
  };
}
