import { RequestConfig } from '@shared/models/endpoints/http/request/request-config';

export interface FilesRequestConfig extends RequestConfig { }
