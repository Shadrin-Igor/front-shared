import { UserAuthHeaders } from '../../../../models/user';
import { FilesRequestConfig } from '../../files-request-config';

export interface FilesDeleteRequestConfig extends FilesRequestConfig {
    id?: number;
    headers: UserAuthHeaders;
}
