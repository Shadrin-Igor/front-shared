import { ResponseStatus } from '@shared/models/endpoints/http/response/response-status';
import { FilesDeleteResponse } from './files-delete-response';

export interface FilesDelete200 extends FilesDeleteResponse {
  status: ResponseStatus.STATUS_200;

  body: {
    data: any;
  };
}
