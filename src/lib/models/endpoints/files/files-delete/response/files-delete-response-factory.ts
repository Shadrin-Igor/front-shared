import { ResponseStatus } from '@shared/models/endpoints/http/response/response-status';
import { FilesResponse } from '../../files-response';
import { FilesDeleteResponse } from './files-delete-response';
import { FilesDelete200 } from './files-delete-200';

export class FilesDeleteResponseFactory {
    static createResponse(httpResponse: any): FilesResponse {
        let response: FilesResponse;
        switch (httpResponse.status) {
            case ResponseStatus.STATUS_200:
                const response201: FilesDelete200 = {
                    status: ResponseStatus.STATUS_200,
                    body: { ...httpResponse.body }
                };
                response = response201;
                break;
            case ResponseStatus.STATUS_422:
            case ResponseStatus.STATUS_400:
            case ResponseStatus.STATUS_404:
                response = {
                    status: httpResponse.status,
                    body: httpResponse.error
                } as FilesDeleteResponse;
                break;
            default:
                response = null;
                break;

        }
        return response;
    }
}
