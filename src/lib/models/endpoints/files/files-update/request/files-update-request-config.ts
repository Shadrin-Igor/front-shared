import { UserAuthHeaders } from '../../../../models/user/user-auth-headers';
import { FilesRequestConfig } from '../../index';

export interface FilesUpdateRequestConfig extends FilesRequestConfig {
  id: number;
  title?: string;
  sort?: number;
  main?: boolean;
  section: string;
  itemId: number;
  headers: UserAuthHeaders;
}
