import { PutRequest } from '@shared/models/endpoints/http/request/put-request';
import { FilesUpdateRequestConfig } from './files-update-request-config';

export class FilesUpdateRequest extends PutRequest {
  constructor(protected domain: string, private config: FilesUpdateRequestConfig) {
    super(domain);
  }

  get url() {
    return `${this.domain}/api/files/${this.config.id}`;
  }

  get headers() {
    return this.addAuthHeaders(this.config.headers);
  }

  get params() {
    return null;
  }

  get body() {
    const { id, title, main, sort, section, itemId } = this.config;
    const comment = {
      id,
      title,
      main,
      sort,
      section,
      item_id: itemId
    };
    console.log('comment', comment);
    return { ...this.cleanParams(comment) };
  }
}
