import { FilesResponse } from '../../files-response';

export interface FilesUpdateResponse extends FilesResponse { }
