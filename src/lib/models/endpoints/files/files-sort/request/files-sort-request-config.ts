import { UserAuthHeaders } from '../../../../models/user';
import { FilesRequestConfig } from '../../index';

export interface FilesSortRequestConfig extends FilesRequestConfig {
  files: {id: number, order: number}[];
  headers: UserAuthHeaders;
}
