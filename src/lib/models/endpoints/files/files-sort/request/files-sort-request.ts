import { PostRequest } from '@shared/models/endpoints/http/request/post-request';
import { FilesSortRequestConfig } from './files-sort-request-config';

export class FilesSortRequest extends PostRequest {
  constructor(protected domain: string, private config: FilesSortRequestConfig) {
    super(domain);
  }

  get url() {
    return `${this.domain}/api/files/sort`;
  }

  get headers() {
    return this.addAuthHeaders(this.config.headers);
  }

  get params() {
    return null;
  }

  get body() {
    const { files } = this.config;
    const data = {
      files
    };
    return { ...this.cleanParams(data) };
  }
}
