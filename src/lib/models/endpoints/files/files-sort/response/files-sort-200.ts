import { ResponseStatus } from '@shared/models/endpoints/http/response/response-status';
import { FilesSortResponse } from './files-sort-response';

export interface FilesSort200 extends FilesSortResponse {
  status: ResponseStatus.STATUS_200;

  body: {
    data: any;
  };
}
