import { GetRequest } from '@shared/models/endpoints/http/request/get-request';
import { StaticDataLoadRequestConfig } from './static-data-load-request-config';

export class StaticDataLoadRequest extends GetRequest {
  constructor(protected domain: string, private config: StaticDataLoadRequestConfig) {
    super(domain);
  }

  get url() {
    return `${this.domain}/api/static-data/${this.config.type}`;
  }

  get headers() {
    return null;
  }

  get params() {
    return null;
  }

  get body() {
    return null;
  }
}
