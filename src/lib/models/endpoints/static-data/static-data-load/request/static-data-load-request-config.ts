import { StaticDataRequestConfig } from '../../index';
import { StaticDataTypes } from '../../../../../../../../core/src/lib/+state/static-data/static-data-types';

export interface StaticDataLoadRequestConfig extends StaticDataRequestConfig {
  type: StaticDataTypes;
}
