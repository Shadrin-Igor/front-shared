import { BaseResponse } from '@shared/models/endpoints';

export interface StaticDataResponse extends BaseResponse { }
