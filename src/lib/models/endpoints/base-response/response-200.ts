import { ResponseStatus, Response } from '../http';
import { MetaModel } from '@shared/models/models';

export interface Response200 extends Response {
  status: ResponseStatus.STATUS_201;

  body: {
    data: any;
    meta?: MetaModel
  };
}
