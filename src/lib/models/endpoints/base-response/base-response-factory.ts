import { ResponseStatus, Response } from '../http';
import { Response200 } from './response-200';

export class BaseResponseFactory {
    /**
     * Create Response
     */
    static createResponse(httpResponse: any): Response {
        let response: Response;
        switch (httpResponse.status) {
            case ResponseStatus.STATUS_201:
                const response200: Response200 = {
                    status: ResponseStatus.STATUS_201,
                    body: { ...httpResponse.body }
                };
                response = response200;
                break;
            case ResponseStatus.STATUS_422:
            case ResponseStatus.STATUS_400:
            case ResponseStatus.STATUS_404:
                response = {
                    status: httpResponse.status,
                    body: httpResponse.error
                } as Response;
                break;
            default:
                response = null;
                break;

        }
        return response;
    }
}
