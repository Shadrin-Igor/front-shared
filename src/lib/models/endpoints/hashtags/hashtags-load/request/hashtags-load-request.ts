import { HashtagsLoadRequestConfig } from './hashtags-load-request-config';
import { GetRequest } from '@shared/models/endpoints/http/request/get-request';

export class HashtagsLoadRequest extends GetRequest {
  constructor(protected domain: string, private config: HashtagsLoadRequestConfig) {
    super(domain);
  }

  get url() {
    const where = this.convertWHereToString(this.config.where);
    const fields = this.convertFieldsToString(this.config.fields);
    const paging = this.convertPagingToString({perPage: this.config.perPage});
    let dopUrl = '';
    if (where) {
      dopUrl = `?${where}`;
    }
    if (fields) {
      dopUrl += `${dopUrl ? '&' : '?'}${fields}`;
    }
    if (paging) {
      dopUrl += `${dopUrl ? '&' : '?'}${paging}`;
    }
    return `${this.domain}/api/hash-tags/${dopUrl}`;
  }

  get headers() {
    return this.addAuthHeaders(this.config.headers);
  }

  get params() {
    return null;
  }

  get body() {
    return null;
  }
}
