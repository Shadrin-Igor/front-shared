import { UserAuthHeaders } from '../../../../models/user';
import { HashtagsRequestConfig } from '../../hashtags-request-config';

export interface HashtagsLoadRequestConfig extends HashtagsRequestConfig {
  where: any;
  fields: any;
  perPage: number;
  headers: UserAuthHeaders;
}
