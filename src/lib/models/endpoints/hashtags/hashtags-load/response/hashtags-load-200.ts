import { ResponseStatus } from '@shared/models/endpoints/http/response/response-status';
import { HashtagsLoadResponse } from './hashtags-load-response';
import { MetaModel } from '@shared/lib/models';

export interface HashtagsLoad200 extends HashtagsLoadResponse {
  status: ResponseStatus.STATUS_200;

  body: {
    data: any;
    meta?: MetaModel
  };

}
