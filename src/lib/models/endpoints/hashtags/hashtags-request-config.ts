import { RequestConfig } from '@shared/models/endpoints/http/request/request-config';

export interface HashtagsRequestConfig extends RequestConfig { }
