export class MetaModel {
  totalCount: number;
  page?: number;
  perPage?: number;
  sort?: number;
}
