import { mappet, Schema } from '@shared/models/mapper/mapper';
import { entityScheme } from '@shared/models/schemes/entity-scheme';
import { UserFieldNames } from '@shared/models/models/user/user-field-names';
import { UserModel } from '@shared/models/models/user/user-model';

export const userScheme: Schema<UserModel> = {
  ...entityScheme,
  email: 'email',
  token: UserFieldNames.TOKEN,
  tokenExp: UserFieldNames.TOKENEXP,
  refreshToken: UserFieldNames.REFRESHTOKEN,
  role: UserFieldNames.ROLE,
  lastName: UserFieldNames.LASTNAME,
  avatar: UserFieldNames.AVATAR,
  sex: UserFieldNames.SEX,
  rights: UserFieldNames.RIGHTS
};

export const userMapper = mappet(userScheme);
