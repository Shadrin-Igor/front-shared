import { UserRightModel } from './user-right-model';

export class UserModel {
  id?: string;
  token?: string;
  tokenExp?: number;
  refreshToken?: string;
  email?: string;
  name?: string;
  role?: number;
  lastName?: string;
  avatar?: string;
  sex?: 0 | 1;
  rights?: UserRightModel[];
}
