import { QueryFieldsNode } from '../../query-fields-adapter/query-fileds-adapter';

export interface UserQueryFields extends QueryFieldsNode {
  avatar?: any; // ImageAttachmentQueryFields
}
