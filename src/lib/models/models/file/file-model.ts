import { VersionsModel } from './versions-model';

export class FileModel {
  id?: number;
  file: string;
  folder: string;
  main?: boolean;
  title?: string;
  sort?: string;
  itemId?: number;
  section?: string;
  versions?: VersionsModel;
}
