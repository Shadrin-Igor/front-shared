export class HashtagModel {
  id?: number;
  name: string;
  slug: string;
  description?: string;
  image?: string;
}
