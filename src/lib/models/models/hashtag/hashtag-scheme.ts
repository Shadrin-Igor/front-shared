import { HashtagFieldNames, HashtagModel } from '@shared/models/models/hashtag/index';
import { entityScheme, mappet, Schema } from '@shared/models/index';

export const hashtagScheme: Schema<HashtagModel> = {
  ...entityScheme,
  name: HashtagFieldNames.NAME,
  slug: HashtagFieldNames.SLUG,
  description: HashtagFieldNames.DESCRIPTION,
  image: HashtagFieldNames.IMAGE
};

export const hashtagMapper = mappet(hashtagScheme);
