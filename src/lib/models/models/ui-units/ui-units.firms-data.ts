export interface UiUnitsFirmsData {
  updating: boolean;
  lastCreatedId?: number | string;
  error?: string;
}
