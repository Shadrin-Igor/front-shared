import {FieldsModel} from './fields-model';

export interface ImageFieldsModel extends FieldsModel{
  versions: string[];
}
