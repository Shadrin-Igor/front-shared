export function isIdenticalArray(a: any[], b: any[]): boolean {
  if (!a || !b) {
    return false;
  }

  if (a.length === b.length) {
    return a.every(item => b.includes(item));
  }

  return false;
}
